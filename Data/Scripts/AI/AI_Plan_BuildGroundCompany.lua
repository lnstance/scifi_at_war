require("pgevents")

function Definitions()
	DebugMessage("%s -- In Definitions", tostring(Script))

	Category = "Build_Ground_Company"
	IgnoreTarget = true
	TaskForce = {
	{
		"StructureForce",
		"Arakyd_HQ | Aratech_HQ | Baktoid_HQ | Bothawui_HQ | Carida_Academy | CEC_HQ | Cloning_HQ | Colicoid_HQ | Commerce_Guild_HQ | Cygnus_HQ | Damorian_HQ | Free_Dac_HQ | Galentro_HQ | Geonosian_HQ | Haor_Chall_HQ | Hoersch_Kessel_HQ | Incom_HQ | Jedi_Temple | KDY_Branch | KDY_HQ | Koensayr_HQ | Loronar_HQ | MCS_HQ | Mekuun_HQ | NenCarvon_HQ | Olanji_Charubah_HQ | REC_HQ | Rendili_HQ | Rothana_HQ | SFS_HQ | Sienar_HQ | Sorosuub_HQ | Taim_Bak_HQ | Tarkin_Estates | Techno_Union_HQ | TransGalMeg_HQ | Uulshos_HQ | Yutrane_Trackata_HQ | Anaxes_War_College | Tactical_Droid_Factory = 1"
	}
	}

	DebugMessage("%s -- Done Definitions", tostring(Script))
end

function StructureForce_Thread()
	DebugMessage("%s -- In StructureForce_Thread.", tostring(Script))
	
	Purge_Goals(PlayerObject)
	
	StructureForce.Set_As_Goal_System_Removable(false)
	AssembleForce(StructureForce)
	
	StructureForce.Set_Plan_Result(true)
	--Clean out MajorItem budget
	Budget.Flush_Category("MajorItem")
	DebugMessage("%s -- StructureForce done!", tostring(Script));
	ScriptExit()
end

function StructureForce_Production_Failed(tf, failed_object_type)
	DebugMessage("%s -- Abandonning plan owing to production failure.", tostring(Script))
	ScriptExit()
end