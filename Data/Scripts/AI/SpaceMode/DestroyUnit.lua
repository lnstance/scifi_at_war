-- $Id: //depot/Projects/StarWars_Expansion/Run/Data/Scripts/AI/SpaceMode/DestroyUnit.lua#1 $
--/////////////////////////////////////////////////////////////////////////////////////////////////
--
-- (C) Petroglyph Games, Inc.
--
--
--  *****           **                          *                   *
--  *   **          *                           *                   *
--  *    *          *                           *                   *
--  *    *          *     *                 *   *          *        *
--  *   *     *** ******  * **  ****      ***   * *      * *****    * ***
--  *  **    *  *   *     **   *   **   **  *   *  *    * **   **   **   *
--  ***     *****   *     *   *     *  *    *   *  *   **  *    *   *    *
--  *       *       *     *   *     *  *    *   *   *  *   *    *   *    *
--  *       *       *     *   *     *  *    *   *   * **   *   *    *    *
--  *       **       *    *   **   *   **   *   *    **    *  *     *   *
-- **        ****     **  *    ****     *****   *    **    ***      *   *
--                                          *        *     *
--                                          *        *     *
--                                          *       *      *
--                                      *  *        *      *
--                                      ****       *       *
--
--/////////////////////////////////////////////////////////////////////////////////////////////////
-- C O N F I D E N T I A L   S O U R C E   C O D E -- D O   N O T   D I S T R I B U T E
--/////////////////////////////////////////////////////////////////////////////////////////////////
--
--              $File: //depot/Projects/StarWars_Expansion/Run/Data/Scripts/AI/SpaceMode/DestroyUnit.lua $
--
--    Original Author: James Yarrow
--
--            $Author: Andre_Arsenault $
--
--            $Change: 37816 $
--
--          $DateTime: 2006/02/15 15:33:33 $
--
--          $Revision: #1 $
--
--/////////////////////////////////////////////////////////////////////////////////////////////////

require("pgevents")

function Definitions()
	DebugMessage("%s -- In Definitions", tostring(Script))
	
	AllowEngagedUnits = false
	MinContrastScale = 0.8
	MaxContrastScale = 3.0
	Category = "Destroy_Unit"
	TaskForce = {
	{
		"MainForce"
		,"MinimumTotalSize = 1"
		,"Corvette|Frigate|Capital|SuperCapital|SpaceHero = 100%"
	},
	{
		"EscortForce"
		,"Fighter = 0, 10" -- don't take too many fighters because we allow engaged units"}}
		,"Corvette|Frigate = 0, 10"
	}
	}
	
	kill_target = nil

	DebugMessage("%s -- Done Definitions", tostring(Script))
end

function MainForce_Thread()
	DebugMessage("%s -- In MainForce_Thread.", tostring(Script))

	BlockOnCommand(MainForce.Produce_Force())
	
	unit_table = MainForce.Get_Unit_Table()
	
	for i,unit in pairs(unit_table) do
		if unit.Has_Property("Carrier") then
			MainForce.Release_Unit(unit)
		end
	end
	
	QuickReinforce(PlayerObject, AITarget, MainForce)
	
	TaskForce_Attack_Move(MainForce, PlayerObject, AITarget)
	
	MainForce.Enable_Attack_Positioning(true)
	DebugMessage("MainForce constructed at stage area!")
	
	SetClassPriorities(MainForce, "Attack_Move")

	DebugMessage("%s -- Attack-moving to %s", tostring(Script), tostring (AITarget))
	BlockOnCommand(MainForce.Attack_Move(AITarget, MainForce.Get_Self_Threat_Max()))

	MainForce.Set_Plan_Result(true)
	
	DebugMessage("%s -- MainForce Done!  Exiting Script!", tostring(Script))
	ScriptExit()
end

-- Make sure that units don't sit idle at the end of their move order, waiting for others
function MainForce_Unit_Move_Finished(tf, unit)

	DebugMessage("%s -- %s reached end of move, giving new order", tostring(Script), tostring(unit))

	-- Assist the tf with whatever is holding it up
	kill_target = FindDeadlyEnemy(tf)
	if TestValid(kill_target) and not kill_target.Is_Category("Fighter") then
		unit.Attack_Move(kill_target)
	else
		unit.Attack_Move(tf)
	end
end

function EscortForce_Thread()
   BlockOnCommand(EscortForce.Produce_Force())
   
   QuickReinforce(PlayerObject, AITarget, EscortForce, MainForce)
   
   EscortForce.Guard_Target(MainForce)
   EscortAlive = true
   while EscortAlive do
      Escort(EscortForce, MainForce)
   end
end
