-- $Id: //depot/Projects/StarWars_Expansion/Run/Data/Scripts/AI/SpaceMode/TacticalMultiplayerBuildSpaceUnitsGeneric.lua#5 $
--/////////////////////////////////////////////////////////////////////////////////////////////////
--
-- (C) Petroglyph Games, Inc.
--
--
--  *****           **                          *                   *
--  *   **          *                           *                   *
--  *    *          *                           *                   *
--  *    *          *     *                 *   *          *        *
--  *   *     *** ******  * **  ****      ***   * *      * *****    * ***
--  *  **    *  *   *     **   *   **   **  *   *  *    * **   **   **   *
--  ***     *****   *     *   *     *  *    *   *  *   **  *    *   *    *
--  *       *       *     *   *     *  *    *   *   *  *   *    *   *    *
--  *       *       *     *   *     *  *    *   *   * **   *   *    *    *
--  *       **       *    *   **   *   **   *   *    **    *  *     *   *
-- **        ****     **  *    ****     *****   *    **    ***      *   *
--                                          *        *     *
--                                          *        *     *
--                                          *       *      *
--                                      *  *        *      *
--                                      ****       *       *
--
--/////////////////////////////////////////////////////////////////////////////////////////////////
-- C O N F I D E N T I A L   S O U R C E   C O D E -- D O   N O T   D I S T R I B U T E
--/////////////////////////////////////////////////////////////////////////////////////////////////
--
--              $File: //depot/Projects/StarWars_Expansion/Run/Data/Scripts/AI/SpaceMode/TacticalMultiplayerBuildSpaceUnitsGeneric.lua $
--
--    Original Author: James Yarrow
--
--            $Author: James_Yarrow $
--
--            $Change: 54441 $
--
--          $DateTime: 2006/09/13 15:08:39 $
--
--          $Revision: #5 $
--
--/////////////////////////////////////////////////////////////////////////////////////////////////

require("pgevents")
						
function Definitions()

	Category = "Tactical_Multiplayer_Build_Space_Units_Flagship"
	IgnoreTarget = true
	TaskForce = {
		{
		"ReserveForce"
		,"Flagship_2_K_Wing | Flagship_3_E_Wing | Flagship_4_NR_Defender | Flagship_2_A_Wing | Flagship_2_B_Wing_E | Flagship_2_B_Wing | Flagship_4_X_Wing | Flagship_A_Wing | Flagship_B_wing | Flagship_2_X_Wing | Flagship_3_TIE_Fighter | Flagship_6_TIE_Fighter | Flagship_1_TIE_Bomber | Flagship_2_TIE_Bomber | Flagship_2_TIE_Defender | Flagship_3_TIE_Defender | Flagship_1_Missile_Boat | Flagship_2_Missile_Boat | Flagship_2_TIE_Interceptor | Flagship_4_TIE_Interceptor | Flagship_4_Super_TIE | Flagship_3_Preybird | Flagship_2_Scimitar_Assault_Bomber = 0, 5"
		,"Flagship_Nebulon_5_CR90 | Flagship_Marauder_5_DP20 | Flagship_Quasar_2_DP20_1_CR90 | Flagship_2_Nebulon | Flagship_1_Quasar_2_Nebulon | Flagship_3_Nebulon | Flagship_1_Hajen_3_Sacheen | Flagship_1_Corona_5_CR90 | Flagship_1_Sacheen_5_Dp20 |  Flagship_2_Corona_1_Quasar | Flagship_3_Corona | Flagship_2_Carrack_1_TIE_Fighter | Flagship_4_Carrack_2_TIE_Fighter | Flagship_2_Ton_Falk_2_Crusader | Flagship_2_Ton_Falk_5_Crusader | Flagship_1_Carrack_5_Crusader | Flagship_2_Carrack_1_Ton_Falk | Flagship_2_Carrack_1_Ton_Falk_1_Lancer = 0, 5" 
		,"Flagship_3_MC30 |  Flagship_1_MC40_2_Assault_Frigate | Flagship_1_Liberty_2_MC40 | Flagship_3_BAC | Flagship_1_Liberator_2_Dauntless | Flagship_1_Defender_2_Majestics | Flagship_1_Endurance_2_Nebula | Flagship_3_DHC_Empire | Flagship_2_Vindicator_1_Strike_Crusier | Flagship_1_MTC_2_Acclamator_Assault_Ship_II_1_Lancer | Flagship_3_Crimson_Command_VSD | Flagship_1_VSD_I_2_VSD_II= 0, 5" 
		,"Flagship_1_MC80b_1_Liberty_1_nebulon_1_Marauder | Flagship_1_MC90_2_Republic | Flagship_1_Home_One_2_MC80B_4_MC40_6_MC30 | Flagship_1_Blue_Diver_2_MC90 | Flagship_1_ISD_I_2_VSD_I | Flagship_2_ISD_II | Flagship_1_ISD_I_1_Tector | Flagship_2_Venator_1_Tector | Flagship_1_Allegiance_2_Tector_2_VSD_II_4_lancer | Flagship_2_Venator_2_ISD_II= 0, 3"
		,"Flagship_3_Torrent_Squadron | Flagship_2_ARC_170_Squadron | Flagship_1_BTLB_Y-Wing_Squadron_1_Torrent_Squadron | Flagship_3_Torrent_Squadron_1_Eta2_Actis | Flagship_2_ARC_170_Squadron_1_Republic_Z95_Headhunter_Squadron | Flagship_1_Cloakshape_Squadron_2_Twin_Ion_Engine_Starfighter_Squadron | Flagship_3_Torrent_Squadron_2_Eta2_Actis | Flagship_2_Y_Squadron_2_V-Wing_Squadron | Flagship_2_ARC_170_Squadron_2_Republic_Z95_Headhunter_Squadron | Flagship_5_Vulture_Squadron | Flagship_2_Mankvim_Squadron | Flagship_2_Hyena_Squadron_1_Vulture_Squadron | Flagship_6_Vulture_Squadron | Flagship_2_Mankvim_Squadron_1_Ginivex_Squadron | Flagship_3_Scarab_Squadron_1_Nantex_Squadron | Flagship_7_Vulture_Squadron | Flagship_4_Hyena_Squadron_2_Vulture_Squadron | Flagship_3_TriFighter_Squadron_2_Nantex_Squadron = 0, 5"
		,"Flagship_1_Carrack_Cruiser_Lasers_4_DP20 | Flagship_1_Arquitens_4_LAC | Flagship_5_LAC_3_DP20 | Flagship_1_Arquitens_3_Pelta | Flagship_1_Arquitens_4_CR90 | Flagship_5_CR90_3_DP20_2_Citadel_Cruiser | Flagship_1_Arquitens_3_Pelta_2_Early_Skipray_Blastboat | Flagship_1_Geonosian_Cruiser_4_Diamond_Frigate | Flagship_1_Munifex_2_Hardcell | Flagship_1_C9979_2_Gozanti_Cruiser | Flagship_2_Munifex_1_Marauder | Flagship_2_Munifex_1_Marauder_2_Interceptor_Frigate = 0, 5"
		,"Flagship_1_Dreadnaught_Carrier_2_Dreadnaught | Flagship_1_Dreadnaught_Lasers_2_Pelta_Support | Flagship_1_Acclamator_Assault_Ship_I_2__Acclamator_Assault_Ship_Leveler | Flagship_1_Dreadnaught_Carrier_2_Acclamator_Assault_Ship_I | Flagship_1_Dreadnaught_2_Pelta_Support | Flagship_1_Acclamator_Assault_Ship_I_2__Acclamator_Assault_Ship_II | Flagship_1_Dreadnaught_2_Acclamator_Assault_Ship_Leveler | Flagship_3_Victory_Destroyer_6_DP20 | Flagship_2_Venator_4_Arquitens_2_CR90 | Flagship_1_Supply_Ship_2_Banking_Clan_Dreadnaught_Lasers | Flagship_1_Recusant_2_Munificent | Flagship_1_Captor_2_Auxilia | Flagship_1_Captor_3_C9979 | Flagship_1_Supply_Ship_2_Banking_Clan_Dreadnaught | Flagship_3_Munificent_2_C9979 | Flagship_1_Recusant_Dreadnought_3_Recusant | Flagship_3_Bulwark_I_3_Hardcell | Flagship_3_Providence_3_Munifex_2_Geonosian_Cruiser | Flagship_3_Captor_1_C9979 = 0, 5"
		,"Flagship_1_Invincible_2_Venator_2_Victory_Destroyer | Flagship_2_Star_Destroyer | Flagship_1_Tector_3_Victory_Destroyer | Flagship_1_Praetor_2_Secutor | Flagship_1_Praetor_2_Star_Destroyer_4_Victory_Destroyer_2_Neutron_Star | Flagship_1_Secutor_2_Tector_2_Venator_4_Acclamator_Assault_Ship_II | Flagship_2_Bulwark_II_4_Bulwark_I | Flagship_1_Providence_Dreadnought_4_Providence | Flagship_1_Lucrehulk_2_Recusant_2_Munificent | Flagship_1_Subjugator_2_Lucrehulk_Battleship | Flagship_1_Subjugator_3_Bulwark_II_6_Bulwark_II_10_Geonosian_Cruiser | Flagship_1_Lucrehulk_Battleship_3_Providence_Dreadnought_3_Recusant_Dreadnought = 0, 3"
		}
	}
	
	AllowFreeStoreUnits = false

end

function ReserveForce_Thread()
			
	BlockOnCommand(ReserveForce.Produce_Force())
	ReserveForce.Set_Plan_Result(true)
	ReserveForce.Set_As_Goal_System_Removable(false)
		
	-- Give some time to accumulate money.
	tech_level = PlayerObject.Get_Tech_Level()
	max_sleep_seconds = 60
	max_cash_on_hand = 6000
	if tech_level == 2 then
		max_sleep_seconds = 60
		max_cash_on_hand = 8000
	elseif tech_level == 3 then
		max_sleep_seconds = 60
		max_cash_on_hand = 10000
	elseif tech_level == 4 then
		max_sleep_seconds = 60
		max_cash_on_hand = 15000
	elseif tech_level == 5 then
		max_sleep_seconds = 60
		max_cash_on_hand = 20000
	end
	
	current_sleep_seconds = 0
	while (current_sleep_seconds < max_sleep_seconds) do
		current_sleep_seconds = current_sleep_seconds + 1
		Sleep(1)
	end

	ScriptExit()
end