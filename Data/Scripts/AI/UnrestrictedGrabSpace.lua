-- $Id: //depot/Projects/StarWars_Expansion/Run/Data/Scripts/AI/UnrestrictedGrabSpace.lua#1 $
--/////////////////////////////////////////////////////////////////////////////////////////////////
--
-- (C) Petroglyph Games, Inc.
--
--
--  *****           **                          *                   *
--  *   **          *                           *                   *
--  *    *          *                           *                   *
--  *    *          *     *                 *   *          *        *
--  *   *     *** ******  * **  ****      ***   * *      * *****    * ***
--  *  **    *  *   *     **   *   **   **  *   *  *    * **   **   **   *
--  ***     *****   *     *   *     *  *    *   *  *   **  *    *   *    *
--  *       *       *     *   *     *  *    *   *   *  *   *    *   *    *
--  *       *       *     *   *     *  *    *   *   * **   *   *    *    *
--  *       **       *    *   **   *   **   *   *    **    *  *     *   *
-- **        ****     **  *    ****     *****   *    **    ***      *   *
--                                          *        *     *
--                                          *        *     *
--                                          *       *      *
--                                      *  *        *      *
--                                      ****       *       *
--
--/////////////////////////////////////////////////////////////////////////////////////////////////
-- C O N F I D E N T I A L   S O U R C E   C O D E -- D O   N O T   D I S T R I B U T E
--/////////////////////////////////////////////////////////////////////////////////////////////////
--
--              $File: //depot/Projects/StarWars_Expansion/Run/Data/Scripts/AI/UnrestrictedGrabSpace.lua $
--
--    Original Author: James Yarrow
--
--            $Author: Andre_Arsenault $
--
--            $Change: 37816 $
--
--          $DateTime: 2006/02/15 15:33:33 $
--
--          $Revision: #1 $
--
--/////////////////////////////////////////////////////////////////////////////////////////////////

--A plan to rapidly grab space control that bypasses timing restrictions on AI attacks.

require("pgtaskforce")

-- Tell the script pooling system to pre-cache this number of scripts.
ScriptPoolCount = 4

function Definitions()	
	MaxContrastScale = 1.3
	MinContrastScale = 1.1
		
	Category = "AlwaysOff"
	TaskForce = {
	{
		"MainForce"		
		, "Corvette | Frigate | Capital  = 100%"
	}
	}
	RequiredCategories = {"Frigate | Capital"}	--we want at least one heavier ship
	
	LandSecured = false
	end_blockade = 0
	
end

function MainForce_Thread()
	
	-- Since we're using plan failure to adjust contrast, we're 
	-- only concerned with failures in battle. Default the 
	-- plan to successful and then 
	-- only on the event of our task force being killed is the
	-- plan set to a failed state.
	MainForce.Set_Plan_Result(true)	
	
	--For fast execution, build and attack in one plan rather than having the first few iterations
	--feed the freestore
	AssembleForce(MainForce)
	
	GlobalValue.Set("CONQUER_OPPONENT", Target.Get_Type().Get_Name())
	BlockOnCommand(MainForce.Move_To(Target))
	
	if MainForce.Get_Force_Count() == 0 then
		DebugMessage("%s -- taskforce destroyed, exiting", tostring(Script))
		MainForce.Set_Plan_Result(false)	
		ScriptExit()
	end
	
	--10% chance the fleet will just leave immediately
	end_blockade = GameRandom.Free_Random(0, 100)
	if end_blockade < 10 then
		DebugMessage("%s -- not blockading, rolled %s", tostring(Script), tostring(end_blockade))
		MainForce.Set_Plan_Result(true)	
		ScriptExit()
	end
	
	--Maybe this keeps them there a while, maybe it doesn't
	Sleep(40)
	
	ScriptExit()
end

function MainForce_Production_Failed(failed_object_type)
	ScriptExit()
end

function MainForce_Original_Target_Owner_Changed(tf, old_owner, new_owner)	
	--If we take control of this planet than we'll release our blockading units
	if new_owner == PlayerObject then
		ScriptExit()
	end
end