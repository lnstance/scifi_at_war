-- $Id: //depot/Projects/StarWars_Expansion/Run/Data/Scripts/FreeStore/BusyTacticalFreeStore.lua#12 $
--/////////////////////////////////////////////////////////////////////////////////////////////////
--
-- (C) Petroglyph Games, Inc.
--
--
--  *****           **                          *                   *
--  *   **          *                           *                   *
--  *    *          *                           *                   *
--  *    *          *     *                 *   *          *        *
--  *   *     *** ******  * **  ****      ***   * *      * *****    * ***
--  *  **    *  *   *     **   *   **   **  *   *  *    * **   **   **   *
--  ***     *****   *     *   *     *  *    *   *  *   **  *    *   *    *
--  *       *       *     *   *     *  *    *   *   *  *   *    *   *    *
--  *       *       *     *   *     *  *    *   *   * **   *   *    *    *
--  *       **       *    *   **   *   **   *   *    **    *  *     *   *
-- **        ****     **  *    ****     *****   *    **    ***      *   *
--                                          *        *     *
--                                          *        *     *
--                                          *       *      *
--                                      *  *        *      *
--                                      ****       *       *
--
--/////////////////////////////////////////////////////////////////////////////////////////////////
-- C O N F I D E N T I A L   S O U R C E   C O D E -- D O   N O T   D I S T R I B U T E
--/////////////////////////////////////////////////////////////////////////////////////////////////
--
--              $File: //depot/Projects/StarWars_Expansion/Run/Data/Scripts/FreeStore/BusyTacticalFreeStore.lua $
--
--    Original Author: Steve_Copeland
--
--            $Author: James_Yarrow $
--
--            $Change: 55010 $
--
--          $DateTime: 2006/09/19 19:14:06 $
--
--          $Revision: #12 $
--
--/////////////////////////////////////////////////////////////////////////////////////////////////

require("pgcommands")
require("TRTacticalFreeStore")

---Basic freestore definitions
function Base_Definitions()

    Common_Base_Definitions()

    ServiceRate = 10
    UnitServiceRate = 2

    if Definitions then
        Definitions()
    end

    FREE_STORE_ATTACK_RANGE = 6000.0
	
	aggressive_mode = false
	outnumbered = 0
end

---freestore cycle until exit
function main()

    if FreeStoreService then
        while 1 do
            FreeStoreService()
            PumpEvents()
        end
    end

    ScriptExit()
end

---Default behavior when unit added to the freestore
---@param object GameObject
function On_Unit_Added(object)
end

---Freestore cycle service. Checks enemy and friendly locations, and turns on aggressive mode, based on perceptions
function FreeStoreService()
    enemy_location = FindTarget.Reachable_Target(PlayerObject, "Current_Enemy_Location", "Tactical_Location", "Any_Threat", 0.5)
    friendly_location = FindTarget.Reachable_Target(PlayerObject, "Current_Friendly_Location", "Tactical_Location", "Any_Threat", 0.5)

	outnumbered = EvaluatePerception("Outnumbered", PlayerObject)

	if (EvaluatePerception("Allowed_As_Defender_Space_Untargeted", PlayerObject) > 0.0) then
		DebugMessage("%s -- Aggressive mode activated", tostring(Script))
		aggressive_mode = true
	else
		aggressive_mode = false
	end
	
	if TestValid(space_station) then
		station_threat = FindDeadlyEnemy(space_station)
		if station_threat then
			space_station.Attack_Target(station_threat)
		end
	else
		space_station = Find_All_Objects_Of_Type("IsStarbase", PlayerObject)
		
		if space_station then
			space_station = space_station[1]
		end
	end
end

---Default behavior on UnitService (controlled by UnitServiceRate)
---@param object GameObject
function On_Unit_Service(object)

    if not TestValid(object) then
        return
    end

    if object.Is_Category("SpaceStructure") or object.Is_Category("Transport") then
        return
    end

    if TR_On_Unit_Service(object) then
        return
    end

    current_target = object.Get_Attack_Target()
    if TestValid(current_target) then

		if object.Is_Category("Fighter") or object.Is_Category("Bomber") or object.Is_Category("Transport") or object.Is_Category("Corvette") or object.Is_Category("Frigate") then
			if Service_Kite(object) then
				return
			end
		end

        object.Activate_Ability("SPOILER_LOCK", false)
    end
	
	-- Are we completely outnumbered?
	if (outnumbered > 0) and TestValid(space_station) then	
		object.Move_To(space_station.Get_Position())
		object.Turn_To_Face(Find_First_Object("Attacker Entry Position"))
		
		Service_Attack(object)
		return
	end

    if not object.Has_Active_Orders() then

        --Keep bored objects mobile
        object.Activate_Ability("SPOILER_LOCK", true)
		
		--Small units kite, or guard
		if object.Is_Category("Fighter") or object.Is_Category("Bomber") or object.Is_Category("Transport") or object.Is_Category("Corvette") then
			
			if Service_Kite(object) then
				return
			end

			Service_Guard(object)
				
		end

        if Service_Attack(object) then
            return
        end

        Service_Guard(object)

    end
end

---Gets units in range of enemies or attack immediately if in range
---@param GameObject controlled unit
---@param enemy unit to attack
function get_in_range(object, enemy_object)
	
	if object.Get_Distance(enemy_object) > object.Get_Type().Get_Max_Range() then
		object.Attack_Target(enemy_object)
	else
		object.Attack_Move(Project_By_Unit_Range(object, enemy_object.Get_Position()))
	end
	
	return true	
end

---Makes units engage enemies
---@param object GameObject
---@return boolean
function Service_Attack(object)

	closest_enemy = Find_Nearest(object, "Corvette|Frigate|Capital|SuperCapital|SpaceStructure", object.Get_Owner(), false)
	
	if not TestValid(closest_enemy) or not TestValid(enemy_location) then
		return false
	else
		DebugMessage("%s -- closest enemy is %s units away", tostring(Script), tostring(object.Get_Distance(closest_enemy)))
	end
	
	if object.Get_Distance(closest_enemy) < FREE_STORE_ATTACK_RANGE then
		if get_in_range(object, closest_enemy) then
			return true
		end
	elseif aggressive_mode then
		if get_in_range(object, closest_enemy) then
			return true
		end
	end
	
	deadly_enemy = FindDeadlyEnemy(object)
	
	if TestValid(deadly_enemy) then
		DebugMessage("%s -- attacking enemy attacker", tostring(Script))
		if get_in_range(object, deadly_enemy) then
			return true
		end
	end	

    return false
end

---Controls freestore guard behavior
---@param object GameObject
---@return boolean
function Service_Guard(object)

	if object.Is_Category("Frigate") or object.Is_Category("Capital") or object.Is_Category("SuperCapital") then
		return false
	end

    friendly_structures = Find_All_Objects_Of_Type("SpaceStructure", object.Get_Owner())
	
	if object.Has_Property("Carrier") then
		if TestValid(friendly_location) then
			object.Attack_Move(friendly_location)
			return true	
		elseif friendly_structures then
			for i,structure in pairs(friendly_structures) do
				object.Attack_Move(structure.Get_Position())
			end
			return true
		elseif aggressive_mode and TestValid(enemy_location) then
			object.Move_To(Project_By_Unit_Range(object, enemy_location))
			return true
		end		
	end
	
	if TestValid(friendly_location) then
		object.Attack_Move(friendly_location)
		return true
	elseif aggressive_mode and TestValid(enemy_location) then
		object.Move_To(Project_By_Unit_Range(object, enemy_location))
		return true
	elseif friendly_structures then
		for i,structure in pairs(friendly_structures) do
			object.Attack_Move(structure.Get_Position())
		end
		return true
	end

    return false
end

---Controls freestore kiting behavior
---@param object GameObject
---@return boolean
function Service_Kite(object)

	if object.Has_Property("DoNotKite") then
        return false
    end

    deadly_enemy = FindDeadlyEnemy(object)

    if TestValid(deadly_enemy) then

		if Try_Ability(object, "BUZZ_DROIDS", object) then
			return true
		end
		
		Try_Ability(object, "SPOILER_LOCK")
		Try_Ability(object, "STEALTH")

		kite_pos = Project_By_Unit_Range(deadly_enemy, object)

		object.Move_To(kite_pos)

		return true
    end

    return false
end
