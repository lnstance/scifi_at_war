-- $Id: //depot/Projects/StarWars_Expansion/Run/Data/Scripts/FreeStore/GalacticHeroFreeStore.lua#2 $
--/////////////////////////////////////////////////////////////////////////////////////////////////
--
-- (C) Petroglyph Games, Inc.
--
--
--  *****           **                          *                   *
--  *   **          *                           *                   *
--  *    *          *                           *                   *
--  *    *          *     *                 *   *          *        *
--  *   *     *** ******  * **  ****      ***   * *      * *****    * ***
--  *  **    *  *   *     **   *   **   **  *   *  *    * **   **   **   *
--  ***     *****   *     *   *     *  *    *   *  *   **  *    *   *    *
--  *       *       *     *   *     *  *    *   *   *  *   *    *   *    *
--  *       *       *     *   *     *  *    *   *   * **   *   *    *    *
--  *       **       *    *   **   *   **   *   *    **    *  *     *   *
-- **        ****     **  *    ****     *****   *    **    ***      *   *
--                                          *        *     *
--                                          *        *     *
--                                          *       *      *
--                                      *  *        *      *
--                                      ****       *       *
--
--/////////////////////////////////////////////////////////////////////////////////////////////////
-- C O N F I D E N T I A L   S O U R C E   C O D E -- D O   N O T   D I S T R I B U T E
--/////////////////////////////////////////////////////////////////////////////////////////////////
--
--              $File: //depot/Projects/StarWars_Expansion/Run/Data/Scripts/FreeStore/GalacticHeroFreeStore.lua $
--
--    Original Author: Steve_Copeland
--
--            $Author: James_Yarrow $
--
--            $Change: 56728 $
--
--          $DateTime: 2006/10/24 14:14:34 $
--
--          $Revision: #2 $
--
--/////////////////////////////////////////////////////////////////////////////////////////////////

require("pgcommands")

function Definitions()
	DebugMessage("%s -- Defining custom freestore movement perceptions", tostring(Script))

	-- Table which maps heroes to perceptions for systems they like to hang out on when not in active use
	-- The boolean is for whether or not the hero prefers to stay in space, if he has a choice
	-- Generally, this is to find the system where their abilities provide the best defensive or infrastructure bonuses
	CustomUnitPlacement = {
		ARALANI_FRONTIER = {nil, true}
		,ASHIK_TEAM = {nil, false}
		,CHAK_FEL_KRSISS_SQUADRON_ASSOCIATION = {nil, true}
		,CHIMERA = {nil, true}
		,GREY_WOLF = {nil, true}
		,HAND_OF_JUDGEMENT_TEAM = {nil, false}
		,JAG_FEL_SQUADRON_TEAM = {nil, true}
		,NIRIZ_ADMONITOR = {nil, true}
		,PARCK_STRIKEFAST = {nil, true}
		,SIATH_BATTLEHAMMER = {nil, true}
		,SOONTIR_FEL_SQUADRON = {nil, true}
		,STENT_THRAWNS_HAND = {nil, true}
		,TASSE_TEAM = {nil, false}
		
		,ANDAL_TEAM = {"Is_Production_Planet", false}
		,DELVARDUS_BRILLIANT = {nil, true}
		,JOHANS_TEAM = {nil, false}
		,KABALIAN_STAR_DESTROYER = {nil, true}
		,NIGHT_HAMMER = {nil, true}
		,NIGHT_HAMMER_DELVARDUS = {"Is_Home_Planet", true}
		,PRITTICK_TYRANT = {nil, true}
		,PRYL_THUNDERFLARE = {nil, true}
		,RETRIBUTION_STAR_DESTROYER = {nil, true}
		,SWORD_SQUADRON = {nil, true}
		,THALASSA = {nil, true}
		,VICTOR_STRANG_TEAM = {nil, false}
		,STRANG_TEAM = {nil, false}
		,WEIR_TEAM = {nil, false}
		,ZED_STALKER = {nil, true}
		
		,AGONIZER_STAR_DESTROYER = {nil, true}
		,ASCIAN = {nil, true}
		,BANJEER_NEUTRON = {nil, true}
		,BRAKISS_TEAM = {nil, false}
		,BRASHIN_INQUISITOR = {nil, true}
		,BROTHIC_TEAM = {nil, false}
		,CARNOR_JAX_TEAM = {"Is_Home_Planet", false}
		,CHIMERA_PELLAEON_GRAND = {nil, true}
		,CHIMERA_PELLAEON_VICE = {nil, true}
		,CORELLIAN_GUNBOAT_FERRIER = {nil, true}
		,CORRUPTER_STAR_DESTROYER = {nil, true}
		,CRIMSONSUNRISE_STAR_DESTROYER = {nil, true}
		,CRONAL_SINGULARITY = {nil, true}
		,DARRON_DIREPTION = {nil, true}
		,DEZON_CONSTRAINER = {nil, true}
		,DROST_TEAM = {"Is_Production_Planet", false}
		,ELITE_SQUADRON = {nil, true}
		,EMPERORS_REVENGE_STAR_DESTROYER = {nil, true}
		--,EMPEROR_PALPATINE_TEAM = {"Is_Home_Planet", false}
		,GENERAL_COVELL_TEAM = {nil, false}
		,GENERAL_VEERS_TEAM = {nil, false}
		,GORGON = {nil, true}
		,GRUNGER_AGGRESSOR = {nil, true}
		,IMMODET_FORTRESS_COMPANY = {nil, false}
		,ISARD_CLONE_TEAM = {"Is_Home_Planet", false}
		,JORUUS_CBOATH_TEAM = {nil, false}
		,JUDICATOR_STAR_DESTROYER = {nil, true}
		,KERMEN_BELLIGERENT = {nil, true}
		,KLEV_SILENCER7 = {nil, true}
		,KNIGHT_HAMMER = {nil, true}
		,KOOLOOTA_TEAM = {"Is_Production_Planet", false}
		,LUKE_SKYWALKER_DARKSIDE_TEAM = {"Is_Home_Planet", false}
		,LUSANKYA = {"Is_Home_Planet", true}
		,MAKATI_STEADFAST = {nil, true}
		,MANOS_TEAM = {"Is_Production_Planet", false}
		,MELVAR_TEAM = {nil, false}
		,NAVETT_TEAM = {nil, false}
		,PELLAEON_REAPER = {nil, true}
		--,PESTAGE_TEAM = {"Is_Home_Planet", false}
		,PHULIK_BINDER = {nil, true}
		,PITTA_TORPEDO_SPHERE = {nil, true}
		,PRAJI_SECUTOR = {nil, true}
		,PRENTIOCH_PRENTIOCH = {nil, true}
		,RECKONING_STAR_DESTROYER = {nil, true}
		,RELENTLESS_STAR_DESTROYER = {nil, true}
		,ROGRISS_AURORA = {nil, true}
		,ROGRISS_DOMINION = {nil, true}
		,RUKH_TEAM = {nil, false}
		,SCYLLA = {nil, true}
		,SEDRISS_TEAM = {nil, false}
		,SHOCKWAVE_STAR_DESTROYER = {nil, true}
		,SINISTER_RELENTLESS = {nil, true}
		,TAKEL_MAGICDRAGON = {nil, true}
		,TIERCE_TEAM = {nil, false}
		,VORRU_TEAM = {"Is_Production_Planet", false}
		,WINDCALLER_TEAM = {nil, false}
		,YONKA_AVARICE = {nil, true}
		,ZA_TEAM = {nil, false}
		,["13X"] = {nil, true}
		,["181ST_FEL_SQUADRON"] = {nil, true}
		,["181ST_STELE"] = {nil, true}
		,["181ST_TIE_INTERCEPTOR_SQUADRON"] = {nil, true}
		
		,ABAHT_INTREPID = {nil, true}
		,ACKBAR_GUARDIAN = {nil, true}
		,ACKDOOL_MEDIATOR = {nil, true}
		,AIREN_CRACKEN_TEAM = {"Is_Production_Planet", false}
		,BELL_ENDURANCE = {nil, true}
		,BELL_SWIFT_LIBERTY = {nil, true}
		,BRAND_INDOMITABLE = {nil, true}
		,BRAND_YALD = {nil, true}
		,BURKE_REMEMBER_ALDERAAN = {nil, true}
		,CILGHAL_TEAM = {nil, false}
		,DORAT_ARROW_OF_SULLUST = {nil, true}
		,ERRANT_VENTURE = {nil, true}
		,ERRANT_VENTURE_FULL = {nil, true}
		,ERRANT_VENTURE_TWO = {nil, true}
		,GALACTIC_VOYAGER = {"Is_Production_Planet", true}
		,GAVRISOM_TEAM = {"Is_Home_Planet", false}
		,HAN_SOLO_TEAM = {nil, false}
		,SOLO_REMONDA = {nil, true}
		,HAN_INTREPID = {nil, true}
		,HOME_ONE = {"Is_Production_Planet", true}
		,IBLIS_PEREGRINE = {nil, true}
		,IBLIS_SELONIAN_FIRE = {nil, true}
		,IBLIS_BAIL_ORGANA = {nil, true}
		,IBLIS_HARBINGER = {nil, true}
		,IILLOR_CORUSCA = {nil, true}
		,KATARN_TEAM = {nil, false}
		,KALBACK_JUSTICE = {nil, true}
		,KREFEY_RALROOST = {nil, true}
		,LANDO_CALRISSIAN_TEAM = {"Is_Home_Planet", false}
		,LUKE_SKYWALKER_JEDI_TEAM = {nil, false}
		,MASSA_LUCREHULK_AUXILIARY = {nil, true}
		,MASSA_LUCREHULK_CARRIER = {nil, true}
		,MARA_SABER_TEAM = {nil, false}
		,MILLENNIUM_FALCON = {nil, true}
		,MIRAX_TEAM = {"Is_Home_Planet", false}
		,MON_MOTHMA_TEAM = {"Is_Production_Planet", false}
		,NAMMO_DEFIANCE = {nil, true}
		,NANTZ_INDEPENDENCE = {nil, true}
		,NANTZ_FAITHFUL_WATCHMAN  = {nil, true}
		,PRINCESS_LEIA_TEAM = {"Is_Production_Planet", false}
		,PRINCESS_LEIA_TEAM_NOGHRI = {"Is_Home_Planet", false}
		,RAGAB_EMANCIPATOR  = {nil, true}
		,TALLON_SILENT_WATER  = {nil, true}
		,DRAYSON_TRUE_FIDELITY  = {nil, true}
		,DRAYSON_NEW_HOPE  = {nil, true}
		,["SALM_Y-WING_SQUADRON"]  = {nil, true}
		,["SALM_B-WING_SQUADRON"]  = {nil, true}
		,["SALM_K-WING_SQUADRON"]  = {nil, true}
		,SNUNB_ANTARES_SIX  = {nil, true}
		,SNUNB_RESOLVE  = {nil, true}
		,ROGUE_SQUADRON_SPACE = {nil, true}
		,SOVV_DAUNTLESS = {nil, true}
		,SOVV_VOICE_OF_THE_PEOPLE = {nil, true}
		,VANTAI_MOONSHADOW = {nil, true}
		,WEDGE_LUSANKYA = {"Is_Home_Planet", true}
		,WILD_KARRDE = {nil, true}
		,YONKA_FREEDOM = {nil, true}
		
		,DEKEET_PRAETOR = {nil, true}
		,DYNAMIC_BESK = {nil, true}
		,GRANT_ORIFLAMME = {nil, true}
		,GREGOR_TEAM = {"Is_Production_Planet", false}
		,JEREC_TEAM = {nil, false}
		,OTRO_ENFORCER = {nil, true}
		,REAPER_KAINE = {"Is_Home_Planet", true}
		,REIKAS_TEAM = {nil, false}
		,SARISS_TEAM = {nil, false}
		,TRELIX_INDENTURE = {nil, true}
		
		,GENDARR_RELIANCE = {nil, true}
		,GETELLES_TEAM = {"Is_Production_Planet", false}
		,LANCET_KOSH = {nil, true}
		,LARM_CARRACK = {nil, true}
		,LOTT_TEAM = {"Is_Production_Planet", false}
		,OKINS_ALLEGIANCE = {nil, true}
		,RAMIER_TEAM = {"Is_Production_Planet", false}
		,SYN_SILOOTH = {nil, true}
		,TAVIRA_INVIDIOUS = {nil, true}
		,TRIER_SECUTOR = {nil, true}
		,["13X_PELLAEON"] = {nil, true}
		,["13X_TERADOC"] = {"Is_Home_Planet", true}
		
		,BANJEER = {nil, true}
		,DEMOLISHER = {nil, true}
		,GETHZERION_TEAM = {nil, false}
		,IMPLACABLE_STAR_DESTROYER = {nil, true}
		,IRON_FIST = {nil, true}
		,JOSHI_PROVOCATEUR = {nil, true}
		,LANU_TEAM = {nil, false}
		,NABYL_HAWKBAT = {nil, true}
		,NETBERS_TEAM = {nil, false}
		,NIGHT_CALLER = {nil, true}
		,RAZORS_KISS = {"Is_Home_Planet", true}
		,TEUBBO_TEAM = {"Is_Production_Planet", false}
		,["181ST_COWALL"] = {nil, true}
		,CARVIN_TEAM = {nil, false}
		,THERBON_ALLEGIANCE = {nil, true}
		,NERVI_BLOOD_AMBITION = {nil, true}
		,RIIZOLO_NEUTRON = {nil, true}
		,NEOMEN_ION_STORM = {nil, true}

		,BOBA_FETT_TEAM = {nil, false}
		,BOSSK_TEAM = {nil, false}
		,DENGAR_TEAM = {nil, false}

		,KRIN_INVINCIBLE = {nil, true}
		,PHINEAS_VSD = {nil, true}
		,GRUMBY_NOTROPIS = {nil, true}
		,SLOANE_ENFORCE = {nil, true}
		,FASSER_TEAM = {nil, false}
		,NIELER_TEAM = {nil, false}
		,FIOLLA_TEAM = {"Is_Production_Planet", false}
		,ODUMIN_TEAM = {"Is_Production_Planet", false}
		,KARREK_FLIM_TEAM = {"Is_Production_Planet", false}

		,SONG_OF_WAR = {"Is_Home_Planet", true}
		
		--FotR heroes
		,AUTO_PROVIDENCE = {nil, true}
		,CANTEVAL_MUNIFICENT = {nil, true}
		,DELLSO_PROVIDENCE = {nil, true}
		,DOOKU_TEAM = {nil, false}
		,DUA_NINGO_UNREPENTANT = {nil, true}
		,CALLI_TRILM_BULWARK = {nil, true}
		,DURD_TEAM = {nil, false}
		,GUNRAY_TEAM = {"Is_Production_Planet", false}
		,HARSOL_MUNIFICENT = {nil, true}
		,SEVRANCE_TEAM = {nil, false}
		,SORA_BULQ_TEAM = {nil, false}
		,TF1726_MUNIFICENT = {nil, true}
		,KALANI_TEAM = {nil, false}
		,TAMBOR_TEAM = {"Is_Production_Planet", false}
		,STARK_RECUSANT = {nil, true}
		,SHONN_RECUSANT = {nil, true}
		,TREETOR_CAPTOR = {nil, true}
		,TONITH_CORPULENTUS = {nil, true}
		,TRENCH_INVINCIBLE = {nil, true}
		,TRENCH_GENERIC = {nil, true}
		,TUUK_PROCURER = {nil, true}
		,VENTRESS_TEAM = {nil, false}
		,WHORM_TEAM = {nil, false}
		,GRIEVOUS_TEAM = {nil, true}
		,GRIEVOUS_TEAM_MUNIFICENT = {nil, true}
		,GRIEVOUS_TEAM_MALEVOLENCE = {nil, true}
		,GRIEVOUS_TEAM_RECUSANT = {nil, true}
		
		,CODY_TEAM = {nil, false}
		,CODY2_TEAM = {nil, false}
		,REX_TEAM = {nil, false}
		,REX2_TEAM = {nil, false}
		,FORDO_TEAM = {nil, false}
		,FORDO2_TEAM = {nil, false}
		,YODA_TEAM = {nil, false}
		,ROMODI_TEAM = {nil, false}
		,OZZEL_TEAM = {nil, false}
		,DELTA_SQUAD = {nil, false}
		,MACE_WINDU_TEAM = {nil, true}
		,COY_IMPERATOR = {nil, true}
		,GRANT_VENATOR = {nil, true}
		,DAO_VENATOR = {nil, true}
		,DENIMOOR_TENACIOUS = {nil, true}
		,WESSEX_REDOUBT = {nil, true}
		,DRON_VENATOR = {nil, true}
		,AUTEM_VENATOR = {nil, true}
		,TRACHTA_VENATOR = {nil, true}
		,WIELER_RESILIENT = {nil, true}
		,COBURN_TRIUMPHANT = {nil, true}
		,YULAREN_RESOLUTE = {nil, true}
		,TARKIN_VENATOR = {nil, true}
		,KILIAN_ENDURANCE = {nil, true}
		,PELLAEON_LEVELER = {nil, true}
		,SCREED_ARLIONNE = {nil, true}
		,DODONNA_ARDENT = {nil, true}
		,DALLIN_KEBIR = {nil, true}
		,OBI_WAN_TEAM = {nil, false}
		,PLO_KOON_TEAM = {nil, false}
		,ANAKIN_TEAM = {nil, false}
		,KIT_FISTO_TEAM = {nil, false}
		,AAYLA_SECURA_TEAM = {nil, false}
		,SHAAK_TI_TEAM = {nil, false}
		,GENTIS_TEAM  = {nil, false}
		,MULLEEN_IMPERATOR = {nil, true}
		,TARKIN_EXECUTRIX = {nil, true}
		,RAYMUS_TANTIVE = {nil, true}
		,TALLON_SUNDIVER = {nil, true}
		,TALLON_BATTALION = {nil, true}
		,VADER_TEAM = {nil, false}
		,EMPEROR_PALPATINE_TEAM = {"Is_Home_Planet", false}
		,MON_MOTHMA_TEAM = {"Is_Production_Planet", false}
		,BAIL_TEAM = {"Is_Production_Planet", false}
		,PESTAGE_TEAM = {"Is_Production_Planet", false}
	}
	
end

function Find_Custom_Target(object)
	object_type = object.Get_Type()
	object_type_name = object_type.Get_Name()

	unit_entry = CustomUnitPlacement[object_type_name]

	if unit_entry then
		perception = unit_entry[1]
		prefers_space = unit_entry[2]
		if perception then
			target = FindTarget.Reachable_Target(PlayerObject, perception, "Friendly", "No_Threat", 1.0, object)
			if TestValid(target) then
				return target
			end
		end
		
		if prefers_space then
			return Find_Space_Unit_Target(object)
		else
			return Find_Ground_Unit_Target(object)
		end
	else
		DebugMessage("%s -- Error: Type %s not found in CustomUnitPlacement table.", tostring(Script), object_type_name)
	end
end
	

