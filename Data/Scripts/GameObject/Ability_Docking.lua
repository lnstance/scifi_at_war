--***********************************************************************************
--*   Trek Wars
--*                                    
--*   @Author:              Imperial
--*   @Date:                
--*   @Project:             Trek Wars
--*   @Last modified by:    Imperial
--*   @License:             Feel free to use
--*   @Copyright:           © Imperial
--***********************************************************************************
-- require("PGBase")
-- require("PGStoryMode")
require("PGStateMachine")

require("Scifi_At_War_Library")
require("Unit_Docking")


--================================= Base Definitions =================================
function Definitions()
    -- DebugMessage("%s -- In Definitions", tostring(Script))
    -- ServiceRate = 0.01
     
    Define_State("State_Init", State_Init);
    Define_State("State_Ability_1", State_Ability_1)
    
    ability_1 = "SPOILER_LOCK"
    auto_join = false
    
    
    -- Script is going to load from top most first, before it tries the lower indexes.
    -- They need to be in capital letters or the script will crash!!  
    Unit_List = 
    { "CONSTITUTION",
      "EXCELSIOR",
      "GALAXY",    
      "PROMETHEUS",     
      "PROMETHEUS",
      "PROMETHEUS",      
      "PROMETHEUS_ALPHA_BETA",
      "PROMETHEUS_BETA_GAMMA",                      
    }
        
    Part_List =                        
    {  { "CONSTITUTION_SAUCER", "CONSTITUTION_STARDRIVE" }, 
       { "EXCELSIOR_SAUCER", "EXCELSIOR_STARDRIVE" },   
       { "GALAXY_SAUCER", "GALAXY_STARDRIVE" },       
       { "PROMETHEUS_ALPHA", "PROMETHEUS_BETA", "PROMETHEUS_GAMMA" },        
       { "PROMETHEUS_ALPHA_BETA", "PROMETHEUS_GAMMA" },              
       { "PROMETHEUS_ALPHA", "PROMETHEUS_BETA_GAMMA"},        
       { "PROMETHEUS_ALPHA", "PROMETHEUS_BETA" }, -- Semi Fusion
       { "PROMETHEUS_BETA", "PROMETHEUS_GAMMA" }, -- Semi Fusion                    
    }                                                   
end

--==================================== State Init ====================================
function State_Init(message)                           

    if message == OnEnter then
		
        if Get_Game_Mode() ~= "Space" then
			ScriptExit()
		end  
        
   
    elseif message == OnUpdate then 
        
        if Object.Is_Ability_Active(ability_1) then 
            Set_Next_State("State_Ability_1")
            
        elseif auto_join then 
            Sleep(1)          
            Set_Next_State("State_Ability_1")
        end
        	
    end
end


--===================================== Ability 1 ====================================   .Get_Bone_Position(X)
function State_Ability_1(message)
    if message == OnEnter then 
          
                 
        --================== Seperate ===================   
                                         
        for i, Full_Unit in pairs(Unit_List) do                      
            if Object.Get_Type().Get_Name() == Full_Unit then                     
                -- Object.Play_Animation("Deploy", true, 0) 
                
                -- This long the animation takes to unfold
                Sleep (0.8) 
                
                if Seperate(Part_List[i]) then -- Called from the "Unit_Docking" Library
                    Object.Despawn() -- No longer needed   
                    ScriptExit() 
                end
            end 
        end
                                                    
        --================== Join ===================                                     

        for i, Parts in pairs(Part_List) do                                   
            if Match(Parts, Object.Get_Type().Get_Name()) then                                             
                
                -- If this returns false it means it continues to the next entry, because the last member is not nearby            
                if Join_Unit(Parts, Unit_List[i]) then 
                    Object.Despawn() -- Only if joined successfully
                    ScriptExit() 
                end               
            end      
        end                                          
                                
                             
    Set_Next_State("State_Init")                                     
   end    
end 


--==================================== End of File ===================================