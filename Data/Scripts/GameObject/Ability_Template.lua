--***********************************************************************************
--*   Trek Wars
--*                                    
--*   @Author:              Imperial
--*   @Date:                
--*   @Project:             Trek Wars
--*   @Last modified by:    Imperial
--*   @License:             Feel free to use
--*   @Copyright:           © Imperial
--***********************************************************************************
-- require("PGBase")
-- require("PGStoryMode")
require("PGStateMachine")
require("Scifi_At_War_Library")


--================================= Base Definitions =================================
function Definitions()
    -- DebugMessage("%s -- In Definitions", tostring(Script))
    -- ServiceRate = 0.01
    
    Define_State("State_Init", State_Init);
    Define_State("State_Ability_1", State_Ability_1)
    Define_State("State_Ability_2", State_Ability_2)
    
    ability_1 = "SPOILER_LOCK"
    ability_2 = "SPOILER_LOCK"                                                     
end

--==================================== State Init ====================================
function State_Init(message)
    if message == OnEnter then
		
        if Get_Game_Mode() ~= "Space" then
			ScriptExit()
		end  
        
           
    elseif message == OnUpdate then 
        
        if Object.Is_Ability_Active(ability_1) then 
            Set_Next_State("State_Ability_1")            
        end       	
    end
end

--===================================== Ability 1 ====================================  
function State_Ability_1(message)
    if message == OnEnter then 
          
                                  
                             
    Set_Next_State("State_Init")                                     
   end    
end 


--===================================== Ability 2 ====================================
function State_Ability_2(message)
   if message == OnEnter then 
   
         
                      
      Set_Next_State("State_Init")                    
   end    
end     

--==================================== End of File ===================================