--******************************************************************************
--     _______ __
--    |_     _|  |--.----.---.-.--.--.--.-----.-----.
--      |   | |     |   _|  _  |  |  |  |     |__ --|
--      |___| |__|__|__| |___._|________|__|__|_____|
--     ______
--    |   __ \.-----.--.--.-----.-----.-----.-----.
--    |      <|  -__|  |  |  -__|     |  _  |  -__|
--    |___|__||_____|\___/|_____|__|__|___  |_____|
--                                    |_____|
--*   @Author:              [TR]Pox
--*   @Date:                2018-03-20T01:27:01+01:00
--*   @Project:             Imperial Civil War
--*   @Filename:            Deconstruction.lua
--*   @Last modified by:    [TR]Pox
--*   @Last modified time:  2018-03-26T09:58:14+02:00
--*   @License:             This source code may only be used with explicit permission from the developers
--*   @Copyright:           © TR: Imperial Civil War Development Team
--******************************************************************************

require("PGBase")
require("PGStateMachine")
require("PGStoryMode")
require("PGSpawnUnits")

function Definitions()
    DebugMessage("%s -- In Definitions", tostring(Script))

    Define_State("State_Init", State_Init);
	
	local timervalue;
	local space_max;
	local ground_max;
end


function State_Init(message)
    if message == OnEnter then
		local influence_level = check_influence(Object.Get_Planet_Location(), Object.Get_Owner())
		
		if influence_level == 0 then
			influence_level = 4
		end
		
		Register_Timer(CadetLoop, timervalue, Object)
    end
end

function check_influence(academy_planet, academy_owner)
	local influence_level = EvaluatePerception("Planet_Influence_Value", academy_owner, academy_planet)

	if influence_level == 10 then
		timervalue = 200
		space_max = 5
		ground_max = 4
	elseif influence_level == 9 then
		timervalue = 220
		space_max = 5
		ground_max = 4
	elseif influence_level == 8 then
		timervalue = 240
		space_max = 4
		ground_max = 3
	elseif influence_level == 7 then
		timervalue = 260
		space_max = 4
		ground_max = 3
	elseif influence_level == 6 then
		timervalue = 280
		space_max = 4
		ground_max = 3
	elseif influence_level == 5 then
		timervalue = 300
		space_max = 3
		ground_max = 2
	elseif influence_level == 4 then
		timervalue = 320
		space_max = 3
		ground_max = 2
	else
		influence_level = 0 --Don't bother producing any below this if the planet dislikes you that much
		timervalue = 340
	end

	return influence_level
end

function CadetLoop(Academy)
	local academy_planet = Academy.Get_Planet_Location()
	local academy_owner = Academy.Get_Owner()
	local influence_level = 0
	
	local fleet_commanders
	local ground_commanders
	
	local faction = Academy.Get_Owner()
	
	if faction == Find_Player("Rebel") then
		fleet_commanders = {"Commander_Dreadnaught_IV_Rebel", "Commander_Liberty_V", "Commander_Dauntless_V", "Commander_MC40_IV", "Commander_Assault_Frigate_III" }
		ground_commanders = {"Commander_T1B_IV_Company", "Commander_T1B_IV_Company", "Commander_T1B_III_Company", "Commander_T1B_III_Company" }
	elseif faction == Find_Player("Teradoc") then
		fleet_commanders = {"Commander_Imperial_IV", "Commander_Procursator_V", "Commander_CCVSD_V", "Commander_Dreadnaught_IV", "Commander_Strike_III" }
		ground_commanders = {"Commander_Chariot_IV_Company", "Commander_Chariot_IV_Company", "Commander_Chariot_III_Company", "Commander_Chariot_III_Company" }
	elseif faction == Find_Player("Hutts") then
		fleet_commanders = {"Commander_Imperial_IV", "Commander_VSD1_V", "Commander_VSD2_V", "Commander_Broadside_IV", "Commander_Vindicator_III" }
		ground_commanders = {"Commander_Chariot_IV_Company", "Commander_Chariot_IV_Company", "Commander_Chariot_III_Company", "Commander_Chariot_III_Company" }
	elseif faction == Find_Player("Pentastar") then
		fleet_commanders = {"Commander_Acclamator_Level_IV", "Commander_Venator_V", "Commander_VSD2_V", "Commander_Dreadnaught_IV", "Commander_Enforcer_III" }
		ground_commanders = {"Commander_Chariot_IV_Company", "Commander_Chariot_IV_Company", "Commander_Chariot_III_Company", "Commander_Chariot_III_Company" }
	elseif faction == Find_Player("Pirates") then
		fleet_commanders = {"Commander_Dreadnaught_IV", "Commander_VSD1_V", "Commander_VSD2_V", "Commander_Neutron_IV", "Commander_Gladiator2_III" }
		ground_commanders = {"Commander_Chariot_IV_Company", "Commander_Chariot_IV_Company", "Commander_Chariot_III_Company", "Commander_Chariot_III_Company" }
	elseif faction == Find_Player("Corporate_Sector") then
		fleet_commanders = {"Commander_Dreadnaught_IV_Rebel", "Commander_VSD1_V", "Commander_Dauntless_V", "Commander_Neutron_IV", "Commander_Gladiator_III" }
		ground_commanders = {"Commander_X10_IV_Company", "Commander_X10_IV_Company", "Commander_X10_III_Company", "Commander_X10_III_Company" }
	elseif faction == Find_Player("EmpireoftheHand") then
		fleet_commanders = {"Commander_Kynigos_IV", "Commander_Chaf_V", "Commander_VSD1_V", "Commander_Dreadnaught_IV", "Commander_Rohkea_III" }
		ground_commanders = {"Commander_Kirov_IV_Company", "Commander_Chariot_IV_Company", "Commander_Kirov_III_Company", "Commander_Kirov_III_Company" }
	else --Empire, but a default case is good practice
		fleet_commanders = {"Commander_Dreadnaught_IV", "Commander_VSD1_V", "Commander_VSD2_V", "Commander_Broadside_IV", "Commander_Vindicator_III" }
		ground_commanders = {"Commander_Chariot_IV_Company", "Commander_Chariot_IV_Company", "Commander_Chariot_III_Company", "Commander_Chariot_III_Company" }
	end
	
	influence_level = check_influence(academy_planet, academy_owner)
	
	local rando_space = GameRandom(1, space_max)
	local rando_land = GameRandom(1, ground_max)
	
	if influence_level > 0 then
		local cadet_list = SpawnList({ fleet_commanders[rando_space] }, academy_planet, academy_owner,true,false)
		local cadet_list2 = SpawnList({ ground_commanders[rando_land] }, academy_planet, academy_owner,true,false)
	end
	
	Register_Timer(CadetLoop, timervalue, Academy)
end