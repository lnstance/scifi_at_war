require("PGStateMachine")
require("PGStoryMode")

function Definitions()

	DebugMessage("%s -- In Definitions", tostring(Script))

	Define_State("State_Init", State_Init);

end

function State_Init(message)
	if message == OnEnter then
		if Get_Game_Mode() == "Land" then

			if Find_First_Object("Base Shield Structure Position") then
			structure = Find_First_Object("Attacker Entry Position")
			pad_list = Find_All_Objects_Of_Type("Attacker Entry Position")
				for i,pad in pairs(pad_list) do
				Create_Generic_Object("Field_Base_Pad", pad, structure.Get_Owner())
				end
			end	
		end
	end
end