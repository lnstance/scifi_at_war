--******************************************************************************
--     _______ __
--    |_     _|  |--.----.---.-.--.--.--.-----.-----.
--      |   | |     |   _|  _  |  |  |  |     |__ --|
--      |___| |__|__|__| |___._|________|__|__|_____|
--     ______
--    |   __ \.-----.--.--.-----.-----.-----.-----.
--    |      <|  -__|  |  |  -__|     |  _  |  -__|
--    |___|__||_____|\___/|_____|__|__|___  |_____|
--                                    |_____|
--*   @Author:              [TR]Jorritkarwehr
--*   @Date:                2018-03-20T01:27:01+01:00
--*   @Project:             Imperial Civil War
--*   @Filename:            UnitSwitcher.lua
--*   @Last modified by:    Nojembre
--*   @Last modified time:  
--*   @License:             This source code may only be used with explicit permission from the developers
--*   @Copyright:           © TR: Imperial Civil War Development Team
--******************************************************************************


require("PGStateMachine")
require("PGSpawnUnits")
require("UnitSwitcherLibrary")

function Definitions()
    DebugMessage("%s -- In Definitions", tostring(Script))

    Define_State("State_Init", State_Init);
end


function State_Init(message)
    if message == OnEnter then	
		if Get_Game_Mode() ~= "Galactic" then
        ScriptExit()
    end
	
	local swap_entry = Get_Swap_Entry(tostring(Object.Get_Type().Get_Name()))
	
	if swap_entry == nil then
		Object.Despawn()
		ScriptExit()
	end
	
	local old_unit = swap_entry[2]
	local new_unit = swap_entry[3]
	
	local locale = Object.Get_Planet_Location()

	checkObject = Find_First_Object(old_unit)
	
	if TestValid(checkObject) then
		checkObject.Despawn()
		spawn_list_new = { new_unit }
		ReplaceSpawn = SpawnList(spawn_list_new, locale, Object.Get_Owner(),true,false)
	end
	
	Object.Despawn()
	
    ScriptExit()
    end
end
