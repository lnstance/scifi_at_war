require("PGBase")
require("PGStateMachine")
require("PGStoryMode")
require("PGSpawnUnits")

function Definitions()
    DebugMessage("%s -- In Definitions", tostring(Script))

    Define_State("State_Init", State_Init);
end

function State_Init(message)
    if Get_Game_Mode() ~= "Land" then
		ScriptExit()
	end
	
	if Object.Get_Owner() == Find_Player("Empire") then	
		if TestValid(Find_First_Object("Pentastar_MoffPalace")) then
			Object.Change_Owner(Find_Player("Pentastar"))
		end
		
		if TestValid(Find_First_Object("Zsinj_MoffPalace")) then
			Object.Change_Owner(Find_Player("Pirates"))
		end
		
		if TestValid(Find_First_Object("Maldrood_MoffPalace")) then
			Object.Change_Owner(Find_Player("Teradoc"))
		end
		
		if TestValid(Find_First_Object("Eriadu_MoffPalace")) then
			Object.Change_Owner(Find_Player("Hutts"))
		end
	end
	
	ScriptExit()
end