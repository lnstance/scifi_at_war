--==================================================================================================
--************************************** Created by Imperial ***************************************
--==================================================================================================
require("PGStateMachine")


function Definitions()
    
    Movie_List = {"Movie_1", "Movie_2"} 
    Play_Time = {10, 20}  -- Slots correspond to .bik movie in Movie_List above. 
    
    -- 99 means infinite cycling.
    Play_Cycles = 99 
    Transition_Time = 0     
    Despawn_When_Done = false 
                         
                  
                            
               
    -- Don't touch these below: 
    Define_State("State_Init", State_Init) 
    Movie_Count = 1
    Sleep_Seconds = 1
    Current_Movie = 0
     
    -- ServiceRate defines how fast the "OnUpdate" states will refresh, 0.1 means 1 second and 0.05 is half a second..
    -- Without writing this variable, the game uses a stock value of 0.05 to refresh twice a second 
    ServiceRate = 0.1
end


--================================= States =================================
function State_Init(message)
    if message == OnEnter then
   
        -- Use  ~= "Galactic" "Space" or "Land".  Game Menu is "Space" 
        if Get_Game_Mode() == "Land" then 
           ScriptExit()
        end     

        Movie_Count = table.getn(Movie_List) 
        Finished = false
           
   
                              
    --============ On Update ============  
    elseif message == OnUpdate then 
        Sleep(Sleep_Seconds + Transition_Time)       
        
        Stop_Bink_Movie() -- Necessary!
             
             
        if Movie_Count < 2 then -- needs to be 1
            Current_Movie = 1  
            Play_Cycles = Play_Cycles - 1     
        else                     
            Current_Movie = Current_Movie + 1
                               
            if  Current_Movie > Movie_Count then -- Completed a Cycle
                Current_Movie = 1 -- Resetting to 1
                
                if Play_Cycles ~= 99 then                  
                    Play_Cycles = Play_Cycles - 1
                    
                    if Play_Cycles < 1 then                      
                        if Despawn_When_Done then          
                            Object.Despawn() 
                        else 
                            ScriptExit() 
                        end                      
                    end                    
                end                                                                            
            end                                                             
        end       
        
                     
        Play_Movie(Current_Movie)  
        
        if Play_Cycles < 1 then -- Last round plays the full playtime
            if Despawn_When_Done then          
                Object.Despawn() 
            else 
                ScriptExit() 
            end     
        end            
	end
end



--================================= Functions =================================         
function Play_Movie(Index_Number)
   -- Adjusts Sleep_Seconds for each Video to correct time according to table above   
   Sleep_Seconds = Play_Time[Index_Number]        
   Play_Bink_Movie(Movie_List[Index_Number])   
end


--================================= End of File =================================

