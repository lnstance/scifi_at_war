--***********************************************************************************
--*   Trek Wars
--*                                    
--*   @Author:              Imperial
--*   @Date:                
--*   @Project:             Trek Wars and Scifi at War
--*   @Last modified by:    Imperial
--*   @License:             Feel free to use
--*   @Copyright:           © Imperial
--***********************************************************************************
require("PGSpawnUnits")


--===================================== Functions ====================================

function Table_Name(tbl)
assert(type(tbl)=="table", "tbl must be a table")
    for n, v in getfenv() do
        if v == tbl then
            return n
        end
    end
    return nil
end


function getTableName(tbl)
  for k, v in pairs(_G) do
    if v == tbl then
          return k
    end
  end
  return nil
end

--================== Is Match ===================    
function Match(List, Item)
  for each, String_Name in pairs(List) do
    if String_Name == Item then
      return true
    end
  end
  return false
end

--================== GC Spawn List ===================         
function GC_Spawn_List(List, Position, Player, Spawn_Over_Target) -- return is missing
    Unit_List = {}
               
    for each, Unit in pairs(List) do            
        local Entity = GC_Spawn_Unit(Unit, Position, Player, Spawn_Over_Target)
          
        if Entity ~= nil then 
            table.insert(Unit_List, Entity) 
        end      
    end    
    return Unit_List
end
                                     
--================== GC Spawn Unit =================== 
function GC_Spawn_Unit(Unit, Position, Player, Spawn_Over_Target)
 
    local Unit_Name = ""
    
 	if type(Unit) == "string" then
		Unit_Name = Unit
        Unit = Find_Object_Type(Unit)
    else
        Unit_Name = Unit.Get_Type().Get_Name()
	end
     
    Reinforce_Unit(Unit, Position, Player, false, true)
    Spawn_From_Reinforcement_Pool(Unit, Position, Player)          
    local Spawned_Unit = Find_Nearest(Position, Unit_Name, Player, true)  
  
       
    if Spawn_Over_Target and Spawned_Unit ~= nil then
        Spawned_Unit.Teleport_And_Face(Position) 
    end  
    return Spawned_Unit
end   
          
            
--==================================== End of File ===================================