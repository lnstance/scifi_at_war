--******************************************************************************
--     _______ __
--    |_     _|  |--.----.---.-.--.--.--.-----.-----.
--      |   | |     |   _|  _  |  |  |  |     |__ --|
--      |___| |__|__|__| |___._|________|__|__|_____|
--     ______
--    |   __ \.-----.--.--.-----.-----.-----.-----.
--    |      <|  -__|  |  |  -__|     |  _  |  -__|
--    |___|__||_____|\___/|_____|__|__|___  |_____|
--                                    |_____|
--*   @Author:              [TR]Jorritkarwehr
--*   @Date:                2018-03-20T01:27:01+01:00
--*   @Project:             Imperial Civil War
--*   @Filename:            SetFighterResearch.lua
--*   @Last modified by:    [TR]Jorritkarwehr
--*   @Last modified time:  2020-05-21T09:58:14+02:00
--*   @License:             This source code may only be used with explicit permission from the developers
--*   @Copyright:           © TR: Imperial Civil War Development Team
--******************************************************************************



function Set_Fighter_Research(rtype)
	local levels = GlobalValue.Get("FIGHTER_RESEARCH")
	if levels == nil then
		levels = {}
	end
	for index, obj in pairs(levels) do
		if obj == rtype then
			return
		end
	end
	table.insert(levels, rtype)
	GlobalValue.Set("FIGHTER_RESEARCH", levels)
end

function Clear_Fighter_Research(rtype)
	local levels = GlobalValue.Get("FIGHTER_RESEARCH")
	if levels == nil then
		return
	end
	for index, obj in pairs(levels) do
		if obj == rtype then
			table.remove(levels, index)
			GlobalValue.Set("FIGHTER_RESEARCH", levels)
			return
		end
	end
end

function Get_Fighter_Research(rtype)
	local levels = GlobalValue.Get("FIGHTER_RESEARCH")
	if levels == nil then
		return false
	end
	for index, obj in pairs(levels) do
		if obj == rtype then
			return true
		end
	end
	return false
end