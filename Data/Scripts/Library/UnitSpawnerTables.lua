function DefineUnitTable(faction)

    if faction == Find_Player("Empire") then
	    Unit_Table = {
            {Find_Object_Type("Generic_Venator"), 3, "Space"}
			,{Find_Object_Type("IPV1_System_Patrol_Craft"), 5, "Space"}			
            ,{Find_Object_Type("Lancer_Frigate"), 5, "Space"}
            ,{Find_Object_Type("Escort_Carrier"), 5, "Space"}
            ,{Find_Object_Type("Carrack_Cruiser"), 5, "Space"}
            ,{Find_Object_Type("Strike_Cruiser"), 4, "Space"}
            ,{Find_Object_Type("Vindicator_Cruiser"), 4, "Space"}
            ,{Find_Object_Type("Dreadnaught_Empire"), 4, "Space"}
            ,{Find_Object_Type("Star_Galleon"), 2, "Space"}
            ,{Find_Object_Type("Generic_Victory_Destroyer"), 3, "Space"}
            ,{Find_Object_Type("Generic_Victory_Destroyer_Two"), 3, "Space"}
            ,{Find_Object_Type("Generic_Star_Destroyer_Two"), 1, "Space"}
            ,{Find_Object_Type("Generic_Star_Destroyer"), 1, "Space"}
            ,{Find_Object_Type("Generic_Dominator"), 1, "Space"}
            ,{Find_Object_Type("Army_Trooper_Squad"), 4, "Land"}			
            ,{Find_Object_Type("Imperial_Stormtrooper_Squad"), 1, "Land"}
            ,{Find_Object_Type("Imperial_Anti_Infantry_Brigade"), 3, "Land"}
            ,{Find_Object_Type("Imperial_Heavy_Scout_Squad"), 2, "Land"}
            ,{Find_Object_Type("Chariot_LAV_Company"), 2, "Land"}			
            ,{Find_Object_Type("Imperial_Heavy_Assault_Company"), 1, "Land"}
        }
    elseif faction == Find_Player("Rebel") then
        Unit_Table = {
            {Find_Object_Type("Calamari_Cruiser"), 2, "Space"}
            ,{Find_Object_Type("Quaser"), 5, "Space"}
            ,{Find_Object_Type("Nebulon_B_Frigate"), 5, "Space"}
            ,{Find_Object_Type("Alliance_Assault_Frigate"), 4, "Space"}
            ,{Find_Object_Type("Dreadnaught_Rebel"), 4, "Space"}
            ,{Find_Object_Type("Liberator_Cruiser"), 2, "Space"}
            ,{Find_Object_Type("Corellian_Corvette"), 5, "Space"}
            ,{Find_Object_Type("Corellian_Gunboat"), 5, "Space"}
            ,{Find_Object_Type("Marauder_Missile_Cruiser"), 5, "Space"}
            ,{Find_Object_Type("Dauntless"), 2, "Space"}
            ,{Find_Object_Type("Nebulon_B_Tender"), 2, "Space"}			
            ,{Find_Object_Type("MC80B"), 1, "Space"}
            ,{Find_Object_Type("MC40a"), 3, "Space"}
            ,{Find_Object_Type("MC30c"), 3, "Space"}
            ,{Find_Object_Type("Rebel_Infantry_Squad"), 3, "Land"}
            ,{Find_Object_Type("Rebel_Light_Tank_Brigade"), 1, "Land"}
            ,{Find_Object_Type("Rebel_Light_Tank_Brigade_T1"), 1, "Land"}
            ,{Find_Object_Type("Rebel_RAF_Brigade"), 1, "Land"}
            ,{Find_Object_Type("Tracker_Company"), 1, "Land"}
            }
	elseif faction == Find_Player("Hapes_Consortium") then
        Unit_Table = {
            {Find_Object_Type("BattleDragon"), 3, "Space"}
            ,{Find_Object_Type("Nova_Cruiser"), 4, "Space"}
            ,{Find_Object_Type("Beta_Cruiser"), 4, "Space"}
            ,{Find_Object_Type("Hapan_Infantry_Squad"), 3, "Land"}
            ,{Find_Object_Type("Hapan_Light_Missile_Brigade"), 1, "Land"}
            }
    elseif faction == Find_Player("EmpireoftheHand") then
        Unit_Table = {
            {Find_Object_Type("Chaf_Destroyer"), 3, "Space"}
            ,{Find_Object_Type("Fruoro"), 5, "Space"}
            ,{Find_Object_Type("Syndic_Destroyer"), 5, "Space"}
            ,{Find_Object_Type("Phalanx_Destroyer"), 2, "Space"}
            ,{Find_Object_Type("Peltast"), 2, "Space"}
            ,{Find_Object_Type("Ormos"), 4, "Space"}
            ,{Find_Object_Type("Muqaraea"), 5, "Space"}
            ,{Find_Object_Type("Rohkea"), 4, "Space"}
            ,{Find_Object_Type("Kuuro"), 5, "Space"}
            ,{Find_Object_Type("Phalanx_Trooper_Squad"), 5, "Land"}
            ,{Find_Object_Type("EotH_Kirov_Brigade"), 2, "Land"}
            ,{Find_Object_Type("MMT_Brigade"), 3, "Land"}
            ,{Find_Object_Type("Flame_Tank_Company"), 1, "Land"}
            ,{Find_Object_Type("RFT_Brigade"), 2, "Land"}
            ,{Find_Object_Type("EotH_Scout_Brigade"), 3, "Land"}
            ,{Find_Object_Type("Gilzean_Brigade"), 1, "Land"}
            }
    elseif faction == Find_Player("Hutts") then            
        Unit_Table = {
            {Find_Object_Type("Raider_Pentastar"), 3, "Space"}
            ,{Find_Object_Type("Lancer_Frigate"), 5, "Space"}			
            ,{Find_Object_Type("Vigil"), 4, "Space"}
            ,{Find_Object_Type("Broadside_Cruiser"), 4, "Space"}
            ,{Find_Object_Type("Vindicator_Cruiser"), 4, "Space"}
			,{Find_Object_Type("Escort_Carrier"), 4, "Space"}
            ,{Find_Object_Type("Star_Galleon"), 2, "Space"}			
            ,{Find_Object_Type("Generic_Imperial_II_Frigate"), 4, "Space"}
            ,{Find_Object_Type("Generic_Victory_Destroyer"), 3, "Space"}
            ,{Find_Object_Type("Generic_Victory_Destroyer_TWO"), 3, "Space"}
            ,{Find_Object_Type("Generic_Star_Destroyer_Two"), 1, "Space"}
            ,{Find_Object_Type("Generic_Star_Destroyer"), 1, "Space"}
			,{Find_Object_Type("Generic_Tector"), 1, "Space"}
            ,{Find_Object_Type("Delvardus_A6_Juggernaut_Company"), 2, "Land"}
            ,{Find_Object_Type("Delvardus_Army_Trooper_Squad"), 5, "Land"}
            ,{Find_Object_Type("Fortress_Company_Delvardus"), 3, "Land"}
            ,{Find_Object_Type("Delvardus_IDT_Squad"), 2, "Land"}
            }
    elseif faction == Find_Player("Pentastar") then           
        Unit_Table = {
            {Find_Object_Type("Raider_Pentastar"), 3, "Space"}
            ,{Find_Object_Type("Arquitens"), 5, "Space"}
            ,{Find_Object_Type("Munificent"), 4, "Space"}
			,{Find_Object_Type("Victory_II_Frigate"), 4, "Space"}
			,{Find_Object_Type("Generic_Gladiator"), 4, "Space"}
            ,{Find_Object_Type("Galleon"), 2, "Space"}			
            ,{Find_Object_Type("Enforcer"), 4, "Space"}
			,{Find_Object_Type("Generic_Acclamator_Assault_Ship_Leveler"), 3, "Space"}
			,{Find_Object_Type("Generic_Venator"), 3, "Space"}
            ,{Find_Object_Type("Generic_Procursator"), 3, "Space"}
            ,{Find_Object_Type("Generic_Victory_Destroyer_TWO"), 3, "Space"}
            ,{Find_Object_Type("Generic_Star_Destroyer_Two"), 1, "Space"}
            ,{Find_Object_Type("Generic_Star_Destroyer"), 1, "Space"}
            ,{Find_Object_Type("Pentastar_Army_Trooper_Squad"), 5, "Land"}
            ,{Find_Object_Type("Pentastar_Walker_Group"), 2, "Land"}
            ,{Find_Object_Type("Pentastar_Repulsor_Scout_Group"), 4, "Land"}
            ,{Find_Object_Type("Pentastar_Speeder_Group"), 1, "Land"}
            }
	elseif faction == Find_Player("Pirates") then            
        Unit_Table = {
            {Find_Object_Type("CR90_Zsinj"), 5, "Space"}
            ,{Find_Object_Type("Nebulon_B_Zsinj"), 5, "Space"}
            ,{Find_Object_Type("Quasar_Zsinj"), 5, "Space"}
			,{Find_Object_Type("Neutron_Star"), 4, "Space"}
            ,{Find_Object_Type("Generic_Gladiator_Two"), 4, "Space"}
            ,{Find_Object_Type("Dreadnaught_Empire"), 4, "Space"}
            ,{Find_Object_Type("Star_Galleon"), 2, "Space"}			
            ,{Find_Object_Type("Generic_Victory_Destroyer"), 3, "Space"}
            ,{Find_Object_Type("Generic_Victory_Destroyer_TWO"), 3, "Space"}
            ,{Find_Object_Type("Generic_Star_Destroyer_Two"), 1, "Space"}
            ,{Find_Object_Type("Generic_Star_Destroyer"), 1, "Space"}
			,{Find_Object_Type("Generic_Dominator"), 1, "Space"}
            ,{Find_Object_Type("Zsinj_A5_Juggernaut_Company"), 2, "Land"}
            ,{Find_Object_Type("Zsinj_Raptor_Squad"), 5, "Land"}
            ,{Find_Object_Type("Zsinj_Armor_Group"), 3, "Land"}
            ,{Find_Object_Type("Zsinj_PX10_Company"), 3, "Land"}
            }
	elseif faction == Find_Player("Teradoc") then            
        Unit_Table = {
            {Find_Object_Type("Tartan_Patrol_Cruiser"), 5, "Space"}
			,{Find_Object_Type("IPV1_System_Patrol_Craft"), 5, "Space"}
            ,{Find_Object_Type("Carrack_Cruiser"), 5, "Space"}
            ,{Find_Object_Type("Broadside_Cruiser"), 4, "Space"}
			,{Find_Object_Type("Escort_Carrier"), 4, "Space"}
            ,{Find_Object_Type("Strike_Cruiser"), 4, "Space"}
            ,{Find_Object_Type("Star_Galleon"), 2, "Space"}			
            ,{Find_Object_Type("Generic_Imperial_II_Frigate"), 4, "Space"}
			,{Find_Object_Type("Generic_Procursator"), 3, "Space"}
            ,{Find_Object_Type("Generic_Victory_Destroyer"), 3, "Space"}
            ,{Find_Object_Type("Crimson_Victory"), 3, "Space"}
            ,{Find_Object_Type("Generic_Star_Destroyer_Two"), 1, "Space"}
            ,{Find_Object_Type("Generic_Star_Destroyer"), 1, "Space"}
            ,{Find_Object_Type("PX4_Company_Teradoc"), 2, "Land"}
            ,{Find_Object_Type("Teradoc_trooper_Squad"), 5, "Land"}
            ,{Find_Object_Type("Teradoc_SA5_Group"), 2, "Land"}
            ,{Find_Object_Type("Teradoc_ATPT_Squad"), 3, "Land"}
            }
    elseif faction == Find_Player("Corporate_Sector") then       
        Unit_Table = {
            {Find_Object_Type("Citadel_Cruiser_Squadron"), 4, "Space"}
            ,{Find_Object_Type("Gozanti_Cruiser_Squadron"), 5, "Space"}
			,{Find_Object_Type("Marauder_Missile_Cruiser"), 5, "Space"}
            ,{Find_Object_Type("Recusant"), 4, "Space"}
			,{Find_Object_Type("Neutron_Star"), 4, "Space"}
			,{Find_Object_Type("Generic_Gladiator"), 3, "Space"}
			,{Find_Object_Type("Dreadnaught_Empire"), 3, "Space"}			
            ,{Find_Object_Type("Galleon"), 2, "Space"}			
            ,{Find_Object_Type("Generic_Victory_Destroyer"), 3, "Space"}
            ,{Find_Object_Type("Bulwark_I"), 3, "Space"}
            ,{Find_Object_Type("Invincible_Cruiser"), 1, "Space"}
            ,{Find_Object_Type("Espo_Squad"), 4, "Land"}
            ,{Find_Object_Type("Strikebreaker_Group"), 5, "Land"}
            ,{Find_Object_Type("JX40_Group"), 2, "Land"}
            ,{Find_Object_Type("SX20_Company"), 4, "Land"}
            ,{Find_Object_Type("X10_Group"), 1, "Land"}
            }
    else
        Unit_Table = {
            {Find_Object_Type("Generic_Venator"), 3, "Space"}
			,{Find_Object_Type("Dauntless"), 2, "Space"}
			,{Find_Object_Type("Generic_Providence"), 2, "Space"}
			,{Find_Object_Type("Keldabe"), 1, "Space"}
			,{Find_Object_Type("Invincible_Cruiser"), 1, "Space"}
            ,{Find_Object_Type("Corellian_Corvette"), 5, "Space"}
            ,{Find_Object_Type("Corellian_Gunboat"), 5, "Space"}
            ,{Find_Object_Type("Generic_Star_Destroyer"), 1, "Space"}			
			,{Find_Object_Type("IPV1_System_Patrol_Craft"), 5, "Space"}
            ,{Find_Object_Type("Marauder_Missile_Cruiser"), 5, "Space"}
            ,{Find_Object_Type("Dreadnaught_Empire"), 4, "Space"}
            ,{Find_Object_Type("Generic_Victory_Destroyer"), 3, "Space"}
			,{Find_Object_Type("Generic_Gladiator"), 2, "Space"}
			,{Find_Object_Type("Generic_Acclamator_Assault_Ship_I"), 2, "Space"}
			,{Find_Object_Type("Generic_Acclamator_Assault_Ship_II"), 2, "Space"}
			,{Find_Object_Type("Generic_Acclamator_Assault_Ship_Leveler"), 2, "Space"}
			,{Find_Object_Type("Generic_Imperial_II_Frigate"), 2, "Space"}
            ,{Find_Object_Type("Liberator_Cruiser"), 2, "Space"}			
            ,{Find_Object_Type("Nebulon_B_Zsinj"), 5, "Space"}			
            ,{Find_Object_Type("Citadel_Cruiser_Squadron"), 4, "Space"}
            ,{Find_Object_Type("Gozanti_Cruiser_Squadron"), 5, "Space"}
            ,{Find_Object_Type("Quasar_Zsinj"), 5, "Space"}
            ,{Find_Object_Type("Neutron_Star"), 4, "Space"}
            ,{Find_Object_Type("Galleon"), 2, "Space"}	
            ,{Find_Object_Type("CC7700"), 1, "Space"}			
            ,{Find_Object_Type("Pirate_Crusader"), 5, "Space"}
			,{Find_Object_Type("Interceptor_Frigate"), 5, "Space"}
            ,{Find_Object_Type("Delvardus_Army_Trooper_Squad"), 5, "Land"}
            ,{Find_Object_Type("Pentastar_Walker_Group"), 2, "Land"}
            ,{Find_Object_Type("Pentastar_Speeder_Group"), 1, "Land"}
			,{Find_Object_Type("LAAT_Group"), 2, "Land"}
			,{Find_Object_Type("Teradoc_ATPT_Squad"), 3, "Land"}
			,{Find_Object_Type("Teradoc_Skiff_Group"), 3, "Land"}
			,{Find_Object_Type("Skyhopper_Squad"), 4, "Land"}
            ,{Find_Object_Type("Zsinj_A5_Juggernaut_Company"), 1, "Land"}			
			,{Find_Object_Type("Rebel_AA5_Brigade"), 4, "Land"}
            ,{Find_Object_Type("Rebel_RAF_Brigade"), 1, "Land"}			
        }
    end

        return Unit_Table
end

function DefineGroundBaseTable(faction)
    if faction == Find_Player("Empire") then
        Groundbase_Table = {
                Find_Object_Type("E_Ground_Barracks"),
                Find_Object_Type("E_Ground_Light_Vehicle_Factory"),					
                Find_Object_Type("E_Ground_Heavy_Vehicle_Factory"),					
            }
    elseif faction == Find_Player("Rebel") then         
        Groundbase_Table = {
                Find_Object_Type("R_Ground_Barracks"),
                Find_Object_Type("R_Ground_Light_Vehicle_Factory"),
                Find_Object_Type("R_Ground_Heavy_Vehicle_Factory"),
                }
	elseif faction == Find_Player("Hapes_Consortium") then         
        Groundbase_Table = {
                Find_Object_Type("R_Ground_Barracks"),
                Find_Object_Type("R_Ground_Light_Vehicle_Factory"),
                Find_Object_Type("R_Ground_Heavy_Vehicle_Factory"),
                }
    elseif faction == Find_Player("EmpireoftheHand") then   
        Groundbase_Table = {
                Find_Object_Type("U_Ground_Barracks"),
                Find_Object_Type("U_Ground_Light_Vehicle_Factory"),
                Find_Object_Type("U_Ground_Vehicle_Factory"),
                }
    elseif faction == Find_Player("Hutts") then                  
        Groundbase_Table = {
                Find_Object_Type("A_Ground_Barracks"),
                Find_Object_Type("A_Ground_Light_Vehicle_Factory"),
                Find_Object_Type("A_Ground_Heavy_Vehicle_Factory"),
                }
    elseif faction == Find_Player("Pentastar") then          
        Groundbase_Table = {
                Find_Object_Type("P_Ground_Barracks"),
                Find_Object_Type("P_Ground_Light_Vehicle_Factory"),
                Find_Object_Type("P_Ground_Heavy_Vehicle_Factory"),
                }
	elseif faction == Find_Player("Pirates") then          
        Groundbase_Table = {
                Find_Object_Type("Z_Ground_Barracks"),
                Find_Object_Type("Z_Ground_Light_Vehicle_Factory"),
                Find_Object_Type("Z_Ground_Heavy_Vehicle_Factory"),
                }
	elseif faction == Find_Player("Teradoc") then          
        Groundbase_Table = {
                Find_Object_Type("T_Ground_Barracks"),
                Find_Object_Type("T_Ground_Light_Vehicle_Factory"),
                Find_Object_Type("T_Ground_Heavy_Vehicle_Factory"),
                }
    elseif faction == Find_Player("Corporate_Sector") then               
        Groundbase_Table = {
                Find_Object_Type("C_Ground_Barracks"),
                Find_Object_Type("C_Ground_Light_Vehicle_Factory"),
                Find_Object_Type("C_Ground_Heavy_Vehicle_Factory"),
                }
    else
        Groundbase_Table = {
                Find_Object_Type("E_Ground_Barracks"),
                Find_Object_Type("E_Ground_Light_Vehicle_Factory"),					
                Find_Object_Type("E_Ground_Heavy_Vehicle_Factory"),					
            }
    end

    return Groundbase_Table
end