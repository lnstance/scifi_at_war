--******************************************************************************
--     _______ __
--    |_     _|  |--.----.---.-.--.--.--.-----.-----.
--      |   | |     |   _|  _  |  |  |  |     |__ --|
--      |___| |__|__|__| |___._|________|__|__|_____|
--     ______
--    |   __ \.-----.--.--.-----.-----.-----.-----.
--    |      <|  -__|  |  |  -__|     |  _  |  -__|
--    |___|__||_____|\___/|_____|__|__|___  |_____|
--                                    |_____|
--*   @Author:              [TR]Jorritkarwehr
--*   @Date:                2018-03-20T01:27:01+01:00
--*   @Project:             Imperial Civil War
--*   @Filename:            UnitSwitcherLibrary.lua
--*   @Last modified by:    [TR]Jorritkarwehr
--*   @Last modified time:  2018-03-26T09:58:14+02:00
--*   @License:             This source code may only be used with explicit permission from the developers
--*   @Copyright:           © TR: Imperial Civil War Development Team
--******************************************************************************

function Get_Swap_Entry(upgrade_object)
	local swaps = {
		{"AckbarHO2GV","Home_One","Galactic_Voyager"},
		{"AckbarGV2HO","Galactic_Voyager","Home_One"},
		{"IblisPeregrine2SF","Iblis_Peregrine","Iblis_Selonian_Fire"},
		{"IblisSF2Harbinger","Iblis_Selonian_Fire","Iblis_Harbinger"},
		{"NantzIn2FW","Nantz_Independence","Nantz_Faithful_Watchman"},
		{"NantzFW2In","Nantz_Faithful_Watchman","Nantz_Independence"},
		{"SovvDaunt2VP","Sovv_Dauntless","Sovv_Voice_of_the_People"},
		{"SovvVP2Daunt","Sovv_Voice_of_the_People","Sovv_Dauntless"},
		{"DraysonTF2NH","Drayson_True_Fidelity","Drayson_New_Hope"},
		{"DraysonNH2TF","Drayson_New_Hope","Drayson_True_Fidelity"},
		{"SovvDaunt2VP","Sovv_Dauntless","Sovv_Voice_of_the_People"},
		{"SovvVP2Daunt","Sovv_Voice_of_the_People","Sovv_Dauntless"},
		{"SnunbA62Resolve","Snunb_Antares_Six","Snunb_Resolve"},
		{"MassaUpgrade","Massa_Lucrehulk_Auxiliary","Massa_Lucrehulk_Carrier"},
		{"BrandIndomitable2Yald","Brand_Indomitable","Brand_Yald"},
		{"BrandYald2Indomitable","Brand_Yald","Brand_Indomitable"},
	}
	
	for index, obj in pairs(swaps) do
		if string.upper(obj[1]) == upgrade_object then
			return obj
		end
	end
	return nil
end