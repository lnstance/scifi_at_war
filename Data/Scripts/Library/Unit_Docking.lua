--***********************************************************************************
--*   Trek Wars
--*                                    
--*   @Author:              Imperial
--*   @Date:                
--*   @Project:             Trek Wars and Scifi at War
--*   @Last modified by:    Imperial
--*   @License:             Feel free to use
--*   @Copyright:           © Imperial
--***********************************************************************************



--===================================== Functions ====================================

-- Minimizing Health       
function Reduce_Health(Receiver_Unit, New_Health) -- New_Health is a float value
 
    Receiver_Health = Receiver_Unit.Get_Health() 
                
    -- Decrease health in steps of 50 until New_Health is approached                   
    while Receiver_Health > New_Health do
        Receiver_Unit.Take_Damage(50)
        Receiver_Health = Receiver_Unit.Get_Health() 
    end   
end     

--================== Minimizing Shield =================== 
function Reduce_Shield(Receiver_Unit, New_Shield)
 
    Receiver_Shield = Receiver_Unit.Get_Shield() 
                
    -- Decrease Shield in steps of 50 until New_Shield is approached                   
    while Receiver_Shield > New_Shield do
        Receiver_Unit.Take_Damage(50)
        Receiver_Shield = Receiver_Unit.Get_Shield() 
    end   
end   


--================== Seperate ===================
function Seperate(Unit_Parts_List)                                  
    
    Unit_List = GC_Spawn_List(Unit_Parts_List, Object, Object.Get_Owner(), true)
                     
    for each, Unit in pairs(Unit_List) do   
        if Object.Get_Health() < 1.0 then                                                                                   
            Reduce_Health(Unit, Object.Get_Health()) 
        elseif Object.Get_Shield() < 1.0 then
            Reduce_Shield(Unit, Object.Get_Shield())
        end  
    end  
            
    if table.getn(Unit_List) >= 0 then
        Assemble_Fleet(Unit_List)
        return true -- Spawned something
    end     
    return false                                 
end   

--==================== Join =====================         
function Join_Unit(Unit_Parts_List, Fused_Unit)
    
    local Unit_Count = table.getn(Unit_Parts_List)  
    local Fused_Health = Object.Get_Health()
    local Fused_Shield = Object.Get_Shield()
    local All_Parts_Nearby = true 
    local Fuse_Distance = 600
    local Nearest_One = nil 
          
                    
    for each, Unit in pairs(Unit_Parts_List) do -- Making sure every part of the unit is present     
        if Object.Get_Type().Get_Name() ~= Unit then
                                                                                                        
            Nearest_One = Find_Nearest(Object, Unit, Object.Get_Owner(), true)	
                                                                                                                                                                                              
            -- Calculating Distance to this Object                     
            if Nearest_One == nil then                                                                                                                  
                Object.Activate_Ability(ability_1, false)              
                return false
                 
            elseif Nearest_One.Get_Distance(Object) > Fuse_Distance then                 
                if Nearest_One.Get_Distance(Object) < Fuse_Distance * 5 then
                    Nearest_One.Move_To(Object) -- Send ALL members in this loop to the Object   
                end 
                                
                All_Parts_Nearby = false
            end                         
        end
    end        
          
                 
    if not All_Parts_Nearby then 
        auto_join = true -- Dont run this in the loop above, because it needs to deal with all member
        Object.Activate_Ability(ability_1, false) 
        return false
    end 
    
    
    
    auto_join = false -- resetting when the ability returns from auto_join mode (because the player activated the ability a while ago)      
                                           
    for each, Unit in pairs(Unit_Parts_List) do     
        if Object.Get_Type().Get_Name() ~= Unit then -- Because this Object despawns as very last
                                                                                          
            -- Nearest_One = Find_Nearest(Object, Find_Object_Type(Unit_Parts_List[each])) 
            -- Nearest_To, Desired_Unit, Desired_Player, if true return only for that player               
            Nearest_One = Find_Nearest(Object, Unit, Object.Get_Owner(), true)	
                                                                                                                                                                                                                                                                                   
            Nearest_Health = Nearest_One.Get_Health() 
            Nearest_Shield = Nearest_One.Get_Shield()
            Nearest_One.Despawn()                    
                                                                                                                                                                                 
            -- 2.0 because in each cycle we compare 2 units, and combine the rest of their remaining health and shield 
            -- CAUTION: Fused_Health and Fused_Shield are stored from the last loop cycle and re-processed here!                   
            Fused_Health = 2.0 - (2.0 - (Nearest_Health + Fused_Health) / 2.0)
            Fused_Shield = 2.0 - (2.0 - (Nearest_Shield + Fused_Shield) / 2.0)   
                                                                                                                
            
            -- Break here after the first joined unit, if the player is supposed to fuse them manually one by one, instead of batch fusing all nearby parts    
            -- break                                                                                                                                                                                         
        end
    end 
    
    
    -- We got this far now, so it can spawn the Fused_Ship and adjust its stats:       
    local Fused_Ship = GC_Spawn_Unit(Fused_Unit, Object, Object.Get_Owner(), true) 
     
                                                    
    if Fused_Health < 1.0 then                                                                                   
        Reduce_Health(Fused_Ship, Fused_Health) -- This is forced to set shied to 0 
    elseif Fused_Shield < 1.0 then
        Reduce_Shield(Fused_Ship, Fused_Shield)
    end    
             
    
    return true
end 
            
--==================================== End of File ===================================