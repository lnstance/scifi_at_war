--******************************************************************************
--     _______ __
--    |_     _|  |--.----.---.-.--.--.--.-----.-----.
--      |   | |     |   _|  _  |  |  |  |     |__ --|
--      |___| |__|__|__| |___._|________|__|__|_____|
--     ______
--    |   __ \.-----.--.--.-----.-----.-----.-----.
--    |      <|  -__|  |  |  -__|     |  _  |  -__|
--    |___|__||_____|\___/|_____|__|__|___  |_____|
--                                    |_____|
--*   @Author:              [TR]Pox <Pox>
--*   @Date:                2018-01-06T18:36:14+01:00
--*   @Project:             Imperial Civil War
--*   @Filename:            GameObjectLibrary.lua
--*   @Last modified by:    [TR]Pox
--*   @Last modified time:  2018-04-12T00:17:51+02:00
--*   @License:             This source code may only be used with explicit permission from the developers
--*   @Copyright:           © TR: Imperial Civil War Development Team
--******************************************************************************

require("eawx-util/Comparators")

GameObjectLibrary = {
    Interdictors = {
        "Prolipsi",
        "Eclipse_Star_Destroyer",
        "Sovereign",
        "Generic_Interdictor_Cruiser",
        "Generic_Dominator",
        "Mothma_Star_Destroyer",		
        "Phulik_Binder",
        "Iillor_Corusca",
        "Kaerobani_Interdictor",
        "Voota_Splendor",
        "CC7700",
		"CC7700_E",
		"Dezon_Constrainer",
        "Interdiction_Minefield"
    },
    Numbers = {
        "Display_One",
        "Display_Two",
        "Display_Three",
        "Display_Four",
        "Display_Five",
        "Display_Six",
        "Display_Seven",
        "Display_Eight",
        "Display_Nine",
        "Display_Ten"
    },
    OrbitalStructures = {
        ["GENERIC_SHIPYARD_LEVEL_ONE"] = {
            Text = "TEXT_DISPLAY_SHIPYARD1",
            Equation = "Planet_Has_Shipyard_One"
        },
        ["GENERIC_SHIPYARD_LEVEL_TWO"] = {
            Text = "TEXT_DISPLAY_SHIPYARD2",
            Equation = "Planet_Has_Shipyard_Two"
        },
        ["GENERIC_SHIPYARD_LEVEL_THREE"] = {
            Text = "TEXT_DISPLAY_SHIPYARD3",
            Equation = "Planet_Has_Shipyard_Three"
        },
        ["GENERIC_SHIPYARD_LEVEL_FOUR"] = {
            Text = "TEXT_DISPLAY_SHIPYARD4",
            Equation = "Planet_Has_Shipyard_Four"
        },
		["EMPIRE_GOLAN_ONE"] = {
            Text = "TEXT_DISPLAY_GOLAN_ONE_STATION",
            Equation = "Planet_Has_Golan_One"
        },
        ["REBEL_GOLAN_ONE"] = {
            Text = "TEXT_DISPLAY_GOLAN_ONE_STATION",
            Equation = "Planet_Has_Golan_One"
        },
		["EMPIRE_GOLAN_TWO"] = {
            Text = "TEXT_DISPLAY_GOLAN_TWO_STATION",
            Equation = "Planet_Has_Golan_Two"
        },
        ["REBEL_GOLAN_TWO"] = {
            Text = "TEXT_DISPLAY_GOLAN_TWO_STATION",
            Equation = "Planet_Has_Golan_Two"
        },
        ["EMPIRE_GOLAN_THREE"] = {
            Text = "TEXT_DISPLAY_GOLAN_THREE_STATION",
            Equation = "Planet_Has_Golan_Three"
        },
        ["REBEL_GOLAN_THREE"] = {
            Text = "TEXT_DISPLAY_GOLAN_THREE_STATION",
            Equation = "Planet_Has_Golan_Three"
        },
		["VALIDUSIA"] = {
            Text = "TEXT_DISPLAY_VALIDUSIA",
            Equation = "Planet_Has_Validusia"
        },
        ["EMPRESS_STATION"] = {
            Text = "TEXT_DISPLAY_EMPRESS",
            Equation = "Planet_Has_Empress"
        },
        ["OTO"] = {
            Text = "TEXT_DISPLAY_OTO",
            Equation = "Planet_Has_Oto"
        },
        ["BRASK"] = {
            Text = "TEXT_DISPLAY_BRASK",
            Equation = "Planet_Has_Brask"
        },
        ["VISVIA"] = {
            Text = "TEXT_DISPLAY_VISVIA",
            Equation = "Planet_Has_Visvia"
        },
        ["GENERIC_TRADESTATION"] = {
            Text = "TEXT_DISPLAY_TRADE",
            Equation = "Planet_Has_Trade_Station"
        },
        ["PIRATE_BASE"] = {
            Text = "TEXT_DISPLAY_TAVIRA",
            Equation = "Planet_Has_Pirate_Base"
        },
        ["RANCOR_BASE"] = {
            Text = "TEXT_DISPLAY_RANCOR_BASE",
            Equation = "Planet_Has_Rancor_Base"
        },
        ["BLACK_15"] = {
            Text = "TEXT_DISPLAY_BLACK15",
            Equation = "Planet_Has_Black_Fifteen"
        },
        ["SLAYN_KORPIL"] = {
            Text = "TEXT_DISPLAY_SLAYN_KORPIL",
            Equation = "Planet_Has_Slayn_Korpil"
        }
        --  ["CREW_RESOURCE_DUMMY"]={
        --      Text="TEXT_DISPLAY_SLAYN_KORPIL"
        --  },
        --    ["PLACEHOLDER_CATEGORY_DUMMY"]={
        --        Text="TEXT_DISPLAY_PLACEHOLDER_CATEGORY_DUMMY"
        --    },
        --    ["NON_CAPITAL_CATEGORY_DUMMY"]={
        --        Text="TEXT_DISPLAY_NON_CAPITAL_CATEGORY_DUMMY"
        --    },
        --    ["CAPITAL_CATEGORY_DUMMY"]={
        --        Text="TEXT_DISPLAY_CAPITAL_CATEGORY_DUMMY"
        --    },
        --    ["STRUCTURE_CATEGORY_DUMMY"]={
        --        Text="TEXT_DISPLAY_STRUCTURE_CATEGORY_DUMMY"
        --    }
    },
    InfluenceLevels = {
        ["INFLUENCE_ONE"] = {},
        ["INFLUENCE_TWO"] = {},
        ["INFLUENCE_THREE"] = {},
        ["INFLUENCE_FOUR"] = {},
        ["INFLUENCE_FIVE"] = {},
        ["INFLUENCE_SIX"] = {},
        ["INFLUENCE_SEVEN"] = {},
        ["INFLUENCE_EIGHT"] = {},
        ["INFLUENCE_NINE"] = {},
        ["INFLUENCE_TEN"] = {},
        ["BONUS_PLACEHOLDER"] = {}
    },
    Units = {
		["Interdiction_Minefield_Container"] = {
		Scripts = {
                "interdictor-ai"
            },
            Fighters = {}
        },
        ["Iillor_Corusca"] = {
            Scripts = {
                "interdictor-ai"
            },
            Fighters = {}
        },
        ["Phulik_Binder"] = {
            Scripts = {
                "interdictor-ai"
            },
            Fighters = {}
        },
        ["CC7700"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "interdictor-ai"
            },
            Fighters = {}
        },
		["CC7700_E"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "interdictor-ai"
            },
            Fighters = {}
        },
		["Dezon_Constrainer"] = {
            Scripts = {
                "interdictor-ai"
            },
            Fighters = {}
        },
        ["MC30C"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer"
            },
            Fighters = {}
        },		
        ["LANCER_FRIGATE"] = {
            Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
        ["INTERCEPTOR_FRIGATE"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
        ["KUURO"] = {
            Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "single-unit-retreat",
                "microjump"
            },
            Fighters = {}
        },
        ["INSTANTACTION_MARKER_EMPIRE"] = {
            Scripts = {
                "multilayer",
                "single-unit-retreat",
                "microjump"
            },
            Fighters = {}
        },
        ["INSTANTACTION_MARKER_NEWREP"] = {
            Scripts = {
                "multilayer",
                "single-unit-retreat",
                "microjump"
            },
            Fighters = {}
        },
        ["GENERIC_INTERDICTOR_CRUISER"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat",
                "interdictor-ai"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 2,
                        TechLevel = LessOrEqualTo(3)
                    },
                    ["PENTASTAR"] = {
                        Reserve = 2,
                        Initial = 2
                    },
                    ["PIRATES"] = {
                        Reserve = 2,
                        Initial = 2
                    },
                    ["TERADOC"] = {
                        Reserve = 2,
                        Initial = 2
                    },
                    ["HUTTS"] = {
                        Reserve = 2,
                        Initial = 2
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 2
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 2
                    }
                },
				["SUPER_TIE_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 2,
                        TechLevel = EqualTo(4)
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 2,
                        TechLevel = GreaterThan(4)
                    }
                },
                ["SHIELDED_TIE_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
				["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }				
            }
        },
        ["ALLIANCE_ASSAULT_FRIGATE"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
				["HOWLRUNNER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["MUQARAEA"] = {
            Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
        ["SYZYGOS"] = {
            Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
        ["PROLIPSI"] = {
            Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat",
                "interdictor-ai"
            },
			Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["CLAWCRAFT_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["KRSISS_INTERCEPTOR_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_IMPERIAL_II_FRIGATE"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }	
                },
                ["TIE_AVENGER_SQUADRON"] = {
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    }	
                },
                ["SHIELDED_INTERCEPTOR_SQUADRON"] = {
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    }	
                },				
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }	
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["SHIELDED_TIE_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["ORMOS"] = {
            Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["KRSISS_INTERCEPTOR_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SYCA_BOMBER_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["CLAWCRAFT_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_ACCLAMATOR_ASSAULT_SHIP_II"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1,
						TechLevel = LessOrEqualTo(3)
                    },
                    ["PENTASTAR"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }					
                },
				["SUPER_TIE_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1,
						TechLevel = EqualTo(4)
                    }
                },	
				["HOWLRUNNER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1,
						TechLevel = GreaterThan(4)
                    }
                },				
                ["SHIELDED_TIE_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_ACCLAMATOR_ASSAULT_SHIP_LEVELER"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }					
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }					
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }					
                },				
                ["TIE_X2_SQUADRON"] = {
                    ["PENTASTAR"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["SHIELDED_TIE_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_ACCLAMATOR_ASSAULT_SHIP_I"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }					
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 4,
                        Initial = 2
                    },
                    ["HOSTILE"] = {
                        Reserve = 4,
                        Initial = 2
                    },
                    ["WARLORDS"] = {
                        Reserve = 4,
                        Initial = 2
                    }					
                },
                ["TIE_GT_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }					
                },				
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["SHIELDED_TIE_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 4,
                        Initial = 2
                    }
                }
            }
        },
        ["ENFORCER"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_AGGRESSOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["REPUBLIC_BOARDING_SHUTTLE"] = {
            Scripts = {
                "boarding"
            },
            Fighters = {}
        },
        ["ROHKEA"] = {
            Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
		["REPUBLIC_SD"] = {
			Ship_Crew_Requirement = 15,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["HOWLRUNNER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 2
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
				["E-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 2
                    }
                },
                ["B-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        ResearchType = "~BwingE"
                    }
                },
                ["B-WING_E_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        ResearchType = "BwingE"
                    }
                }				
            }
        },
        ["NEBULA"] = {
			Ship_Crew_Requirement = 15,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["DEFENDER_STARFIGHTER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 2
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 2
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 2
                    }
                },
                ["K-WING_SQUADRON_HALF"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SKIPRAY_SQUADRON_HALF"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["A9_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 2
                    }
                }
            }
        },
        ["RAIDER_PENTASTAR"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },		
            Fighters = {}
        },
		["BROADSIDE_CRUISER"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {}
        },
        ["ADZ"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON_HALF"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_STAR_DESTROYER_TWO"] = {
            Ship_Crew_Requirement = 30,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Flags = {
                FIGHTERINHERIT = "GENERIC_STAR_DESTROYER"
            }
        },
        ["CORONA"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["A9_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_GLADIATOR"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["TIE_BOMBER_SQUADRON"] = {
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },					
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_OPPRESSOR_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["BTLB_Y-WING_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_GLADIATOR_TWO"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                FIGHTERINHERIT = "GENERIC_GLADIATOR"
            }			
        },			
        ["VIGIL"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
		["TARTAN_PATROL_CRUISER"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
        ["MC80B"] = {
			Ship_Crew_Requirement = 20,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["HOWLRUNNER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessOrEqualTo(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["B-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        ResearchType = "~BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["E-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterThan(3)
                    }
                },
                ["B-WING_E_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        ResearchType = "BwingE"
                    }
                }				
            }
        },
        ["CORELLIAN_CORVETTE"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer"
            },
            Fighters = {}
        },
        ["ESCORT_CARRIER"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = IsOneOf({1, 2, 3, 5})
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 2,
                        Initial = 2
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["A9_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = EqualTo(4)
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["TIE_BOMBER_SQUADRON"] = {
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = IsOneOf({1, 3, 4})
                    }
                },
                ["SCIMMY_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = IsOneOf({2, 5})
                    }
                },
                ["TIE_HEAVY_BOMBER_SQUADRON"] = {
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_DEFENDER_SQUADRON_HALF"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = IsOneOf({1, 3, 4, 5})
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },					
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["MISSILE_BOAT_SQUADRON_HALF"] = {
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    },
					["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = EqualTo(2)
                    }
                },
                ["STARWING_SQUADRON_HALF"] = {
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                }
			}
        },
        ["EFODIO_FLEET_TENDER"] = {
			Ship_Crew_Requirement = 25,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["KRSISS_INTERCEPTOR_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 0,
                        Initial = 1,
                        TechLevel = LessOrEqualTo(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1,
                        TechLevel = LessOrEqualTo(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1,
                        TechLevel = LessOrEqualTo(3)
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["SCARSISS_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 0,
                        Initial = 1,
                        TechLevel = GreaterThan(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1,
                        TechLevel = GreaterThan(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1,
                        TechLevel = GreaterThan(3)
                    }
                },
                ["CLAWCRAFT_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["CHISS_STAR_DESTROYER"] = {
            Ship_Crew_Requirement = 25,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["A-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SYCA_BOMBER_SQUADRON_DOUBLE"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["KRSISS_INTERCEPTOR_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 2,
                        Initial = 2,
                        TechLevel = LessOrEqualTo(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 2,
                        TechLevel = LessOrEqualTo(3)
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON_DOUBLE"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SCARSISS_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 2,
                        Initial = 2,
                        TechLevel = GreaterThan(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 2,
                        TechLevel = GreaterThan(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 2,
                        TechLevel = GreaterThan(3)
                    }
                },
                ["TIE_BOMBER_SQUADRON_DOUBLE"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["CLAWCRAFT_SQUADRON_DOUBLE"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["WORLD_DEVASTATOR"] = {
			-- Ship_Crew_Requirement = 50,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_DROID_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 10,
                        Initial = 5
                    }
                }
            }
        },
        ["TORPEDO_SPHERE"] = {
			Ship_Crew_Requirement = 35,
            Scripts = {
                "multilayer"
            },
            Fighters = {}
        },
        ["VINDICATOR_CRUISER"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessOrEqualTo(3)
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["PENTASTAR"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["SUPER_TIE_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = EqualTo(4)
                    }
                },				
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterThan(4)
                    }
                },
                ["SHIELDED_TIE_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
				["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
        ["MARAUDER_MISSILE_CRUISER"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["IRDA_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
				["TIE_GT_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 0,
                        Initial = 1
                    },
					["TERADOC"] = {
                        Reserve = 0,
                        Initial = 1
                    },
					["PIRATES"] = {
                        Reserve = 0,
                        Initial = 1
                    },
					["HUTTS"] = {
                        Reserve = 0,
                        Initial = 1
                    },
					["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                },
				["TIE_X2_SQUADRON"] = {
                    ["PENTASTAR"] = {
                        Reserve = 0,
                        Initial = 1
                    }				
                },					
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
        ["IMPERIAL_BOARDING_SHUTTLE"] = {
            Scripts = {
                "multilayer",
                "boarding"
            },
            Fighters = {}
        },
        ["CORELLIAN_GUNBOAT_FERRIER"] = {
            Scripts = {
                "boarding"
            },
            Fighters = {}
        },
        ["KATANA_DREADNAUGHT_REBEL"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                FIGHTERINHERIT = "DREADNAUGHT_EMPIRE"
            }
        },
		["DREADNAUGHT_CARRIER"] = {
			Ship_Crew_Requirement = 20,
            Scripts = {
                "multilayer",
                "single-unit-retreat",
				"fighter-spawn"
            },
            Fighters = {
                ["IRD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1,
                    }
				},
                ["IRDA_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1,
                    }
				},				
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1,
                    }
				},
                ["TIE_GT_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1,
                    }
				},
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                    }
				},
                ["SHIELDED_TIE_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1,
                    }
				}				
            }
        },
        ["PIRATE_CRUSADER"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
		["CRUSADER_GUNSHIP"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
        ["GENERIC_SECUTOR"] = {
			Ship_Crew_Requirement = 35,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_SENTINEL_SQUADRON"] = {
                    ["TERADOC"] = {
                        Reserve = 8,
                        Initial = 3
                    },
                    ["EMPIRE"] = {
                        Reserve = 8,
                        Initial = 3
                    },
                    ["WARLORDS"] = {
                        Reserve = 8,
                        Initial = 3
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["PIRATES"] = {
                        Reserve = 8,
                        Initial = 3
                    },
                    ["HUTTS"] = {
                        Reserve = 8,
                        Initial = 3
                    },
                    ["HOSTILE"] = {
                        Reserve = 8,
                        Initial = 3
                    }
                },
                ["TIE_GT_SQUADRON"] = {
                    ["PENTASTAR"] = {
                        Reserve = 8,
                        Initial = 3
                    }
                },				
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 8,
                        Initial = 3
                    },
                    ["HOSTILE"] = {
                        Reserve = 8,
                        Initial = 3
                    },
                    ["WARLORDS"] = {
                        Reserve = 8,
                        Initial = 3
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 8,
                        Initial = 3
                    },
                    ["HOSTILE"] = {
                        Reserve = 8,
                        Initial = 3
                    },
                    ["WARLORDS"] = {
                        Reserve = 8,
                        Initial = 3
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 8,
                        Initial = 3
                    }
                },
                ["SHIELDED_TIE_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 8,
                        Initial = 3
                    }
                },
                ["HOWLRUNNER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 8,
                        Initial = 3
                    }
                }
            }
        },
		["EX_F"] = {
			Ship_Crew_Requirement = 50,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 3,
                        Initial = 3
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["KYNIGOS"] = {
            Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["SCARSISS_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
        ["LUCREHULK_CSA"] = {
			Ship_Crew_Requirement = 50,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["2_WARPOD_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 12,
                        Initial = 3
                    }
                },
                ["MANKVIM_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 8,
                        Initial = 2
                    }
                },
                ["IRD_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 8,
                        Initial = 2
                    }
                }
            }
        },
		["RECUSANT"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
        ["INVINCIBLE_CRUISER"] = {
			Ship_Crew_Requirement = 35,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["2_WARPOD_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["Z95_HEADHUNTER_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["IRDA_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["T-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON_DOUBLE"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["TIE_FIGHTER_SQUADRON_DOUBLE"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }				
            }
        },
        ["GENERIC_PROCURSATOR"] = {
			Ship_Crew_Requirement = 15,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
        ["CORELLIAN_GUNBOAT"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
        ["VISVIA"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["CLAWCRAFT_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SYCA_BOMBER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
        ["NOVA_CRUISER"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["MIYTIL_SQUADRON"] = {
                    ["HAPES_CONSORTIUM"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["NOVA_CRUISER_INFLUENCE"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["MIYTIL_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["BETA_CRUISER"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
		["BETA_CRUISER_INFLUENCE"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
        ["GENERIC_ALLEGIANCE"] = {
			Ship_Crew_Requirement = 50,
            Scripts = {
                "multilayer"
            },
            Fighters = {}
        },
        ["GENERIC_VICTORY_DESTROYER"] = {
			Ship_Crew_Requirement = 15,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    }
                },
                ["TIE_DROID_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = EqualTo(3)
                    }
                },
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["IRD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["HOWLRUNNER_SQUADRON"] = {
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterThan(3)
                    }
                }
            }
        },
        ["EMPRESS_STATION"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["Y-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 5,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 5,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["B-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 5,
                        Initial = 1,
						ResearchType = "~BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 5,
                        Initial = 1,
						TechLevel = LessThan(4)
                    },
                    ["WARLORDS"] = {
                        Reserve = 5,
                        Initial = 1,
						TechLevel = LessThan(4)
                    }
                },
                ["B-WING_E_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 5,
                        Initial = 1,
						ResearchType = "BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 5,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(4)
                    },
                    ["WARLORDS"] = {
                        Reserve = 5,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(4)
                    }
                },				
                ["A-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 5,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 5,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 5,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 5,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["IRDA_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["BTLB_Y-WING_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["T-WING_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["EARLY_SKIPRAY_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_DOMINATOR"] = {
			Ship_Crew_Requirement = 30,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat",
                "interdictor-ai"
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON_DOUBLE"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessOrEqualTo(3)
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SUPER_TIE_SQUADRON_DOUBLE"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = EqualTo(4)
                    }
                },				
                ["TIE_INTERCEPTOR_SQUADRON_DOUBLE"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterThan(4)
                    }
                },
                ["SHIELDED_TIE_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["MOTHMA_STAR_DESTROYER"] = {
			Ship_Crew_Requirement = 25,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat",
                "interdictor-ai"
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_AVENGER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },					
                ["A9_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(2)
                    }
                },
                ["E-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(2)
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(2)
                    }
                },				
                ["SHIELDED_TIE_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(2)
                    }
                }
            }
        },		
        ["VALIDUSIA"] = {
            Scripts = {
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 9,
                        Initial = 2
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 9,
                        Initial = 2
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 9,
                        Initial = 2
                    }
                }
            }
        },
        ["MUNIFICENT"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["ADVANCED_SKIPRAY_SQUADRON_HALF"] = {
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SKIPRAY_SQUADRON_HALF"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
					["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
					["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    },
					["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["EARLY_SKIPRAY_SQUADRON_HALF"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_TRADESTATION"] = {
            Scripts = {
				"fighter-spawn",
                "turn-station"
            },
            Fighters = {
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
				["CLAWCRAFT_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["IRD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
		["EMPIRE_STAR_BASE_1"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 1
                    }
				}
            }
        },
        ["EMPIRE_STAR_BASE_2"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Flags = {
                FIGHTERINHERIT = "EMPIRE_GOLAN_ONE"
            }
        },
        ["EMPIRE_STAR_BASE_3"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Flags = {
                FIGHTERINHERIT = "EMPIRE_GOLAN_TWO"
            }
        },
        ["EMPIRE_STAR_BASE_4"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Flags = {
                FIGHTERINHERIT = "EMPIRE_GOLAN_THREE"
            }
        },
        ["EMPIRE_STAR_BASE_5"] = {
            Scripts = {
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 9,
                        Initial = 2
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 9,
                        Initial = 2
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 9,
                        Initial = 2
                    }
                }
            }
        },
		["NEWREPUBLIC_STAR_BASE_1"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["IRD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
				["MIYTIL_SQUADRON"] = {
                    ["HAPES_CONSORTIUM"] = {
                        Reserve = 2,
                        Initial = 1
                    }
				}
            }
        },
        ["NEWREPUBLIC_STAR_BASE_2"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["IRD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
				["MIYTIL_SQUADRON"] = {
                    ["HAPES_CONSORTIUM"] = {
                        Reserve = 1,
                        Initial = 1
                    }
				}
            }
        },
        ["NEWREPUBLIC_STAR_BASE_3"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["Z95_HEADHUNTER_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["IRD_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
				["MIYTIL_SQUADRON_DOUBLE"] = {
                    ["HAPES_CONSORTIUM"] = {
                        Reserve = 1,
                        Initial = 1
                    }
				}
			}
        },
        ["NEWREPUBLIC_STAR_BASE_4"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["Z95_HEADHUNTER_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["IRD_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
				["MIYTIL_SQUADRON_DOUBLE"] = {
                    ["HAPES_CONSORTIUM"] = {
                        Reserve = 2,
                        Initial = 1
                    }
				}
            }
        },
        ["NEWREPUBLIC_STAR_BASE_5"] = {
            Scripts = {
                "fighter-spawn"
            },
            Fighters = {
                 ["Y-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["B-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 5,
                        Initial = 1,
						ResearchType = "~BwingE"
                    }
                },
                ["B-WING_E_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 5,
                        Initial = 1,
						ResearchType = "BwingE"
                    }
                },				
                ["A-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["IRDA_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["BTLB_Y-WING_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["T-WING_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["EARLY_SKIPRAY_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
				["MIYTIL_SQUADRON_DOUBLE"] = {
                    ["HAPES_CONSORTIUM"] = {
                        Reserve = 10,
                        Initial = 2
                    }
				},
				["MIYTIL_BOMBER_SQUADRON_DOUBLE"] = {
                    ["HAPES_CONSORTIUM"] = {
                        Reserve = 10,
                        Initial = 2
                    }
                }
            }
        },
        ["BRASK_STARBASE"] = {
            Scripts = {
                "turn-station"
            },
            Fighters = {
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
				["CLAWCRAFT_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["IRD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["EOTH_STAR_BASE_2"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["T-WING_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["EOTH_STAR_BASE_3"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_INTERCEPTOR_SQUADRON_DOUBLE"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Z95_HEADHUNTER_SQUADRON_DOUBLE"] = {
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["T-WING_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["VISVIA_STARBASE"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_INTERCEPTOR_SQUADRON_DOUBLE"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["PENTASTAR"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 2,
                        Initial = 1
                    },					
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["Z95_HEADHUNTER_SQUADRON_DOUBLE"] = {
                    ["PIRATES"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },				
                ["T-WING_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["OTO_STARBASE"] = {
            Scripts = {
                "fighter-spawn"
            },
            Fighters = {
                ["CLAWCRAFT_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 2
                    }
                },
                ["KRSISS_INTERCEPTOR_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SYCA_BOMBER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["SYNDIC_DESTROYER"] = {
            Ship_Crew_Requirement = 20,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                FighterDespawnFactor = 2.5
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 2
                    }
                },
                ["CLAWCRAFT_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 2
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 2
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 2
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SYCA_BOMBER_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 2
                    }
                }
            }
        },
        ["INTEGO_DESTROYER"] = {
            Ship_Crew_Requirement = 35,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["SYCA_BOMBER_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["CLAWCRAFT_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 2,
                        Initial = 2
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 2
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 2
                    }
                }
            }
        },
        ["VISCOUNT"] = {
			Ship_Crew_Requirement = 300,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["E-WING_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["B-WING_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 5,
                        Initial = 1,
                        ResearchType = "~BwingE"
                    }
                },
                ["B-WING_E_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 5,
                        Initial = 1,
                        ResearchType = "BwingE"
                    }
                },				
                ["DEFENDER_STARFIGHTER_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 10,
                        Initial = 2
                    }
                }
            }
        },
        ["EMPIRE_GOLAN_ONE"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["T-WING_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["EMPIRE_GOLAN_TWO"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_INTERCEPTOR_SQUADRON_DOUBLE"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Z95_HEADHUNTER_SQUADRON_DOUBLE"] = {
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["T-WING_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["EMPIRE_GOLAN_THREE"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_INTERCEPTOR_SQUADRON_DOUBLE"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["PENTASTAR"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 2,
                        Initial = 1
                    },					
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["Z95_HEADHUNTER_SQUADRON_DOUBLE"] = {
                    ["PIRATES"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },				
                ["T-WING_SQUADRON_DOUBLE"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["AMM_NOVA_CRUISER"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["MIYTIL_BOMBER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["MIYTIL_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_VICTORY_DESTROYER_TWO"] = {
			Ship_Crew_Requirement = 15,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Flags = {
                FIGHTERINHERIT = "GENERIC_VICTORY_DESTROYER"
            }
        },
        ["GENERIC_PROVIDENCE"] = {
			Ship_Crew_Requirement = 15,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 4,
                        Initial = 2
                    }
                },
                ["BTLB_Y-WING_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 4,
                        Initial = 2
                    }
                }
            }
        },
        ["MC40A"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },			
            Fighters = {
				["B-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1,
						ResearchType = "~BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1,
						TechLevel = LessThan(4)
                    },
                    ["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1,
						TechLevel = LessThan(4)
                    }
                },
                ["B-WING_E_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1,
						ResearchType = "BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(4)
                    },
                    ["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(4)
                    }
                },				
                ["TIE_HEAVY_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_VENATOR"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_GT_SQUADRON"] = {
                    ["PENTASTAR"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 2,
                        Initial = 1
                    },					
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["A9_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1,
                        TechLevel = GreaterThan(3)
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1,
						TechLevel = IsOneOf({1, 2, 3, 5})
                    }
                },
                ["TIE_BOMBER_SQUADRON_DOUBLE"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    },					
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["TIE_FIGHTER_SQUADRON_DOUBLE"] = {
                    ["PENTASTAR"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 2,
                        Initial = 1
                    },					
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1,
                        TechLevel = LessThan(4)
                    }
                },
                ["SUPER_TIE_SQUADRON_DOUBLE"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1,
						TechLevel = EqualTo(4)
                    }
                },				
                ["SHIELDED_TIE_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["HOWLRUNNER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["REBEL_GOLAN_ONE"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["E-WING_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterThan(2)
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessOrEqualTo(2)
                    }
                }
            }
        },
        ["KELDABE"] = {
			Ship_Crew_Requirement = 20,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["AGGRESSOR_ASSAULT_FIGHTER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["IMPERIAL"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["STARVIPER_SQUADRON"] = {
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["KELDABE_INFLUENCE"] = {
			Ship_Crew_Requirement = 20,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["AGGRESSOR_ASSAULT_FIGHTER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["STARVIPER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["CRIMSON_VICTORY"] = {
			Ship_Crew_Requirement = 15,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Flags = {
                FIGHTERINHERIT = "GENERIC_VICTORY_DESTROYER"
            }
        },
        ["QUASER"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["Y-WING_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["A-WING_SQUADRON_HALF"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1,
						ResearchType = "~CoS_Tevv"
                    }
                },
				["PREYBIRD_SQUADRON_HALF"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1,
						ResearchType = "CoS_Tevv"
                    }
                }
            }
        },
		["LIBERATOR_CRUISER"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["IRD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["T-WING_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },					
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["BTLB_Y-WING_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
				["HOWLRUNNER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
				["Z95_HEADHUNTER_SQUADRON"] = {
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
				["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["A-WING_SQUADRON"] = {
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },				
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },					
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                }
            }
        },
        ["ENDURANCE"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["E-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["DEFENDER_STARFIGHTER_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["K-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["SKIPRAY_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["A9_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["HOWLRUNNER_SQUADRON_DOUBLE"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["STARWING_SQUADRON_DOUBLE"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }				
            }
        },
        ["STAR_HOME"] = {
			-- Ship_Crew_Requirement = 10,
            Scripts = {
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["MIYTIL_BOMBER_SQUADRON_TRIPLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["MIYTIL_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
        ["AMM_BATTLEDRAGON"] = {
			Ship_Crew_Requirement = 20,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["MIYTIL_BOMBER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["MIYTIL_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["CHAF_DESTROYER"] = {
            Ship_Crew_Requirement = 15,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["CLAWCRAFT_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["FURION_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_STAR_DESTROYER"] = {
            Ship_Crew_Requirement = 30,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = IsOneOf({1, 2, 5})
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["TIE_DROID_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = EqualTo(3)
                    }					
                },
                ["SUPER_TIE_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = EqualTo(4)
                    }					
                },				
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = LessOrEqualTo(2)
                    },
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["A9_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = IsOneOf({3, 4})
                    }					
                },
                ["SHIELDED_INTERCEPTOR_SQUADRON"] = {
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["TIE_RAPTOR_SQUADRON"] = {
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },	
                ["TIE_AGGRESSOR_SQUADRON"] = {
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["PREYBIRD_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(5)
                    }					
                },				
                ["TIE_BOMBER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = IsOneOf({1, 3, 4})
                    },
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },					
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["SCIMMY_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = IsOneOf({2, 5})
                    }					
                },
                ["TIE_HEAVY_BOMBER_SQUADRON"] = {
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["TIE_OPPRESSOR_SQUADRON"] = {
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["SKIPRAY_SQUADRON_HALF"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["EARLY_SKIPRAY_SQUADRON_HALF"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },	
				["ADVANCED_SKIPRAY_SQUADRON_HALF"] = {
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
				["TIE_PUNISHER_SQUADRON_HALF"] = {
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
				["STARWING_SQUADRON_HALF"] = {
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["SHIELDED_TIE_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
				["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                }				
            }
        },
        ["YEVETHA_DREADNAUGHT"] = {
			Ship_Crew_Requirement = 20,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Flags = {
                FIGHTERINHERIT = "DREADNAUGHT_EMPIRE"
            }
        },
        ["ARMADIA"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TRIFOIL_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["TRIFOIL_SQUADRON_HALF"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }				
            }
        },
        ["ARMADIA_INFLUENCE"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TRIFOIL_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["TRIFOIL_SQUADRON_HALF"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }				
            }
        },
        ["C_TYPE_THRUSTSHIP"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TRIFOIL_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
				["TRIFOIL_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_HEAVY_BOMBER_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["C_TYPE_INFLUENCE"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TRIFOIL_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
				["TRIFOIL_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_HEAVY_BOMBER_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["ARQUITENS"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer"
            },
            Fighters = {}
        },
        ["SOVEREIGN"] = {
			Ship_Crew_Requirement = 300,
            Scripts = {
                "fighter-spawn",
                "interdictor-ai"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_INTERCEPTOR_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 11,
                        Initial = 4
                    }
                },
                ["TIE_BOMBER_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 4,
                        Initial = 1
                    }
                }
            }
        },
        ["BATTLEDRAGON"] = {
			Ship_Crew_Requirement = 20,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["MIYTIL_BOMBER_SQUADRON"] = {
                    ["HAPES_CONSORTIUM"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["MIYTIL_SQUADRON"] = {
                    ["HAPES_CONSORTIUM"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["BATTLEDRAGON_INFLUENCE"] = {
			Ship_Crew_Requirement = 20,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["MIYTIL_BOMBER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["MIYTIL_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["KATANA_DREADNAUGHT_EMPIRE"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                FIGHTERINHERIT = "DREADNAUGHT_EMPIRE"
            }
        },
        ["CALAMARI_CRUISER"] = {
			Ship_Crew_Requirement = 15,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["HOWLRUNNER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["BAC"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = LessThan(2)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = LessThan(2)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = LessThan(2)
                    }
                },			
                ["E-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(2)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(2)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(2)
                    }
                },
                ["B-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
						ResearchType = "~BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = LessThan(4)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = LessThan(4)
                    }
                },
                ["B-WING_E_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
						ResearchType = "BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(4)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(4)
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SCIMMY_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["HOME_ONE_TYPE"] = {
			Ship_Crew_Requirement = 50,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["B-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 3,
                        Initial = 1,
						ResearchType = "~BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 3,
                        Initial = 1,
						TechLevel = LessThan(4)
                    },
                    ["WARLORDS"] = {
                        Reserve = 3,
                        Initial = 1,
						TechLevel = LessThan(4)
                    }
                },
                ["B-WING_E_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 3,
                        Initial = 1,
						ResearchType = "BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 3,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(4)
                    },
                    ["WARLORDS"] = {
                        Reserve = 3,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(4)
                    }
                },				
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["HOWLRUNNER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },				
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 3,
                        Initial = 1
                    }
                }
            }
        },
        ["MEDIATOR"] = {
			Ship_Crew_Requirement = 50,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["B-WING_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 2,
						ResearchType = "~BwingE"
                    }
                },
                ["B-WING_E_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 2,
						ResearchType = "BwingE"
                    }
                },				
                ["E-WING_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 2
                    }
                }
            }
        },
		["BLUEDIVER"] = {
			Ship_Crew_Requirement = 50,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["B-WING_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 4,
                        Initial = 2,
						ResearchType = "~BwingE"
                    }
                },
                ["B-WING_E_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 4,
                        Initial = 2,
						ResearchType = "BwingE"
                    }
                },				
                ["E-WING_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 3,
                        Initial = 1
                    }
                },
				["REBEL_X-WING_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
				["A-WING_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
		["MINI_VISCOUNT"] = {
			Ship_Crew_Requirement = 50,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["B-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1,
						ResearchType = "~BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1,
						TechLevel = LessThan(4)
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1,
						TechLevel = LessThan(4)
                    }
                },
                ["B-WING_E_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1,
						ResearchType = "BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(4)
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(4)
                    }
                },				
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 4,
                        Initial = 2
                    },
                    ["HOSTILE"] = {
                        Reserve = 4,
                        Initial = 2
                    },
                    ["WARLORDS"] = {
                        Reserve = 4,
                        Initial = 2
                    }
                },

                ["HOWLRUNNER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 4,
                        Initial = 2
                    }
                },		
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },		
        ["FRUORO"] = {
            Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["CLAWCRAFT_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
        ["MC90"] = {
			Ship_Crew_Requirement = 25,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["E-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["B-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
						ResearchType = "~BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = LessThan(4)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = LessThan(4)
                    }
                },
                ["B-WING_E_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
						ResearchType = "BwingE"
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(4)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
						TechLevel = GreaterOrEqualTo(4)
                    }
                },				
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SCIMMY_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["A9_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["HOWLRUNNER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["NEBULON_B_FRIGATE"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["Y-WING_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
		["NEBULON_B_TENDER"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true,
            }
        },
        ["COMBAT_ESCORT_CARRIER"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_HUNTER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["SACHEEN"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["B-WING_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1,
						ResearchType = "~BwingE"
                    }
                },
				["B-WING_E_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1,
						ResearchType = "BwingE"
                    }
                }
            }
        },
		["HAJEN"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["B-WING_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1,
						ResearchType = "~BwingE"
                    }
                },
				["B-WING_E_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 0,
                        Initial = 1,
						ResearchType = "BwingE"
                    }
                }
            }
        },
        ["PELTAST"] = {
            Ship_Crew_Requirement = 20,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["SCARSISS_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["CR90_ZSINJ"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer"
            },
            Fighters = {}
        },
        ["QUASAR_ZSINJ"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
			Fighters = {
				["TIE_FIGHTER_SQUADRON"] = {
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_RAPTOR_SQUADRON"] = {
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["PREYBIRD_SQUADRON_HALF"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    },					
                },
                ["TIE_TERROR_SQUADRON_HALF"] = {
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },					
                },				
                ["2_WARPOD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["IRDA_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["TIE_BOMBER_SQUADRON"] = {
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }				
            }
        },
        ["NEUTRON_STAR"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_AVENGER_SQUADRON"] = {
                    ["TERADOC"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SHIELDED_RAPTOR_SQUADRON"] = {
                    ["PIRATES"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },			
                ["TIE_AGGRESSOR_SQUADRON"] = {
                    ["PENTASTAR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["T-WING_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["BTLB_Y-WING_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
		["NEUTRON_INFLUENCE"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["CHIRDAKI_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 2
                    }
                }
			}
        },
        ["REBEL_GOLAN_THREE"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["E-WING_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 1,
                        TechLevel = GreaterThan(2)
                    }
                },
                ["REBEL_X-WING_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 1,
                        TechLevel = LessOrEqualTo(2)
                    }
                }
            }
        },
        ["BAOMU"] = {
            Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["FURION_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["DREADNAUGHT_EMPIRE"] = {
			Ship_Crew_Requirement = 20,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["HOWLRUNNER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 0,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },				
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }
            }
        },
        ["DREADNAUGHT_REBEL"] = {
			Ship_Crew_Requirement = 20,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                FIGHTERINHERIT = "DREADNAUGHT_EMPIRE"
            }
        },
        ["GENERIC_TECTOR"] = {
			Ship_Crew_Requirement = 30,
            Scripts = {
                "multilayer"
            },
            Fighters = {}
        },
        ["MAJESTIC"] = {
			Ship_Crew_Requirement = 15,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["E-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
						ResearchType = "~CoS_Tevv"
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
				["PREYBIRD_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
						ResearchType = "CoS_Tevv"
                    }
                },
                ["DEFENDER_STARFIGHTER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(3)
                    }
                },				
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    }
                },				
                ["B-WING_SQUADRON_HALF"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    }
                },
                ["K-WING_SQUADRON_HALF"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(3)
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SKIPRAY_SQUADRON_HALF"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }				
            }
        },
		["DEFENDER_CARRIER"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["E-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1,
						ResearchType = "~CoS_Tevv"
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
				["PREYBIRD_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1,
						ResearchType = "CoS_Tevv"
                    }
                },
                ["DEFENDER_STARFIGHTER_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(3)
                    }
                },			
                ["A-WING_SQUADRON_DOUBLE"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    }
                },				
                ["B-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessThan(3)
                    }
                },
                ["K-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterOrEqualTo(3)
                    }
                },
                ["TIE_FIGHTER_SQUADRON_DOUBLE"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["SKIPRAY_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }				
            }
        },
        ["CARRACK_CRUISER"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },			
            Fighters = {
                ["TIE_FIGHTER_SQUADRON_HALF"] = {
                    ["EMPIRE"] = {
                        Reserve = 0,
                        Initial = 1,
						TechLevel = LessOrEqualTo(3)
                    },
					["PENTASTAR"] = {
                        Reserve = 0,
                        Initial = 1
                    },
					["PIRATES"] = {
                        Reserve = 0,
                        Initial = 1
                    },
					["TERADOC"] = {
                        Reserve = 0,
                        Initial = 1
                    },
					["HUTTS"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["SUPER_TIE_SQUADRON_HALF"] = {
                    ["EMPIRE"] = {
                        Reserve = 0,
                        Initial = 1,
						TechLevel = EqualTo(4)
                    }					
                },
                ["TIE_INTERCEPTOR_SQUADRON_HALF"] = {
                    ["EMPIRE"] = {
                        Reserve = 0,
                        Initial = 1,
						TechLevel = GreaterThan(4)
                    }					
                }				
            }
        },
        ["STAR_GALLEON"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },			
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 0,
                        Initial = 1,
						TechLevel = LessOrEqualTo(3)
                    },
					["PENTASTAR"] = {
                        Reserve = 0,
                        Initial = 1
                    },
					["PIRATES"] = {
                        Reserve = 0,
                        Initial = 1
                    },
					["TERADOC"] = {
                        Reserve = 0,
                        Initial = 1
                    },
					["HUTTS"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                },
                ["SUPER_TIE_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 0,
                        Initial = 1,
						TechLevel = EqualTo(4)
                    }					
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 0,
                        Initial = 1,
						TechLevel = GreaterThan(4)
                    }					
                },
                ["IRD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 0,
                        Initial = 1
                    }
                }				
            }	
        },
        ["GALLEON"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Flags = {
                FIGHTERINHERIT = "STAR_GALLEON"
            }
        },		
        ["VICTORY_II_FRIGATE"] = {
			Ship_Crew_Requirement = 5,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
        ["NEBULON_B_ZSINJ"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1,
						ResearchType = "~V38"
                    },
					["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    },
					["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },
                ["BTLB_Y-WING_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
				["V38_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1,
						ResearchType = "V38"
                    }
                }
            }
        },
		["GENERIC_BELLATOR"] = {
			Ship_Crew_Requirement = 150,
            Scripts = {
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 7,
                        Initial = 3
                    }
                },
				["TIE_BOMBER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 3,
                        Initial = 1
                    }
                }
            }
        },
		["MEGADOR"] = {
            Scripts = {
                "fighter-spawn"
            },
            Flags = {
                FIGHTERINHERIT = "GENERIC_BELLATOR"
            }
        },
		["DOMINION"] = {
            Scripts = {
                "fighter-spawn"
            },
            Flags = {
                FIGHTERINHERIT = "GENERIC_BELLATOR"
            }
        },
		["ECLIPSE_STAR_DESTROYER"] = {
			Ship_Crew_Requirement = 300,
            Scripts = {
                "interdictor-ai",
				"fighter-spawn"
            },
            Flags = {
                HANGAR = true
            },
			 Fighters = {
				["TIE_FIGHTER_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 7,
                        Initial = 3
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
				["TIE_DROID_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 7,
                        Initial = 3
                    }
                },
                ["TIE_DROID_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 5,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 3,
                        Initial = 1
                    }
                }
            }
        },
		["GENERIC_VENGEANCE"] = {
			Ship_Crew_Requirement = 300,
            Scripts = {
				"fighter-spawn"
            },
            Flags = {
                HANGAR = true
            },
			 Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["DEFAULT"] = {
						Reserve = 6,
						Initial = 2
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 6,
                        Initial = 2
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 6,
                        Initial = 2
                    }
                },
				["TIE_GT_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 6,
                        Initial = 2
                    }
                }
            }
        },
        ["GENERIC_EXECUTOR"] = {
			Ship_Crew_Requirement = 300,
            Scripts = {
                "fighter-spawn"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 9,
                        Initial = 3,
                        TechLevel = IsOneOf({1, 2, 3, 5})
                    }
                },
                ["SUPER_TIE_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 9,
                        Initial = 3,
						TechLevel = EqualTo(4)
                    }					
                },	
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 9,
                        Initial = 3
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 6,
                        Initial = 2
                    }
                },
				["SHIELDED_INTERCEPTOR_SQUADRON"] = {
                    ["HUTTS"] = {
                        Reserve = 3,
                        Initial = 1
                    },
					["PENTASTAR"] = {
                        Reserve = 3,
                        Initial = 1
                    },
					["TERADOC"] = {
                        Reserve = 3,
                        Initial = 1
                    }
                },
				["SHIELDED_RAPTOR_SQUADRON"] = {
                    ["PIRATES"] = {
                        Reserve = 3,
                        Initial = 1
                    }
                },
				["TIE_DEFENDER_SQUADRON_HALF"] = {
                    ["EMPIRE"] = {
                        Reserve = 3,
                        Initial = 1
                    }
                }
            }
        },
		["KNIGHT_HAMMER"] = {
			Ship_Crew_Requirement = 300,
            Scripts = {
                "fighter-spawn"
            },
            Flags = {
                HANGAR = true,
				FIGHTERINHERIT = "GENERIC_EXECUTOR"
            }
        },
        ["MTC_SENSOR"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["TIE_DROID_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 3,
                        Initial = 2
                    },
                    ["HOSTILE"] = {
                        Reserve = 3,
                        Initial = 2
                    },
                    ["WARLORDS"] = {
                        Reserve = 3,
                        Initial = 2
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["SKIPRAY_SQUADRON_HALF"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }					
                },				
                ["IRD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 3,
                        Initial = 2
                    }
                },
                ["B-EARLY_SKIPRAY_SQUADRON_HALF"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["BTLB_Y-WING_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },				
                ["SHIELDED_TIE_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 3,
                        Initial = 2
                    }
                },
                ["B-WING_SQUADRON_HALF"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["MTC_CSA"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Flags = {
                FIGHTERINHERIT = "MTC_SENSOR"
            }
        },
        ["ISD_1_NR"] = {
			Ship_Crew_Requirement = 30,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                FIGHTERINHERIT = "GENERIC_STAR_DESTROYER"
            }
        },
        ["DAUNTLESS"] = {
			Ship_Crew_Requirement = 15,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["IRD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["2_WARPOD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["BULWARK_I"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["MANKVIM_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["BTLB_Y-WING_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                }
            }
        },
        ["BULWARK_III"] = {
			Ship_Crew_Requirement = 40,
            Scripts = {
                "multilayer",
                "fighter-spawn"
            },
            Fighters = {
                ["IRD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["PREYBIRD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    },					
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["REBEL_X-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["2_WARPOD_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["EARLY_SKIPRAY_SQUADRON"] = {
                    ["CORPORATE_SECTOR"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    },					
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SKIPRAY_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },	
                ["T-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },				
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["Y-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["IPV1_SYSTEM_PATROL_CRAFT"] = {
			Ship_Crew_Requirement = 1,
            Scripts = {
                "multilayer",
                "single-unit-retreat"
            },
            Fighters = {}
        },
        ["STRIKE_CRUISER"] = {
			Ship_Crew_Requirement = 10,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                HANGAR = true
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1,
                        TechLevel = LessOrEqualTo(3)
                    },
                    ["PENTASTAR"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["PIRATES"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["TERADOC"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HUTTS"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 2,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["SUPER_TIE_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1,
                        TechLevel = EqualTo(4)
                    }
                },
                ["HOWLRUNNER_SQUADRON"] = {
                    ["EMPIRE"] = {
                        Reserve = 2,
                        Initial = 1,
                        TechLevel = GreaterThan(4)
                    }
                },				
                ["Z95_HEADHUNTER_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["GENERIC_PRAETOR"] = {
			Ship_Crew_Requirement = 150,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Fighters = {
                ["TIE_FIGHTER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 3,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["DEFAULT"] = {
                        Reserve = 2,
                        Initial = 1
                    }
                }
            }
        },
        ["REBEL_GOLAN_TWO"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["E-WING_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = GreaterThan(2)
                    }
                },
                ["REBEL_X-WING_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 1,
                        Initial = 1,
                        TechLevel = LessOrEqualTo(2)
                    }
                }
            }
        },
        ["BLACK_15"] = {
            Scripts = {
                "fighter-spawn"
            },
            Fighters = {
                ["TRIFOIL_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 5,
                        Initial = 2
                    }
                }
            }
        },
        ["SLAYN_KORPIL"] = {
            Scripts = {
                "turn-station",
                "fighter-spawn"
            },
            Fighters = {
                ["B-WING_SQUADRON_DOUBLE"] = {
                    ["DEFAULT"] = {
                        Reserve = 5,
                        Initial = 3
                    }
                }
            }
        },
        ["PHALANX_DESTROYER"] = {
            Ship_Crew_Requirement = 30,
            Scripts = {
                "multilayer",
                "fighter-spawn",
                "single-unit-retreat"
            },
            Flags = {
                FighterDespawnFactor = 2.5
            },
            Fighters = {
                ["B-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["SCARSISS_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 2,
                        TechLevel = GreaterThan(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 2,
                        TechLevel = GreaterThan(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 2,
                        TechLevel = GreaterThan(3)
                    }
                },
                ["TIE_INTERCEPTOR_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 2
                    }
                },
                ["A-WING_SQUADRON"] = {
                    ["REBEL"] = {
                        Reserve = 1,
                        Initial = 2
                    }
                },
                ["SYCA_BOMBER_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 1
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["TIE_BOMBER_SQUADRON"] = {
                    ["IMPERIAL"] = {
                        Reserve = 1,
                        Initial = 1
                    }
                },
                ["CLAWCRAFT_SQUADRON"] = {
                    ["EMPIREOFTHEHAND"] = {
                        Reserve = 1,
                        Initial = 2,
                        TechLevel = LessOrEqualTo(3)
                    },
                    ["HOSTILE"] = {
                        Reserve = 1,
                        Initial = 2,
                        TechLevel = LessOrEqualTo(3)
                    },
                    ["WARLORDS"] = {
                        Reserve = 1,
                        Initial = 2,
                        TechLevel = LessOrEqualTo(3)
                    }
                }
            }
        }
    }
}
return GameObjectLibrary