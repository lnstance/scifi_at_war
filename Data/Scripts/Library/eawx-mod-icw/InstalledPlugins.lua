--**************************************************************************************************
--*    _______ __                                                                                  *
--*   |_     _|  |--.----.---.-.--.--.--.-----.-----.                                              *
--*     |   | |     |   _|  _  |  |  |  |     |__ --|                                              *
--*     |___| |__|__|__| |___._|________|__|__|_____|                                              *
--*    ______                                                                                      *
--*   |   __ \.-----.--.--.-----.-----.-----.-----.                                                *
--*   |      <|  -__|  |  |  -__|     |  _  |  -__|                                                *
--*   |___|__||_____|\___/|_____|__|__|___  |_____|                                                *
--*                                   |_____|                                                      *
--*                                                                                                *
--*                                                                                                *
--*       File:              InstalledPlugins.lua                                                  *
--*       File Created:      Saturday, 22nd February 2020 05:27                                    *
--*       Author:            [TR] Pox                                                              *
--*       Last Modified:     Monday, 24th February 2020 01:38                                      *
--*       Modified By:       [TR] Pox                                                              *
--*       Copyright:         Thrawns Revenge Development Team                                      *
--*       License:           This code may not be used without the author's explicit permission    *
--**************************************************************************************************

return {
    -- "timed-dummy-lifecycle-handler",
    "key-dummy-lifecycle-handler",
    "category-filter",
    "galactic-display",
    "galactic-events-news",
    "abstract-resources",
    "selected-planet-listener",
    "invading-fleet-listener",
    "influence-service",
    "icw-governments",
    "boarding-listener",
	--"production-listener"
    "resource-manager",
    "revolt-manager",
    "blockade-attrition",
    "bounty-hunters"
}
