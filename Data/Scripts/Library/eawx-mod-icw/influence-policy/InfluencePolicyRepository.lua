InfluencePolicyRepository = {
    DEFAULT = function(planet)
        local chief_of_state = GlobalValue.Get("ChiefOfState")
        if planet:get_owner() == Find_Player("Rebel") and chief_of_state == "DUMMY_CHIEFOFSTATE_LEIA" then
            return 1
        end

        return 0
    end
}

---@param planet Planet
function InfluencePolicyRepository.chandrila(planet)
    local chief_of_state = GlobalValue.Get("ChiefOfState")
    if chief_of_state ==  "DUMMY_CHIEFOFSTATE_MOTHMA" and planet:get_owner() == Find_Player("Rebel")then
        return 10
    end

    return 0
end

---@param planet Planet
function InfluencePolicyRepository.coruscant(planet)
    local chief_of_state = GlobalValue.Get("ChiefOfState")
    if chief_of_state ==  "DUMMY_CHIEFOFSTATE_MOTHMA" and planet:get_owner() == Find_Player("Rebel")then
        return 10
    end

    return 0
end

---@param planet Planet
function InfluencePolicyRepository.bothawui(planet)
    local chief_of_state = GlobalValue.Get("ChiefOfState")
    if chief_of_state == "DUMMY_CHIEFOFSTATE_FEYLYA" and planet:get_owner() == Find_Player("Rebel") then
         return 10
    end

    return 0
end

---@param planet Planet
function InfluencePolicyRepository.sullust(planet)
    local chief_of_state = GlobalValue.Get("ChiefOfState")
    if chief_of_state == "DUMMY_CHIEFOFSTATE_TEVV" and planet:get_owner() == Find_Player("Rebel") then
         return 10
    end

    return 0
end

---@param planet Planet
function InfluencePolicyRepository.duro(planet)
    local chief_of_state = GlobalValue.Get("ChiefOfState")
    if chief_of_state == "DUMMY_CHIEFOFSTATE_SOBILLES" and planet:get_owner() == Find_Player("Rebel") then
         return 10
    end

    return 0
end

---@param planet Planet
function InfluencePolicyRepository.rodia(planet)
    local chief_of_state = GlobalValue.Get("ChiefOfState")
    if chief_of_state == "DUMMY_CHIEFOFSTATE_NAVIK" and planet:get_owner() == Find_Player("Rebel") then
         return 10
    end

    return 0
end

---@param planet Planet
function InfluencePolicyRepository.hapes(planet)
    local chief_of_state = GlobalValue.Get("ChiefOfState")
    if chief_of_state ==  "DUMMY_CHIEFOFSTATE_GAVRISOM" and planet:get_owner() == Find_Player("Rebel")then
        return 4
    end

    return 0
end

---@param planet Planet
function InfluencePolicyRepository.ryloth(planet)
    local chief_of_state = GlobalValue.Get("ChiefOfState")
    if chief_of_state ==  "DUMMY_CHIEFOFSTATE_GAVRISOM" and planet:get_owner() == Find_Player("Rebel")then
        return 4
    end

    return 0
end

---@param planet Planet
function InfluencePolicyRepository.nzoth(planet)
    local chief_of_state = GlobalValue.Get("ChiefOfState")
    if chief_of_state ==  "DUMMY_CHIEFOFSTATE_GAVRISOM" and planet:get_owner() == Find_Player("Rebel")then
        return 4
    end

    return 0
end

---@param planet Planet
function InfluencePolicyRepository.mandalore(planet)
    local chief_of_state = GlobalValue.Get("ChiefOfState")
    if chief_of_state ==  "DUMMY_CHIEFOFSTATE_GAVRISOM" and planet:get_owner() == Find_Player("Rebel")then
        return 4
    end

    return 0
end

---@param planet Planet
function InfluencePolicyRepository.hapes(planet)
    local chief_of_state = GlobalValue.Get("ChiefOfState")
    if chief_of_state ==  "DUMMY_CHIEFOFSTATE_GAVRISOM" and planet:get_owner() == Find_Player("Rebel")then
        return 4
    end

    return 0
end

---@param planet Planet
function InfluencePolicyRepository.kashyyyk(planet)
    if planet:get_owner() ~= Find_Player("Rebel") then
        return -2
    end

    return 0
end
