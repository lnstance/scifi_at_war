return {
    type = "plugin",
    target = "frame-update",
    dependencies = {},
    init = function(self, ctx)
        VictoryPoint = require("eawx-plugins-gameobject-land/victory-point/VictoryPoint")
        return VictoryPoint()
    end
}
