return {
    type = "plugin",
    target = "frame-update",
    init = function(self, ctx)
        ModContentLoader.get("GameObjectLibrary")
        FighterSpawn = require("eawx-plugins-gameobject-space/fighter-spawn/FighterSpawn")
        local unit_entry = GameObjectLibrary.Units[Object.Get_Type().Get_Name()]
		if unit_entry.Flags then
			if unit_entry.Flags.FIGHTERINHERIT ~= nil then
				return FighterSpawn(GameObjectLibrary.Units[unit_entry.Flags.FIGHTERINHERIT])
			end
		end
        return FighterSpawn(unit_entry)
    end
}
