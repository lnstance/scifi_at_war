return {
    type = "plugin",
    target = "frame-update",
    init = function(self, ctx)
        InterdictorAi = require("eawx-plugins-gameobject-space/interdictor-ai/InterdictorAI")
        return InterdictorAi()
    end
}
