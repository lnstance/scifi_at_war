return {
    type = "plugin",
    target = "frame-update",
    init = function(self, ctx)
        Microjump = require("eawx-plugins-gameobject-space/microjump/Microjump")
        return Microjump()
    end
}
