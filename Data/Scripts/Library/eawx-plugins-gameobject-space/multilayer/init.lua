return {
    type = "plugin",
    target = "passive",
    init = function(self, ctx)
        MultiLayer = require("eawx-plugins-gameobject-space/multilayer/MultiLayer")
        return MultiLayer()
    end
}
