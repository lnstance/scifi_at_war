return {
    type = "plugin",
    target = "frame-update",
    dependencies = {"fighter-spawn"},
    init = function(self, ctx, fighter_spawn)
        SingleUnitRetreat = require("eawx-plugins-gameobject-space/single-unit-retreat/SingleUnitRetreat")
        return SingleUnitRetreat(fighter_spawn)
    end
}
