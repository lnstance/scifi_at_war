return {
    type = "plugin",
    target = "frame-update",
    init = function(self, ctx)
        TurnStation = require("eawx-plugins-gameobject-space/turn-station/TurnStation")
        return TurnStation()
    end
}
