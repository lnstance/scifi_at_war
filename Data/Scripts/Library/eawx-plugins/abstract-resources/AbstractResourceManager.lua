--**************************************************************************************************
--*    _______ __                                                                                  *
--*   |_     _|  |--.----.---.-.--.--.--.-----.-----.                                              *
--*     |   | |     |   _|  _  |  |  |  |     |__ --|                                              *
--*     |___| |__|__|__| |___._|________|__|__|_____|                                              *
--*    ______                                                                                      *
--*   |   __ \.-----.--.--.-----.-----.-----.-----.                                                *
--*   |      <|  -__|  |  |  -__|     |  _  |  -__|                                                *
--*   |___|__||_____|\___/|_____|__|__|___  |_____|                                                *
--*                                   |_____|                                                      *
--*                                                                                                *
--*                                                                                                *
--*       File:              AbstractResourceManager.lua                                           *
--*       File Created:      Sunday, 23rd February 2020 08:26                                      *
--*       Author:            [TR] Corey                                                            *
--*       Last Modified:     Sunday, 23rd February 2020 10:22                                      *
--*       Modified By:       [TR] Corey                                                            *
--*       Copyright:         Thrawns Revenge Development Team                                      *
--*       License:           This code may not be used without the author's explicit permission    *
--**************************************************************************************************
require("eawx-std/class")
require("pgevents")
require("pgbase")

---@class AbstractResourceManager
AbstractResourceManager = class()

---@param human_player FactionObject
---@param planets table<string, Planet>
function AbstractResourceManager:new(human_player, resource_manager)
    self.plot = Get_Story_Plot("Player_Agnostic_Plot.xml")

    GlobalValue.Set("FIGHTER_PILOTS", 100)

    self.human_player = human_player 
    self.resource_manager = resource_manager

    self.FoodModifier = 0
    self.DurasteelModifier = 0
    self.ShipCrewIncome = 0
    self.InfluenceModifier = 0
    self.ResourceInfluenceModifier = 0
    self.ShipCrewIncomeDisplay = "TEXT_RESOURCE_" .. tostring(self.ShipCrewIncome)

    self.PlanetaryFoodPositive = 0
    self.PlanetaryFoodNegative = 0
    self.FoodIncome = 0
    self.FoodExpenditure = 0
    self.FoodNet = 0
    self.FoodModifier = 0

    self.PlanetaryDurasteelPositive = 0
    self.PlanetaryDurasteelNegative = 0
    self.DurasteelIncome = 0
    self.DurasteelExpenditure = 0
    self.DurasteelNet = 0
    self.DurasteelModifier = 0

    self.PilotIncome = 0
    self.PilotsOld = 0
    self.Pilots = 0

    self.ModifierTiers = {1, 0, -5, -10 }

    self.all_planets = FindPlanet.Get_All_Planets()

end

function AbstractResourceManager:update()

    self:planetaryvalues()
    self:food()
    self:durasteel()
    --self:pilots()
    self:shipcrews()
    
    self:DetermineResourceInfluenceModifier()
    self:UpdateDisplay()

end

function AbstractResourceManager:planetaryvalues()

    self.PlanetaryFoodPositive = 0
    self.PlanetaryFoodNegative = 0

    self.PlanetaryDurasteelPositive = 0
    self.PlanetaryDurasteelNegative = 0

    for _, planet in pairs(self.all_planets) do
        if planet.Get_Owner() == self.human_player then
            self.PlanetaryFoodNegative = self.PlanetaryFoodNegative + EvaluatePerception("Resource_Food_Modifier_Negative", self.human_player, planet)
            self.PlanetaryFoodPositive = self.PlanetaryFoodPositive + EvaluatePerception("Resource_Food_Modifier_Positive", self.human_player, planet)
            self.PlanetaryDurasteelNegative = self.PlanetaryDurasteelNegative + EvaluatePerception("Resource_Durasteel_Modifier_Negative", self.human_player, planet)
            self.PlanetaryDurasteelPositive = self.PlanetaryDurasteelPositive + EvaluatePerception("Resource_Durasteel_Modifier_Positive", self.human_player, planet)
        end
    end
  

end

function AbstractResourceManager:food()

    self.FoodIncome = EvaluatePerception("Farmland_Count", self.human_player) + self.PlanetaryFoodPositive

    self.FoodExpenditure = EvaluatePerception("Barracks_Count", self.human_player) + EvaluatePerception("GovernedPlanet_Count", self.human_player) + self.PlanetaryFoodNegative



    self.FoodNet = self.FoodIncome - self.FoodExpenditure 

    self.FoodModifier = 1
    for i, tier in self.ModifierTiers do
        if self.FoodNet < tier then
            self.FoodModifier = self.FoodModifier - 1
        end
    end

end

function AbstractResourceManager:durasteel()

    self.DurasteelIncome = EvaluatePerception("IndustrialParts_Count", self.human_player)  + self.PlanetaryDurasteelPositive 
    self.DurasteelExpenditure = EvaluatePerception("LightFactory_Count", self.human_player) + (1*(EvaluatePerception("HeavyFactory_Count", self.human_player))) + EvaluatePerception("GovernedPlanet_Count", self.human_player) + self.PlanetaryDurasteelNegative

    self.DurasteelNet = self.DurasteelIncome - self.DurasteelExpenditure

    self.DurasteelModifier = 1
    for i, tier in self.ModifierTiers do
        if self.DurasteelNet < tier then
            self.DurasteelModifier = self.DurasteelModifier - 1
        end
    end


end

function AbstractResourceManager:pilots()

    self.PilotIncome = (10 * EvaluatePerception("PilotAcademy_Count", self.human_player)) + (25 * EvaluatePerception("CloningFacility_Count", self.human_player))
    
    self.PilotsOld = GlobalValue.Get("FIGHTER_PILOTS")

    self.Pilots = self.PilotIncome + self.PilotsOld

    GlobalValue.Set("FIGHTER_PILOTS", self.Pilots)

end

function AbstractResourceManager:shipcrews()

    self.ShipCrewIncome = (10 * EvaluatePerception("NavalAcademy_Count", self.human_player)) + (30 * EvaluatePerception("CloningFacility_Count", self.human_player))

    if self.human_player == Find_Player("Rebel") then
        local chief_of_state = GlobalValue.Get("ChiefOfState")
        if chief_of_state ==  "DUMMY_CHIEFOFSTATE_MOTHMA" then
            self.ShipCrewIncome = Dirty_Floor(self.ShipCrewIncome * 1.15)
        end
    end

    self.ShipCrewIncomeDisplay = "TEXT_RESOURCE_" .. tostring(self.ShipCrewIncome)
    
    self.resource_manager:add_resources(self.ShipCrewIncome)

end


function AbstractResourceManager:DetermineResourceInfluenceModifier()

    self.ResourceInfluenceModifier = self.FoodModifier + self.DurasteelModifier

    -- For some reason if it's a negative and a negative added together, it ends up offset by -1. I don't know why, but it needs to be adjusted.
    if self.ResourceInfluenceModifier < 0 then
        self.ResourceInfluenceModifier = self.ResourceInfluenceModifier + 1
    end

    return self.ResourceInfluenceModifier
end

function AbstractResourceManager:UpdateDisplay()
   
    local resource_display_event = self.plot.Get_Event("Abstract_Resource_Display")

    resource_display_event.Clear_Dialog_Text()
    resource_display_event.Add_Dialog_Text("TEXT_RESOURCES_OVERVIEW")

    resource_display_event.Add_Dialog_Text("TEXT_NONE")
    resource_display_event.Add_Dialog_Text("TEXT_RESOURCES_PREVIOUS")
    resource_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")

    resource_display_event.Add_Dialog_Text("STAT_SHIPCREWS_NAME")
	resource_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_SEPARATOR_MEDIUM")
    resource_display_event.Add_Dialog_Text("STAT_SHIPCREWS_DESCRIPTION") 
    resource_display_event.Add_Dialog_Text(self.ShipCrewIncomeDisplay)

    resource_display_event.Add_Dialog_Text("TEXT_NONE")

    resource_display_event.Add_Dialog_Text("STAT_RESOURCE_INFLUENCE", self.ResourceInfluenceModifier)

    resource_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")

    resource_display_event.Add_Dialog_Text("STAT_FOODS_NAME")
	resource_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_SEPARATOR_MEDIUM")
    resource_display_event.Add_Dialog_Text("STAT_FOODS_DESCRIPTION") 
    resource_display_event.Add_Dialog_Text("STAT_FOODS_FLOW", self.FoodExpenditure,  self.FoodIncome)
    resource_display_event.Add_Dialog_Text("STAT_FOODS_NET", self.FoodNet)
    resource_display_event.Add_Dialog_Text("STAT_FOODS_INFLUENCE", self.FoodModifier)

    resource_display_event.Add_Dialog_Text("TEXT_NONE")

    resource_display_event.Add_Dialog_Text("STAT_DURASTEEL_NAME")
	resource_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_SEPARATOR_MEDIUM")
    resource_display_event.Add_Dialog_Text("STAT_DURASTEEL_DESCRIPTION") 
    resource_display_event.Add_Dialog_Text("STAT_DURASTEEL_FLOW", self.DurasteelExpenditure, self.DurasteelIncome)
    resource_display_event.Add_Dialog_Text("STAT_DURASTEEL_NET", self.DurasteelNet)
    resource_display_event.Add_Dialog_Text("STAT_DURASTEEL_INFLUENCE", self.DurasteelModifier)

    --resource_display_event.Add_Dialog_Text("TEXT_NONE")

    --resource_display_event.Add_Dialog_Text("STAT_PILOTS_NAME")
	--resource_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_SEPARATOR_MEDIUM")
    --resource_display_event.Add_Dialog_Text("STAT_PILOTS_DESCRIPTION") 
    --resource_display_event.Add_Dialog_Text("STAT_PILOTS_NET", GlobalValue.Get("FIGHTER_PILOTS"))
    --resource_display_event.Add_Dialog_Text("STAT_PILOTS_FLOW", self.PilotIncome)

end

