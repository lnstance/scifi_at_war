--**************************************************************************************************
--*    _______ __                                                                                  *
--*   |_     _|  |--.----.---.-.--.--.--.-----.-----.                                              *
--*     |   | |     |   _|  _  |  |  |  |     |__ --|                                              *
--*     |___| |__|__|__| |___._|________|__|__|_____|                                              *
--*    ______                                                                                      *
--*   |   __ \.-----.--.--.-----.-----.-----.-----.                                                *
--*   |      <|  -__|  |  |  -__|     |  _  |  -__|                                                *
--*   |___|__||_____|\___/|_____|__|__|___  |_____|                                                *
--*                                   |_____|                                                      *
--*                                                                                                *
--*                                                                                                *
--*       File:              BountyHunters.lua                                                     *
--*       File Created:      Monday, 24th February 2020 02:19                                      *
--*       Author:            [TR] Kiwi                                                             *
--*       Last Modified:     Monday, 24th February 2020 02:34                                      *
--*       Modified By:       [TR] Kiwi                                                             *
--*       Copyright:         Thrawns Revenge Development Team                                      *
--*       License:           This code may not be used without the author's explicit permission    *
--**************************************************************************************************

require("eawx-std/class")
require("eawx-util/StoryUtil")
require("PGDebug")

BountyHunters = class()

function BountyHunters:new(production_finished_event, human_player)
    self.human_player = human_player
    self.BountyHunterObject = nil
    self.bounty_hunter_purchased = {}
    self.BountyHunterHeroes = {
        "Boba_Fett_Team",
        "Dengar_Team",
        "Bossk_Team"
    }
    production_finished_event:AttachListener(self.on_production_finished, self)
end

function BountyHunters:on_production_finished(planet, object_type_name)
    if not object_type_name == "RANDOM_BOUNTY_HUNTER" then
        return
    end

    if table.getn(self.BountyHunterHeroes) == 0 then
        local RandomBountyHunter = Find_First_Object("Random_Bounty_Hunter")
        if RandomBountyHunter ~= nil then
            local BountyHunterOwner = RandomBountyHunter.Get_Owner()
            if BountyHunterOwner == self.human_player then
                StoryUtil.ShowScreenText("TEXT_TOOLTIP_NO_FREE_BOUNTY_HUNTERS", 5)
            end
            BountyHunterOwner.Give_Money(3000)
            if TestValid(RandomBountyHunter) then
                RandomBountyHunter.Despawn()
            end
        end
        return
    else
        local looper = 0
        for j, unit in pairs(self.BountyHunterHeroes) do
            local looper = looper + 1
            if GameRandom.Free_Random(1,100) >= 50 then
                bounty_hunter_to_spawn = unit
                table.remove(self.BountyHunterHeroes, j)
                break
            end
            --Stops there being no unit spawned
            if looper >= 3 then
                bounty_hunter_to_spawn = unit
                table.remove(self.BountyHunterHeroes, j)
            end
        end
        local RandomBountyHunter = Find_First_Object("Random_Bounty_Hunter")

        if TestValid(RandomBountyHunter) then
            local BountyHunterOwner = RandomBountyHunter.Get_Owner()
            local BountyHunterLocation = RandomBountyHunter.Get_Planet_Location()
            -- Issue is unit appears to be invalid
            local BountyHunterUnit = Find_Object_Type(bounty_hunter_to_spawn)
            Spawn_Unit(BountyHunterUnit, BountyHunterLocation, BountyHunterOwner)
            RandomBountyHunter.Despawn()
        end
    end
end