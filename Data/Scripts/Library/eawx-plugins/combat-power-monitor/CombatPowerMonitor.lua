require("eawx-std/class")
require("eawx-util/GalacticUtil")


---@class CombatPowerMonitor
CombatPowerMonitor = class()

---@param galactic_conquest GalacticConquest
---@param dummy_lifecycle_handler KeyDummyLifeCycleHandler
---@param category_filter CategoryFilter
function CombatPowerMonitor:new(galactic_conquest, dummy_lifecycle_handler, category_filter)
    self.power_tiers = {
        0,
        2800,
        6000,
        10000
    }

    self.starbase_types = {
        "Generic_Star_Base_1",
        "Generic_Star_Base_2",
        "Generic_Star_Base_3",
        "Generic_Star_Base_4"
    }

    self.dummy_lifecycle_handler = dummy_lifecycle_handler
    self.category_filter = category_filter

    ---@type table<Planet, number>
    self.current_planet_starbase_level = {}
    self.number_of_planets = 0
    for _, planet in pairs(galactic_conquest.Planets) do
        self.current_planet_starbase_level[planet] = 1
        self.number_of_planets = self.number_of_planets + 1
    end

    self.last_update = -1
    self.interval = 10
    self.planet_loop_index = 0
end


---@param planet Planet
function CombatPowerMonitor:update(planet)
    if GetCurrentTime() < self.last_update + self.interval then
        return
    end

    self.planet_loop_index = self.planet_loop_index + 1

    if planet:get_owner() == Find_Player("Neutral") then
        return
    end

    if EvaluatePerception("Space_Contrast", planet:get_owner(), planet:get_game_object()) > 0 then
        return
    end

    local current_starbase_level = EvaluatePerception("Get_Starbase_Level", planet:get_owner(), planet:get_game_object())
    DebugMessage("Current starbase level on %s is %s", tostring(planet:get_name()), tostring(current_starbase_level))

    local combat_rating = EvaluatePerception("Defense_Station_Combat_Value", planet:get_owner(), planet:get_game_object())
    DebugMessage("Combat rating for planet %s is %s", tostring(planet:get_name()), tostring(combat_rating))

     for index, cr in ipairs(self.power_tiers) do
        if combat_rating <= cr then
            if index ~= current_starbase_level then
                local starbase_type_string = self.starbase_types[index]
                local sb_type = Find_Object_Type(starbase_type_string)
                Spawn_Unit(sb_type, planet:get_game_object(), planet:get_owner())
                DebugMessage("Spawned starbase %s on %s", starbase_type_string, tostring(planet:get_name()))
            end
            break
        end
    end

    if self.planet_loop_index >= self.number_of_planets then
        self.planet_loop_index = 0
        self.last_update = GetCurrentTime()
    end
end

function CombatPowerMonitor:get_planet_combat_rating(planet)
    local structure_info = planet:get_orbital_structure_types_and_amount()
    local total_combat_rating = 0
    for structure_type_name, amount in pairs(structure_info) do
        local structure_type = Find_Object_Type(structure_type_name)
        if structure_type then
            local combat_rating = structure_type.Get_Combat_Rating()
            if combat_rating then
                total_combat_rating = total_combat_rating + combat_rating * amount
            end
        end
    end

    return total_combat_rating
end
