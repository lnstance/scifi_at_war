require("eawx-plugins/combat-power-monitor/CombatPowerMonitor")

return {
    target = "frame-planet-update",
    dependencies = {"key-dummy-lifecycle-handler", "category-filter"},
    init = function(self, ctx, dummy_lifecycle_handler, category_filter)
        return CombatPowerMonitor(ctx.galactic_conquest, dummy_lifecycle_handler, category_filter)
    end
}