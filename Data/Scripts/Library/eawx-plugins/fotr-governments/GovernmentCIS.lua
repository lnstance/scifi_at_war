require("eawx-std/class")

---@class GovernmentCIS
GovernmentCIS = class()

function GovernmentCIS:new(player_agnostic_plot)
    self.CISPlayer = Find_Player("Rebel")

    GlobalValue.Set("IGBCApprovalRating", 50)
    GlobalValue.Set("CommerceApprovalRating", 50)
    GlobalValue.Set("TechnoApprovalRating", 50)
    GlobalValue.Set("TradeFedApprovalRating", 50)
    self.LastCycleTime = 0
end

function GovernmentCIS:Update()
    local current = GetCurrentTime()
    if current - self.LastCycleTime >= 40 then
        self.LastCycleTime = current
    end

    if self.CISPlayer.Is_Human() then
        self:UpdateDisplay()
    end
end

function GovernmentCIS:UpdateDisplay()
    local plot = Get_Story_Plot("Player_Agnostic_Plot.xml")
    local government_display_event = plot.Get_Event("Government_Display")

    government_display_event.Clear_Dialog_Text()

    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_CIS")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_CIS_FUNCTION")
    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_CIS_MEMBERCORPS")

    liveFactionTable = {
        Find_Player("Teradoc"),
        Find_Player("Pentastar"),
        Find_Player("Pirates"),
        Find_Player("Hutts")
    }

    for _, faction in pairs(liveFactionTable) do
        numPlanets = EvaluatePerception("Planet_Ownership", faction)

            government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
            government_display_event.Add_Dialog_Text(CONSTANTS.ALL_FACTION_TEXTS[faction.Get_Faction_Name()])
            government_display_event.Add_Dialog_Text("STAT_PLANET_COUNT", numPlanets)
            if faction == Find_Player("Teradoc") then
                government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_CIS_APPROVAL", GlobalValue.Get("CommerceApprovalRating"))
            elseif faction == Find_Player("Pirates") then
                government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_CIS_APPROVAL", GlobalValue.Get("TradeFedApprovalRating"))
            elseif faction == Find_Player("Hutts") then
                government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_CIS_APPROVAL", GlobalValue.Get("TechnoApprovalRating"))
            elseif faction == Find_Player("Pentastar") then
                government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_CIS_APPROVAL", GlobalValue.Get("IGBCApprovalRating"))
            end

    end
  
end

return GovernmentCIS
