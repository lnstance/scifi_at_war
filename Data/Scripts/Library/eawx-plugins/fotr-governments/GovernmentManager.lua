require("eawx-std/class")
require("eawx-plugins/fotr-governments/GovernmentRepublic")
require("eawx-plugins/fotr-governments/GovernmentCIS")

---@class GovernmentManager
GovernmentManager = class()

function GovernmentManager:new()
    self.REPGOV = GovernmentRepublic()
    self.CISGOV = GovernmentCIS()
end

function GovernmentManager:update()
    self.REPGOV:Update()
    self.CISGOV:Update()
end

return GovernmentManager
