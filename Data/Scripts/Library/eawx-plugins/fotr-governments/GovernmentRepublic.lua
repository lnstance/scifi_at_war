require("eawx-std/class")
StoryUtil = require("eawx-util/StoryUtil")

---@class GovernmentRepublic
GovernmentRepublic = class()

function GovernmentRepublic:new(player_agnostic_plot)
    self.RepublicPlayer = Find_Player("Empire")

    GlobalValue.Set("ChiefOfState", "DUMMY_CHIEFOFSTATE_PALPATINE")
    GlobalValue.Set("ChiefOfStatePreference", "DUMMY_CHIEFOFSTATE_PALPATINE")
    GlobalValue.Set("RepublicApprovalRating", 0)
    self.LastCycleTime = 0
    self.ApprovalRatingInfluence = 0
    self.ChoiceMade = false

    self.spawn_location_table = {
        ["KUAT"] = false
    }
    local all_planets = FindPlanet.Get_All_Planets()

    for _, planet in pairs(all_planets) do
        local name = planet.Get_Type().Get_Name()

        if self.spawn_location_table[name] ~= nil then
            self.spawn_location_table[name] = true
        end
    end
    
    --self.Events = {}
    --self.Events.ElectionHeld = Observable()
end

function GovernmentRepublic:Update()
    local current = GetCurrentTime()
    if current - self.LastCycleTime >= 40 then
        self:ApprovalRating()
        self:KDYContracts()
        self.LastCycleTime = current
    end

    if self.RepublicPlayer.Is_Human() then
        self:UpdateDisplay()
    end
end

function GovernmentRepublic:ApprovalRating()
    local lowInfluenceCount = 0
    local highInfluenceCount = 0

    local oldValue = self.ApprovalRatingInfluence

    local influenceOneList = Find_All_Objects_Of_Type("INFLUENCE_ONE", self.RepublicPlayer)
    lowInfluenceCount = lowInfluenceCount + table.getn(influenceOneList)

    local influenceTwoList = Find_All_Objects_Of_Type("INFLUENCE_TWO", self.RepublicPlayer)
    lowInfluenceCount = lowInfluenceCount + table.getn(influenceTwoList)

    local influenceThreeList = Find_All_Objects_Of_Type("INFLUENCE_THREE", self.RepublicPlayer)
    lowInfluenceCount = lowInfluenceCount + table.getn(influenceThreeList)

    local influenceFourList = Find_All_Objects_Of_Type("INFLUENCE_FOUR", self.RepublicPlayer)
    lowInfluenceCount = lowInfluenceCount + table.getn(influenceFourList)

    -- These automatically support player choices

    local influenceFiveList = Find_All_Objects_Of_Type("INFLUENCE_FIVE", self.RepublicPlayer)
    highInfluenceCount = highInfluenceCount + table.getn(influenceFiveList)

    local influenceSixList = Find_All_Objects_Of_Type("INFLUENCE_SIX", self.RepublicPlayer)
    highInfluenceCount = highInfluenceCount + table.getn(influenceSixList)

    local influenceSevenList = Find_All_Objects_Of_Type("INFLUENCE_SEVEN", self.RepublicPlayer)
    highInfluenceCount = highInfluenceCount + table.getn(influenceSevenList)

    local influenceEightList = Find_All_Objects_Of_Type("INFLUENCE_EIGHT", self.RepublicPlayer)
    highInfluenceCount = highInfluenceCount + table.getn(influenceEightList)

    local influenceNineList = Find_All_Objects_Of_Type("INFLUENCE_NINE", self.RepublicPlayer)
    highInfluenceCount = highInfluenceCount + table.getn(influenceNineList)

    local influenceTenList = Find_All_Objects_Of_Type("INFLUENCE_TEN", self.RepublicPlayer)
    highInfluenceCount = highInfluenceCount + table.getn(influenceTenList)

    local planetCount =
        table.getn(influenceOneList) + table.getn(influenceTwoList) + table.getn(influenceThreeList) +
        table.getn(influenceFourList) +
        table.getn(influenceFiveList) +
        table.getn(influenceSixList) +
        table.getn(influenceSevenList) +
        table.getn(influenceEightList) +
        table.getn(influenceNineList) +
        table.getn(influenceTenList)
    local approvalNumberCount =
        (10 * table.getn(influenceOneList)) + (20 * table.getn(influenceTwoList)) +
        (30 * table.getn(influenceThreeList)) +
        (40 * table.getn(influenceFourList)) +
        (50 * table.getn(influenceFiveList)) +
        (60 * table.getn(influenceSixList)) +
        (70 * table.getn(influenceSevenList)) +
        (80 * table.getn(influenceEightList)) +
        (90 * table.getn(influenceNineList)) +
        (100 * table.getn(influenceTenList))

    self.ApprovalRatingInfluence = (approvalNumberCount / planetCount)

    local oldApprovalRating = GlobalValue.Get("RepublicApprovalRating")
    local overallApprovalRating = oldApprovalRating - oldValue + self.ApprovalRatingInfluence

    if overallApprovalRating >= 80 and self.ChoiceMade == false then
        self:MakeLeaderChoice()
    end

    GlobalValue.Set("RepublicApprovalRating", overallApprovalRating)
end

function GovernmentRepublic:MakeLeaderChoice()
    self.ChoiceMade = true
    local plot = Get_Story_Plot("Conquests\\Story_Sandbox_Government_Rep.XML")
    if Find_Player("Empire").Is_Human() then
        Story_Event("LEADER_APPEAL")
    else
        Story_Event("LEADER_APPEAL_AI")
    end
end

function GovernmentRepublic:KDYContracts()
    local shipList = {
        "Generic_Star_Destroyer",
        "Generic_Tector",
        "Generic_Secutor"
    }

    local contractObject = Find_First_Object("DUMMY_KUAT_CONTRACT")
    if contractObject then
        local planet = contractObject.Get_Planet_Location()
        if planet.Get_Owner() == self.RepublicPlayer then
            local spawnChance = GameRandom(1, 100)
            if spawnChance <= 15 then
                table.remove(shipList, GameRandom(1, 3))
                table.remove(shipList, GameRandom(1, 2))
                local KDYspawn = SpawnList(shipList, planet, self.RepublicPlayer, true, false)
            end
        end
    end
end

function GovernmentRepublic:UpdateDisplay()
    local plot = Get_Story_Plot("Player_Agnostic_Plot.xml")
    local government_display_event = plot.Get_Event("Government_Display")

    government_display_event.Clear_Dialog_Text()

    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_REPUBLIC")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_CURRENT_APPROVAL", GlobalValue.Get("RepublicApprovalRating"))
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_CURRENT_CHANCELLOR", Find_Object_Type(GlobalValue.Get("ChiefOfState")))
    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_REPUBLIC_FUNCTION")
    government_display_event.Add_Dialog_Text("TEXT_TOOLTIP_NONE")  
    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_REPUBLIC_SECTORFORCES")
  
end


return GovernmentRepublic
