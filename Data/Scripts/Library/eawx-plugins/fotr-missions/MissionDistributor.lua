--**************************************************************************************************
--*    _______ __                                                                                  *
--*   |_     _|  |--.----.---.-.--.--.--.-----.-----.                                              *
--*     |   | |     |   _|  _  |  |  |  |     |__ --|                                              *
--*     |___| |__|__|__| |___._|________|__|__|_____|                                              *
--*    ______                                                                                      *
--*   |   __ \.-----.--.--.-----.-----.-----.-----.                                                *
--*   |      <|  -__|  |  |  -__|     |  _  |  -__|                                                *
--*   |___|__||_____|\___/|_____|__|__|___  |_____|                                                *
--*                                   |_____|                                                      *
--*                                                                                                *
--*                                                                                                *
--*       File:              MissionDistributor.lua                                                *
--*       File Created:      Sunday, 23rd February 2020 10:52                                      *
--*       Author:            Corey                                                                 *
--*       Last Modified:     Sunday, 23rd February 2020 10:58                                      *
--*       Modified By:       [TR] Pox                                                              *
--*       Copyright:         Thrawns Revenge Development Team                                      *
--*       License:           This code may not be used without the author's explicit permission    *
--**************************************************************************************************

require("eawx-std/class")
--require("eawx-plugins/fotr-missions/missions/Accumulate")
--require("eawx-plugins/fotr-missions/missions/CISShipyards")
--require("eawx-plugins/fotr-missions/missions/CISConquerPlanet")
--require("eawx-plugins/fotr-missions/missions/CISDestroyShips")
--require("eawx-plugins/fotr-missions/missions/RepShipyards")
--require("eawx-plugins/fotr-missions/missions/RepConquerPlanet")

---@class MissionDistributor
MissionDistributor = class()

function MissionDistributor:new()
    self.cis = Find_Player("Rebel")
    self.republic = Find_Player("Empire")

    -- Universal Missions
    --self.AccumulateMission = MissionAccumulate()

    -- CIS-Specific Missions
    --self.CISConquerPlanet = MissionCISConquerPlanet()
    --self.CISBuildShipyardsMission = MissionCISBuildShipyards()
    --self.CISDestroyShipsMission = MissionCISDestroyShips()

    -- Republic-Specific Missions
    --self.RepConquerPlanet = MissionRepConquerPlanet()
    --self.RepBuildShipyardsMission = MissionRepBuildShipyards()
end

function MissionDistributor:Assign()
    if self.republic.Is_Human() then
        
        local currentSupport = GlobalValue.Get("RepublicApprovalRating")
        GlobalValue.Set("RepublicApprovalRating", currentSupport + 1)
        --self.missionChoice = GameRandom(1, 3)

       --[[  if self.missionChoice == 1 then
            self.AccumulateMission:begin()
        elseif self.missionChoice == 2 then
            self.RepBuildShipyardsMission:begin()
        elseif self.missionChoice == 3 then
            self.RepConquerPlanet:begin()
        end ]]
    elseif self.cis.Is_Human() then
--[[         self.missionChoice = GameRandom(1, 4)

        if self.missionChoice == 1 then
            self.AccumulateMission:begin()
        elseif self.missionChoice == 2 then
            self.CISBuildShipyardsMission:begin()
        elseif self.missionChoice == 3 then
            self.CISConquerPlanet:begin()
        elseif self.missionChoice == 4 then
            self.CISDestroyShipsMission:begin()
        end ]]
    end
end

return MissionDistributor
