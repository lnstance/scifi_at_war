--**************************************************************************************************
--*    _______ __                                                                                  *
--*   |_     _|  |--.----.---.-.--.--.--.-----.-----.                                              *
--*     |   | |     |   _|  _  |  |  |  |     |__ --|                                              *
--*     |___| |__|__|__| |___._|________|__|__|_____|                                              *
--*    ______                                                                                      *
--*   |   __ \.-----.--.--.-----.-----.-----.-----.                                                *
--*   |      <|  -__|  |  |  -__|     |  _  |  -__|                                                *
--*   |___|__||_____|\___/|_____|__|__|___  |_____|                                                *
--*                                   |_____|                                                      *
--*                                                                                                *
--*                                                                                                *
--*       File:              MissionManager.lua                                                    *
--*       File Created:      Sunday, 23rd February 2020 10:56                                      *
--*       Author:            Corey                                                                 *
--*       Last Modified:     Sunday, 23rd February 2020 11:00                                      *
--*       Modified By:       [TR] Pox                                                              *
--*       Copyright:         Thrawns Revenge Development Team                                      *
--*       License:           This code may not be used without the author's explicit permission    *
--**************************************************************************************************

require("eawx-std/class")
require("eawx-util/StoryUtil")
require("eawx-plugins/fotr-missions/MissionDistributor")

---@class MissionManager
MissionManager = class()

function MissionManager:new()
    self.MissionDistributor = MissionDistributor()
    self.weeksSinceMission = 0
end

function MissionManager:update()
    self.weeksSinceMission = self.weeksSinceMission + 1

    if self.weeksSinceMission >= 3 then
        self.missionChance = GameRandom(1, 100)
        if self.missionChance <= 15 then
            self.MissionDistributor:Assign()
            self.weeksSinceMission = 0
        else
            if self.weeksSinceMission >= 5 then
                self.MissionDistributor:Assign()
                self.weeksSinceMission = 0
            end
        end
    end
end

return MissionManager
