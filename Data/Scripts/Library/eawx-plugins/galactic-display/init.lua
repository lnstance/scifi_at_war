--**************************************************************************************************
--*    _______ __                                                                                  *
--*   |_     _|  |--.----.---.-.--.--.--.-----.-----.                                              *
--*     |   | |     |   _|  _  |  |  |  |     |__ --|                                              *
--*     |___| |__|__|__| |___._|________|__|__|_____|                                              *
--*    ______                                                                                      *
--*   |   __ \.-----.--.--.-----.-----.-----.-----.                                                *
--*   |      <|  -__|  |  |  -__|     |  _  |  -__|                                                *
--*   |___|__||_____|\___/|_____|__|__|___  |_____|                                                *
--*                                   |_____|                                                      *
--*                                                                                                *
--*                                                                                                *
--*       File:              init.lua                                                              *
--*       File Created:      Sunday, 23rd February 2020 02:24                                      *
--*       Author:            [TR] Pox                                                              *
--*       Last Modified:     Wednesday, 8th July 2020 12:12                                        *
--*       Modified By:       [TR] Pox                                                              *
--*       Copyright:         Thrawns Revenge Development Team                                      *
--*       License:           This code may not be used without the author's explicit permission    *
--**************************************************************************************************

require("eawx-plugins/galactic-display/NewsFeedDisplayComponent")
require("eawx-plugins/galactic-display/PlanetInformationDisplayComponent")
require("eawx-plugins/galactic-display/ShipCrewDisplayComponent")
require("eawx-plugins/galactic-display/DisplayComponentContainer")

return {
    type = "plugin",
    target = "frame-update",
    dependencies = {"selected-planet-listener", "influence-service", "resource-manager"},
    init = function(self, ctx, selected_planet_changed_event, influence_service, resource_manager)

        local galactic_news_feed = NewsFeedDisplayComponent()

        local structure_display =
            PlanetInformationDisplayComponent(
            influence_service,
            selected_planet_changed_event,
            ctx.galactic_conquest.Events.GalacticProductionFinished
        )

        local galactic_display = DisplayComponentContainer()
        galactic_display:add_display_component("ship_crew_display", ShipCrewDisplayComponent(resource_manager), 1)
        galactic_display:add_display_component("planet_info", structure_display, 1)
        galactic_display:add_display_component("news_feed", galactic_news_feed, 1)

        return galactic_display
    end
}
