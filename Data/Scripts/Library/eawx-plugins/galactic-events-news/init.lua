require("eawx-plugins/galactic-events-news/GalacticEventsNewsSource")

return {
    type = "plugin",
    target = "passive",
    dependencies = { "galactic-display",  "invading-fleet-listener", "blockade-attrition" },
    ---@param ctx table<string, any>
    ---@param galactic_display DisplayComponentContainer
    ---@param incoming_fleet_event IncomingFleetEvent
    init = function(self, ctx, galactic_display, incoming_fleet_event, blockade_attrition)
        ---@type GalacticConquest
        local galactic_conquest = ctx.galactic_conquest

        local gc_event_news_source =
            GalacticEventsNewsSource(
            ctx.galactic_conquest.Events.PlanetOwnerChanged,
            ctx.galactic_conquest.Events.GalacticHeroKilled,
            incoming_fleet_event,
            blockade_attrition.blockade_attrition_unit_killed
        )

        ---@type NewsFeedDisplayComponent
        local news_feed = galactic_display:get_component("news_feed")
        news_feed:add_news_source(gc_event_news_source)

        return gc_event_news_source
    end
}