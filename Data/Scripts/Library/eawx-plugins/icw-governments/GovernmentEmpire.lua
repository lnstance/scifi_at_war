require("eawx-std/class")
require("eawx-crossplot/crossplot")
require("eawx-util/GalacticUtil")
require("PGSpawnUnits")
require("TRCommands")

---@class GovernmentEmpire
GovernmentEmpire = class()

function GovernmentEmpire:new()
    self.PlayerEmpire = Find_Player("Empire")
    self.PlayerPentastar = Find_Player("Pentastar")
    self.PlayerMaldrood = Find_Player("Teradoc")
    self.PlayerZsinj = Find_Player("Pirates")
    self.PlayerEriadu = Find_Player("Hutts")

    self.imperialTable = {
        ["Empire"] = 50,
        ["Pentastar"] = 50,
        ["Teradoc"] = 50,
        ["Pirates"] = 50,
        ["Hutts"] = 50
    }

    self.faction_count = self:get_alive_faction_count()

    self.hero_list_high = {}

    self.hero_list_low = {}
end

function GovernmentEmpire:get_alive_faction_count()
    local faction_count = 0
    for faction_name, _ in pairs(self.imperialTable) do
        local faction = Find_Player(faction_name)
        if FactionOwnsPlanets(faction) then
            faction_count = faction_count + 1
        end
    end

    return faction_count
end

function GovernmentEmpire:Update()
    self.faction_count = self:get_alive_faction_count()

    local factionModifier = 10 * (self.faction_count - 1)
    for faction_name, _ in pairs(self.imperialTable) do
        self.imperialTable[faction_name] = 50 - factionModifier
        local faction = Find_Player(faction_name)
        
        local coruscant = FindPlanet("Coruscant")
        if TestValid(coruscant) and coruscant.Get_Owner() == faction then
            self.imperialTable[faction_name] = self.imperialTable[faction_name] + 10
        end

        self:SpawnSupporter(faction, self.imperialTable[faction_name])
    end
    
    if self.PlayerEmpire.Is_Human() then
        self:UpdateDisplay()
    end

    crossplot:publish("empire-government-updated", self.imperialTable)
end

function GovernmentEmpire:SpawnSupporter(faction, legitimacy)
    if legitimacy >= 60 then
        local spawnChance = GameRandom(1, 100)
        if spawnChance <= 15 then
            self:SpawnHigh(faction)
        end
    elseif legitimacy >= 20 then
        local spawnChance = GameRandom(1, 100)
        if spawnChance <= 15 then
            self:SpawnLow(faction)
        end
    end
end

function GovernmentEmpire:SpawnHigh(faction)
    local available_high_heroes = table.getn(self.hero_list_high)
    if available_high_heroes == 0 then
        self:SpawnLow(faction)
    else
        local heroNumber = GameRandom(1, table.getn(self.hero_list_high))
        local heroObject = self.hero_list_high[heroNumber]
        table.remove(self.hero_list_high, heroNumber)

        local planet = GalacticUtil.find_friendly_planet(faction)

        local spawnList = {heroObject}
        local legitimacyReward = SpawnList(spawnList, planet, faction, true, false)
    end
end

function GovernmentEmpire:SpawnLow(faction)
    if self.hero_list_low ~= nil then
        local heroNumber = GameRandom(1, table.getn(self.hero_list_low))
        local heroObject = self.hero_list_low[heroNumber]
        table.remove(self.hero_list_low, heroNumber)

        local planet = GalacticUtil.find_friendly_planet(faction)

        local spawnList = {heroObject}
        local legitimacyReward = SpawnList(spawnList, planet, faction, true, false)
    end
end

function GovernmentEmpire:UpdateDisplay()
    local plot = Get_Story_Plot("Player_Agnostic_Plot.xml")
    local government_display_event = plot.Get_Event("Government_Display")

    government_display_event.Clear_Dialog_Text()

    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_EMPIRE")
    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_EMPIRE_DESCRIPTION")

    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_EMPIRE_LEGITIMACY_HEADER")
    for faction_name, legitimacy_value in pairs(self.imperialTable) do
        government_display_event.Add_Dialog_Text(
            "TEXT_GOVERNMENT_LEGITIMACY",
            CONSTANTS.ALL_FACTION_TEXTS[string.upper(faction_name)],
            legitimacy_value
        )
    end

    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_EMPIRE_LEGITIMACY_BASE")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_EMPIRE_LEGITIMACY_MOD_RIVAL")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_EMPIRE_LEGITIMACY_MOD_CORUSCANT")
end

return GovernmentEmpire
