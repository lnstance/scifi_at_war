require("eawx-std/class")
require("eawx-plugins/icw-governments/GovernmentNewRepublic")
--require("eawx-plugins/icw-governments/GovernmentEmpire")

---@class GovernmentManager
GovernmentManager = class()

---@param dummy_lifecycle_handler KeyDummyLifeCycleHandler
function GovernmentManager:new(dummy_lifecycle_handler)
    self.NRGOV = GovernmentNewRepublic(dummy_lifecycle_handler)
    --self.EMPIREGOV = GovernmentEmpire()
end

function GovernmentManager:update()
    self.NRGOV:Update()
    --self.EMPIREGOV:Update()
end

return GovernmentManager
