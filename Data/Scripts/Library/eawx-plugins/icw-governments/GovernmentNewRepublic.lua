require("eawx-std/class")
require("eawx-crossplot/crossplot")
require("SetFighterResearch")

---@class GovernmentNewRepublic
GovernmentNewRepublic = class()

---@param dummy_lifecycle_handler KeyDummyLifeCycleHandler
function GovernmentNewRepublic:new(dummy_lifecycle_handler)
    self.RepublicPlayer = Find_Player("Rebel")
    self.votesFor = 0
    self.votesAgainst = 0
    self.electionResult = 0
    self.planets = {}

    self.dummy_lifecycle_handler = dummy_lifecycle_handler

    GlobalValue.Set("ChiefOfState", "DUMMY_CHIEFOFSTATE_MOTHMA")
    GlobalValue.Set("ChiefOfStatePreference", "DUMMY_CHIEFOFSTATE_MOTHMA")

    self.chief_of_state = "DUMMY_CHIEFOFSTATE_MOTHMA"

    self.dummy_lifecycle_handler:add_to_dummy_set(
        self.RepublicPlayer,
        {
            [self.chief_of_state] = 1
        }
    )

    -- starting at -1 so the beginning of the first week doesn't count as a completed week
    self.LastElectionTime = -1

    self.Events = {}
    self.Events.ElectionHeld = Observable()
end

function GovernmentNewRepublic:Update()
    DebugMessage("GovernmentNewRepublic Update Started")
    self.LastElectionTime = self.LastElectionTime + 1
    if self.LastElectionTime >= 10 then
        self:ElectionHeld()
        self.LastElectionTime = 0
    end

    if self.RepublicPlayer.Is_Human() then
        self:UpdateDisplay()
    end
    DebugMessage("GovernmentNewRepublic Update Finished")
end

function GovernmentNewRepublic:ElectionHeld()
    DebugMessage("GovernmentNewRepublic ElectionHeld Started")
    -- These don't automatically follow your choice

    self.votesFor = EvaluatePerception("ElectionResultInfluenceFor", self.RepublicPlayer)
    self.votesAgainst = EvaluatePerception("ElectionResultInfluenceAgainst", self.RepublicPlayer)

    self.electionResult = self.votesFor - self.votesAgainst

    self.dummy_lifecycle_handler:remove_from_dummy_set(
        self.RepublicPlayer,
        {
            [self.chief_of_state] = 1
        }
    )

    if self.electionResult > 0 then
        self.chief_of_state = GlobalValue.Get("ChiefOfStatePreference")
    else
        self.chief_of_state = "DUMMY_CHIEFOFSTATE_MOTHMA"
    end

    self.dummy_lifecycle_handler:add_to_dummy_set(
        self.RepublicPlayer,
        {
            [self.chief_of_state] = 1
        }
    )
	
	if self.chief_of_state ~= GlobalValue.Get("ChiefOfState") then
		Clear_Fighter_Research("CoS_Tevv")
		if self.chief_of_state == "DUMMY_CHIEFOFSTATE_TEVV" then
			Set_Fighter_Research("CoS_Tevv")
		end
	end

    GlobalValue.Set("ChiefOfState", self.chief_of_state)
    self.Events.ElectionHeld:Notify {
        winner = self.chief_of_state,
        preferred_candidate = self.electionResult > 0
    }

    DebugMessage("GovernmentNewRepublic ElectionHeld Finished")
end

function GovernmentNewRepublic:UpdateDisplay()
    local plot = Get_Story_Plot("Player_Agnostic_Plot.xml")
    local government_display_event = plot.Get_Event("Government_Display")

    government_display_event.Clear_Dialog_Text()

    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC")
    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_OVERVIEW")
	government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text(
        "TEXT_GOVERNMENT_CURRENT_COS",
        Find_Object_Type(GlobalValue.Get("ChiefOfState"))
    )
    government_display_event.Add_Dialog_Text(
        "TEXT_GOVERNMENT_SUPPORTED_COS",
        Find_Object_Type(GlobalValue.Get("ChiefOfStatePreference"))
    )
    government_display_event.Add_Dialog_Text(
        "TEXT_GOVERNMENT_LAST_ELECTION",
        self.votesFor, self.votesAgainst
    )
	government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_ADMIRAL_LIST")
	government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
	local admiral_list = GlobalValue.Get("NR_ADMIRAL_LIST")
	if admiral_list ~= nil then
		for index, obj in pairs(admiral_list) do
			government_display_event.Add_Dialog_Text(obj)
		end
	end
	
    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_SYSTEM")
	government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_CANDIDATES")
	government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    -- Always options
    government_display_event.Add_Dialog_Text("TEXT_HERO_LEIA")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_REQUIREMENTS_NONE")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_LEIA_EFFECT")
    government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_HERO_MON_MOTHMA")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_REQUIREMENTS_NONE")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_MOTHMA_EFFECT")
	government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_HERO_BORSK")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_REQUIREMENTS_NONE")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_FEYLYA_EFFECT")
	government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
    government_display_event.Add_Dialog_Text("TEXT_HERO_GAVRISOM")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_REQUIREMENTS_NONE")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_GAVRISOM_EFFECT")
	government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
	government_display_event.Add_Dialog_Text("TEXT_HERO_TEVV")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_REQUIREMENTS_NONE")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_TEVV_EFFECT")
	government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
	government_display_event.Add_Dialog_Text("TEXT_HERO_SOBILLES")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_REQUIREMENTS_NONE")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_SOBILLES_EFFECT")
	government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")
	government_display_event.Add_Dialog_Text("TEXT_HERO_NAVIK")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_REQUIREMENTS_NONE")
    government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NAVIK_EFFECT")
    -- Not always options
    --government_display_event.Add_Dialog_Text("TEXT_HERO_SHESH")
    --government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_REQUIREMENTS_KUAT")
    --government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_SHESH_EFFECT")

	government_display_event.Add_Dialog_Text("TEXT_DOCUMENTATION_BODY_SEPARATOR")

	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_LIST")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_ACKBAR")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_NANTZ")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_SOVV")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_HAN")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_HAN2")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_GARM")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_DRAYSON")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_TALLON")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_BURKE")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_MASSA")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_DORAT")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_RAGAB")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_SNUNB")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_KALBACK")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_VANTAI")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_GRANT")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_BELL")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_BRAND")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_ABAHT")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_KREFEY")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_NAMMO")
	government_display_event.Add_Dialog_Text("TEXT_GOVERNMENT_NEW_REPUBLIC_ADMIRAL_ACKDOOL")
end


return GovernmentNewRepublic
