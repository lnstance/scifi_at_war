--**************************************************************************************************
--*    _______ __                                                                                  *
--*   |_     _|  |--.----.---.-.--.--.--.-----.-----.                                              *
--*     |   | |     |   _|  _  |  |  |  |     |__ --|                                              *
--*     |___| |__|__|__| |___._|________|__|__|_____|                                              *
--*    ______                                                                                      *
--*   |   __ \.-----.--.--.-----.-----.-----.-----.                                                *
--*   |      <|  -__|  |  |  -__|     |  _  |  -__|                                                *
--*   |___|__||_____|\___/|_____|__|__|___  |_____|                                                *
--*                                   |_____|                                                      *
--*                                                                                                *
--*                                                                                                *
--*       File:              InfluenceService.lua                                                  *
--*       File Created:      Sunday, 23rd February 2020 08:26                                      *
--*       Author:            [TR] Pox                                                              *
--*       Last Modified:     Sunday, 23rd February 2020 10:22                                      *
--*       Modified By:       [TR] Pox                                                              *
--*       Copyright:         Thrawns Revenge Development Team                                      *
--*       License:           This code may not be used without the author's explicit permission    *
--**************************************************************************************************
require("PGSpawnUnits")

require("eawx-std/class")
ModContentLoader.get("influence-policy/InfluencePolicyRepository")

---@class InfluenceService
InfluenceService = class()

---@param human_player FactionObject
---@param planets table<string, Planet>
---@param dummy_lifecycle_handler KeyDummyLifeCycleHandler
---@param abstract_resources AbstractResources
---@param revolt_manager RevoltManager
function InfluenceService:new(human_player, planets, dummy_lifecycle_handler, abstract_resources, revolt_manager)
    self.human_player = human_player
    self.planets = {}
    self.dummy_lifecycle_handler = dummy_lifecycle_handler
    self.abstract_resources = abstract_resources
    self.revolt_manager = revolt_manager
    self.influence_modifier = 0
    
    for _, planet in pairs(planets) do
        self.planets[planet] = {
            __current_influence_dummies = {},
            weeksControlled = 0,
            ownerInfluence = 0,
            oldInfluence = 0,
            unrestLevel = 0,
            blockadeModifier = 0,
            oldOwner = nil,
            newOwner = nil
        }
    end
end

---@param planet Planet
function InfluenceService:get_influence_for_planet(planet)
    return self.planets[planet].ownerInfluence
end

function InfluenceService:get_unrest_for_planet(planet)
    return self.planets[planet].unrestLevel
end

---@param planet Planet
function InfluenceService:update(planet)
    local planet_stats = self.planets[planet]

    if planet:get_owner() == Find_Player("Neutral") then
        return
    end

    planet_stats.ownerInfluence = 3
    planet_stats.newOwner = planet:get_owner()

    if planet_stats.newOwner == planet_stats.oldOwner then
        planet_stats.weeksControlled = planet_stats.weeksControlled + 1
        if planet_stats.unrestLevel == 3 then
            self.revolt_manager:planetary_revolt(planet)
        end
    else
        planet_stats.weeksControlled = 0
        planet_stats.unrestLevel = 0
    end

    if planet_stats.weeksControlled > 2 then
        self.influence_modifier = EvaluatePerception("Influence_Modifier", self.human_player, planet:get_game_object())
    else
        self.influence_modifier = EvaluatePerception("Influence_Modifier_NewCapture", self.human_player, planet:get_game_object())
    end

    if EvaluatePerception("Blockaded", planet:get_owner(), planet:get_game_object()) > 0 then
        if GameRandom(1, 100) < 20 then
            if EvaluatePerception("Ground_Base_Level", self.human_player, planet:get_game_object()) < 4 then
                planet_stats.blockadeModifier = planet_stats.blockadeModifier + 1
            end
        end
    else
        planet_stats.blockadeModifier = 0
    end

    --self:apply_week_controlled_influence(planet_stats)

    planet_stats.ownerInfluence = planet_stats.ownerInfluence + self.influence_modifier - planet_stats.blockadeModifier

    local planetary_influence_policy =
        InfluencePolicyRepository[string.lower(planet:get_name())] or InfluencePolicyRepository.DEFAULT

    planet_stats.ownerInfluence = planet_stats.ownerInfluence + planetary_influence_policy(planet) 

    if planet:get_owner() == Find_Player("local")  then
        planet_stats.ownerInfluence = planet_stats.ownerInfluence + self.abstract_resources:DetermineResourceInfluenceModifier() 
        if planet:get_owner().Get_Difficulty() == "Easy" then
            planet_stats.ownerInfluence = planet_stats.ownerInfluence + 2
        elseif planet:get_owner().Get_Difficulty() == "Hard" then
            planet_stats.ownerInfluence = planet_stats.ownerInfluence
        else
            planet_stats.ownerInfluence = planet_stats.ownerInfluence + 1
        end
    else 
        if planet:get_owner().Get_Difficulty() == "Easy" then
            planet_stats.ownerInfluence = planet_stats.ownerInfluence + 1
        elseif planet:get_owner().Get_Difficulty() == "Hard" then
            planet_stats.ownerInfluence = planet_stats.ownerInfluence + 3
        else
            planet_stats.ownerInfluence = planet_stats.ownerInfluence + 2
        end
    end

    if planet:get_owner() == Find_Player("Warlords")  then
        planet_stats.ownerInfluence = 10
    end
    
    if planet_stats.ownerInfluence <= 1 then
        planet_stats.unrestLevel = planet_stats.unrestLevel + 1
    elseif planet_stats.ownerInfluence == 2 then
        if GameRandom(1, 100) > 50 then
            planet_stats.unrestLevel = planet_stats.unrestLevel + 1
        end
    elseif planet_stats.ownerInfluence == 3 then
        if GameRandom(1, 100) > 66 then
            planet_stats.unrestLevel = planet_stats.unrestLevel + 1
        end
    elseif planet_stats.ownerInfluence >= 6 then
        planet_stats.unrestLevel = planet_stats.unrestLevel - 1
    end

    self:normalize_influence(planet_stats)
   
    planet_stats.oldOwner = planet:get_owner()

    self:attach_particle(planet, planet_stats)
    self:apply_loyalty_modifiers(planet)

    planet_stats.oldInfluence = planet_stats.ownerInfluence
end

---@private
---@param planet_stats table<string, any>
function InfluenceService:apply_week_controlled_influence(planet_stats)
    if planet_stats.newOwner == planet_stats.oldOwner then
        planet_stats.weeksControlled = planet_stats.weeksControlled + 1
    else
        planet_stats.weeksControlled = 0
    end

    for i = 10, 50, 20 do
        if planet_stats.weeksControlled < i then
            break
        end

        planet_stats.ownerInfluence = planet_stats.ownerInfluence + 1
    end
end

function InfluenceService:attach_particle(planet, planet_stats)
    if planet:get_owner() ~= Find_Player("Neutral") then
        planet:get_game_object().Attach_Particle_Effect("Display_Influence_" .. tostring(planet_stats.ownerInfluence))
        if planet_stats.unrestLevel <= 3 and planet_stats.unrestLevel >= 1 then
            planet:get_game_object().Attach_Particle_Effect("Display_Unrest_" .. tostring(planet_stats.unrestLevel))
        end
    end
end

---@private
---@param planet Planet
function InfluenceService:apply_loyalty_modifiers(planet)
    local planet_stats = self.planets[planet]
    if planet_stats.oldInfluence == planet_stats.ownerInfluence then
        return
    end

    local influence_structure_table = {
        {INFLUENCE_ONE = 1},
        {INFLUENCE_TWO = 1},
        {INFLUENCE_THREE = 1},
        {INFLUENCE_FOUR = 1},
        {INFLUENCE_FIVE = 1},
        {INFLUENCE_SIX = 1},
        {INFLUENCE_SEVEN = 1},
        {INFLUENCE_EIGHT = 1},
        {INFLUENCE_NINE = 1},
        {INFLUENCE_TEN = 1}
    }

    self.dummy_lifecycle_handler:remove_from_dummy_set(planet, planet_stats.__current_influence_dummies)
    
    planet_stats.__current_influence_dummies = influence_structure_table[planet_stats.ownerInfluence]
    self.dummy_lifecycle_handler:add_to_dummy_set(planet, planet_stats.__current_influence_dummies)
end

---@private
---@param planet_stats table<string, any>
function InfluenceService:normalize_influence(planet_stats)
    if planet_stats.ownerInfluence > 10 then
        planet_stats.ownerInfluence = 10
    end

    if planet_stats.ownerInfluence <= 0 then
        planet_stats.ownerInfluence = 1
    end

    if planet_stats.unrestLevel > 3 then
        planet_stats.unrestLevel = 3
    end

    if planet_stats.unrestLevel <= 0 then
        planet_stats.unrestLevel = 0
    end
end
