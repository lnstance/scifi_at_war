require("eawx-plugins/key-dummy-lifecycle-handler/KeyDummyLifeCycleHandler")

return {
    type = "plugin",
    target = "frame-planet-update",
    init = function(self, ctx)
        return KeyDummyLifeCycleHandler(ctx.galactic_conquest.Planets, ctx.galactic_conquest.Events.PlanetOwnerChanged)
    end
}
