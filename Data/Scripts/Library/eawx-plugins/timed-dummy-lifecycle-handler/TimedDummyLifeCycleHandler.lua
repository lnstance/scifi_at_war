--**************************************************************************************************
--*    _______ __                                                                                  *
--*   |_     _|  |--.----.---.-.--.--.--.-----.-----.                                              *
--*     |   | |     |   _|  _  |  |  |  |     |__ --|                                              *
--*     |___| |__|__|__| |___._|________|__|__|_____|                                              *
--*    ______                                                                                      *
--*   |   __ \.-----.--.--.-----.-----.-----.-----.                                                *
--*   |      <|  -__|  |  |  -__|     |  _  |  -__|                                                *
--*   |___|__||_____|\___/|_____|__|__|___  |_____|                                                *
--*                                   |_____|                                                      *
--*                                                                                                *
--*                                                                                                *
--*       File:              AiDummyLifeCycleHandler.lua                                           *
--*       File Created:      Sunday, 23rd February 2020 05:09                                      *
--*       Author:            [TR] Pox                                                              *
--*       Last Modified:     Monday, 24th February 2020 12:45                                      *
--*       Modified By:       [TR] Pox                                                              *
--*       Copyright:         Thrawns Revenge Development Team                                      *
--*       License:           This code may not be used without the author's explicit permission    *
--**************************************************************************************************

require("eawx-std/class")
require("eawx-util/GalacticUtil")

---@class TimedDummyLifeCycleHandler
TimedDummyLifeCycleHandler = class()

---@param planets table<string, Planet>
---@param planet_owner_changed_event PlanetOwnerChangedEvent
---@param interval number
function TimedDummyLifeCycleHandler:new(planets, planet_owner_changed_event, interval)
    planet_owner_changed_event:AttachListener(self.__on_planet_owner_changed, self)

    ---@private
    self.interval = interval or 10

    ---@private
    self.__ai_planet_dummy_set = {}

    ---@private
    ---@type table<Planet, table<string, number>>
    self.__planet_specific_dummy_sets = {}

    ---@private
    ---@type table<FactionObject, table<string, number>>
    self.__faction_specific_dummy_sets = {}

    ---@private
    ---@type table<Planet, GameObjectWrapper[]>
    self.__ai_planet_dummy_objects = {}

    ---@private
    ---@type table<Planet, GameObjectWrapper[]>
    self.__planet_specific_dummy_objects = {}

    ---@private
    ---@type table<Planet, GameObjectWrapper[]>
    self.__faction_specific_dummy_objects = {}

    ---@private
    self.__ai_dummies_need_update_next_cycle = false

    ---@private
    ---@type table<FactionObject, boolean>
    self.__faction_dummies_need_update_next_cycle = {}

    ---@private
    self.__should_force_respawn_ai_dummies = false

    ---@private
    ---@type table<Planet, boolean>
    self.__should_force_respawn_planet_dummies = {}

    ---@private
    ---@type table<FactionObject, boolean>
    self.__should_force_respawn_faction_dummies = {}

    ---@private
    self.__all_planets = planets

    ---@private
    self.__number_of_planets = 0

    ---@private
    self.__planet_cycle_index = 0

    for _, planet in pairs(self.__all_planets) do
        self.__ai_planet_dummy_objects[planet] = {}
        self.__planet_specific_dummy_objects[planet] = {}
        self.__faction_specific_dummy_objects[planet] = {}

        self.__number_of_planets = self.__number_of_planets + 1
    end

    ---@private
    ---@type number
    self.__last_update = nil
end

---@param dummy_set table<string, number>
function TimedDummyLifeCycleHandler:add_to_ai_dummy_set(dummy_set)
    self:add_to_dummy_set(self.__ai_planet_dummy_set, dummy_set)
    self.__ai_dummies_need_update_next_cycle = true
end

---@param dummy_set table<string, number>
function TimedDummyLifeCycleHandler:remove_from_ai_dummy_set(dummy_set)
    self:remove_from_dummy_set(self.__ai_planet_dummy_set, dummy_set)
    self.__ai_dummies_need_update_next_cycle = true
end

---@param planet Planet
---@param dummy_set table<string, number>
function TimedDummyLifeCycleHandler:add_to_planet_specific_dummy_set(planet, dummy_set)
    if not self.__planet_specific_dummy_sets[planet] then
        self.__planet_specific_dummy_sets[planet] = {}
    end

    local planet_dummy_set = self.__planet_specific_dummy_sets[planet]
    self:add_to_dummy_set(planet_dummy_set, dummy_set)

    self.__should_force_respawn_planet_dummies[planet] = true
end

---@param planet Planet
---@param dummy_set table<string, number>
function TimedDummyLifeCycleHandler:remove_from_planet_specific_dummy_set(planet, dummy_set)
    local planet_dummy_set = self.__planet_specific_dummy_sets[planet]

    if not planet_dummy_set then
        return
    end

    self:remove_from_dummy_set(planet_dummy_set, dummy_set)
    self.__should_force_respawn_planet_dummies[planet] = true
end

---@param faction FactionObject
---@param dummy_set table<string, number>
function TimedDummyLifeCycleHandler:add_to_faction_specific_dummy_set(faction, dummy_set)
    if not self.__faction_specific_dummy_sets[faction] then
        self.__faction_specific_dummy_sets[faction] = {}
    end

    local faction_dummy_set = self.__faction_specific_dummy_sets[faction]
    self:add_to_dummy_set(faction_dummy_set, dummy_set)

    self.__faction_dummies_need_update_next_cycle[faction] = true
end

---@param faction FactionObject
---@param dummy_set table<string, number>
function TimedDummyLifeCycleHandler:remove_from_faction_specific_dummy_set(faction, dummy_set)
    local faction_dummy_set = self.__faction_specific_dummy_sets[faction]

    if not faction_dummy_set then
        return
    end

    self:remove_from_dummy_set(faction_dummy_set, dummy_set)
    self.__faction_dummies_need_update_next_cycle[faction] = true
end

---@private
---@param dummy_set table<string, number>
---@param new_dummy_entries table<string, number>
function TimedDummyLifeCycleHandler:add_to_dummy_set(dummy_set, new_dummy_entries)
    for dummy_name, amount in pairs(new_dummy_entries) do
        if not dummy_set[dummy_name] then
            dummy_set[dummy_name] = 0
        end

        dummy_set[dummy_name] = dummy_set[dummy_name] + amount
    end
end

---@private
---@param dummy_set table<string, number>
---@param dummy_entries_to_remove table<string, number>
function TimedDummyLifeCycleHandler:remove_from_dummy_set(dummy_set, dummy_entries_to_remove)
    for dummy_name, amount in pairs(dummy_entries_to_remove) do
        if dummy_set[dummy_name] then
            dummy_set[dummy_name] = dummy_set[dummy_name] - amount

            if dummy_set[dummy_name] <= 0 then
                dummy_set[dummy_name] = nil
            end
        end
    end
end

---@param planet Planet
function TimedDummyLifeCycleHandler:update(planet)
    if not self:__should_update() then
        return
    end

    if self.__planet_cycle_index == 0 then
        self:pre_cycle_settings()
    end

    self.__planet_cycle_index = self.__planet_cycle_index + 1

    if not planet:get_owner().Is_Human() then
        self:handle_respawn(
            planet,
            self.__ai_planet_dummy_objects,
            self.__ai_planet_dummy_set,
            self.__should_force_respawn_ai_dummies
        )
    end

    if self.__planet_specific_dummy_sets[planet] then
        DebugMessage(
            "TimedDummyLifeCycleHandler -- In planet specific dummy set spawn on planet %s",
            tostring(planet:get_name())
        )

        self:handle_respawn(
            planet,
            self.__planet_specific_dummy_objects,
            self.__planet_specific_dummy_sets[planet],
            self.__should_force_respawn_planet_dummies[planet]
        )

        self.__should_force_respawn_planet_dummies[planet] = false
    end

    if self.__faction_specific_dummy_sets[planet:get_owner()] then
        DebugMessage(
            "TimedDummyLifeCycleHandler -- In faction specific dummy set spawn for %s on planet %s",
            tostring(planet:get_owner().Get_Faction_Name()),
            tostring(planet:get_name())
        )

        self:handle_respawn(
            planet,
            self.__faction_specific_dummy_objects,
            self.__faction_specific_dummy_sets[planet:get_owner()],
            self.__should_force_respawn_faction_dummies[planet:get_owner()]
        )
    end

    if self.__planet_cycle_index == self.__number_of_planets then
        self:post_cycle_settings()
    end
end

---@private
function TimedDummyLifeCycleHandler:__should_update()
    return not self.__last_update or GetCurrentTime() - self.__last_update > self.interval
end

---@private
function TimedDummyLifeCycleHandler:pre_cycle_settings()
    if self.__ai_dummies_need_update_next_cycle then
        self.__should_force_respawn_ai_dummies = true
        self.__ai_dummies_need_update_next_cycle = false
    end

    for faction, need_update in pairs(self.__faction_dummies_need_update_next_cycle) do
        if need_update then
            DebugMessage(
                "TimedDummyLifeCycleHandler -- Faction specific dummies for %s need to be updated next cycle.",
                tostring(faction.Get_Faction_Name())
            )
            self.__should_force_respawn_faction_dummies[faction] = true
            self.__faction_dummies_need_update_next_cycle[faction] = false
        end
    end
end

---@private
function TimedDummyLifeCycleHandler:handle_respawn(planet, dummy_object_table, dummy_set, force_respawn)
    if force_respawn then
        self:force_respawn_dummies(dummy_object_table, dummy_set, planet)
    else
        self:__spawn_dummies_when_missing(dummy_object_table, dummy_set, planet)
    end
end

---@private
function TimedDummyLifeCycleHandler:post_cycle_settings()
    self.__last_update = GetCurrentTime()
    self.__planet_cycle_index = 0
    self.__should_force_respawn_ai_dummies = false

    for faction, force_respawn in pairs(self.__should_force_respawn_faction_dummies) do
        self.__should_force_respawn_faction_dummies[faction] = false
    end
end

---@private
---@param planet Planet
function TimedDummyLifeCycleHandler:__on_planet_owner_changed(planet)
    if not planet then
        return
    end

    DebugMessage(
        "TimedDummyLifeCycleHandler::on_planet_owner_changed -- pre force respawn for planet %s",
        tostring(planet:get_name())
    )

    self:__clear_dummies(self.__ai_planet_dummy_objects, planet)
    self:__clear_dummies(self.__planet_specific_dummy_objects, planet)
    self:__clear_dummies(self.__faction_specific_dummy_objects, planet)

    if not planet:get_owner().Is_Human() then
        self:__spawn_dummies_on_planet(planet, self.__ai_planet_dummy_set)
    end

    if self.__planet_specific_dummy_sets[planet] then
        self:__spawn_dummies_on_planet(planet, self.__planet_specific_dummy_sets[planet])
    end

    if self.__faction_specific_dummy_sets[planet:get_owner()] then
        self:__spawn_dummies_on_planet(planet, self.__faction_specific_dummy_sets[planet:get_owner()])
    end
end

---@private
---@param dummy_object_table table<Planet, GameObjectWrapper[]>
---@param dummy_set table<string, number>
---@param planet Planet
function TimedDummyLifeCycleHandler:force_respawn_dummies(dummy_object_table, dummy_set, planet)
    if not (dummy_object_table and dummy_set) then
        DebugMessage(
            "TimedDummyLifeCycleHandler::force_respawn_dummies -- Got nil argument. dummy_object_table: %s ; dummy_set: %s",
            tostring(dummy_object_table),
            tostring(dummy_set)
        )
        return
    end

    DebugMessage(
        "TimedDummyLifeCycleHandler::force_respawn_dummies -- force respawning dummies on planet %s",
        tostring(planet:get_name())
    )
    self:__clear_dummies(dummy_object_table, planet)
    dummy_object_table[planet] = self:__spawn_dummies_on_planet(planet, dummy_set)
end

---@private
---@param dummy_object_table table<Planet, GameObjectWrapper[]>
---@param planet Planet
function TimedDummyLifeCycleHandler:__clear_dummies(dummy_object_table, planet)
    for _, dummy in pairs(dummy_object_table[planet]) do
        if TestValid(dummy) then
            dummy.Despawn()
        end
    end

    dummy_object_table[planet] = {}
end

---@private
---@param dummy_object_table table<Planet, GameObjectWrapper[]>
---@param dummy_set table<string, number>
---@param planet Planet
function TimedDummyLifeCycleHandler:__spawn_dummies_when_missing(dummy_object_table, dummy_set, planet)
    if not (dummy_object_table and dummy_set) then
        return
    end

    local dummies_on_planet = dummy_object_table[planet]
    self:__remove_invalid_entries(dummies_on_planet)

    if table.getn(dummies_on_planet) > 0 then
        return
    end

    DebugMessage("TimedDummyLifeCycleHandler -- respawning missing dummies on planet %s", tostring(planet:get_name()))
    dummy_object_table[planet] = self:__spawn_dummies_on_planet(planet, dummy_set)
end

---@private
function TimedDummyLifeCycleHandler:__remove_invalid_entries(tab)
    for i, object in pairs(tab) do
        if not TestValid(object) then
            table.remove(tab, i)
        end
    end
end

---@private
---@param planet Planet
function TimedDummyLifeCycleHandler:__spawn_dummies_on_planet(planet, dummy_set)
    local units =
        GalacticUtil.spawn {
        location = planet:get_name(),
        objects = dummy_set,
        owner = planet:get_owner().Get_Faction_Name()
    }

    return units
end

return TimedDummyLifeCycleHandler
