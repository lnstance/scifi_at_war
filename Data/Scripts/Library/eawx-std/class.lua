---@alias Class table<string, any>

---Creates a new class
---@param extends string @The name of the parent class
---@return Class
function class(extends)
    local mt = {
        __call = function(class, ...)
            local obj = setmetatable({}, {__index = class})

            if class.__extends and class.__extends.new then
                class.__extends.new(obj, unpack(arg))
            end

            if class.new then
                obj:new(unpack(arg))
            end

            return obj
        end
    }

    if extends then
        mt.__index = extends
    end

    return setmetatable(
        {
            __important = true,
            __extends = extends
        },
        mt
    )
end

---Returns whether the given object is an instance of the specified class
---@param object any
---@param class Class
function is_type(object, class)
    local metatable = getmetatable(object)
    if not metatable then
        return false
    end

    local index_table = metatable.__index
    if not index_table then
        return false
    end

    if class == index_table then
        return true
    end

    if index_table.__extends then
        return is_type(object, index_table.__extends)
    end

    return false
end
