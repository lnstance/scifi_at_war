--******************************************************************************
--     _______ __
--    |_     _|  |--.----.---.-.--.--.--.-----.-----.
--      |   | |     |   _|  _  |  |  |  |     |__ --|
--      |___| |__|__|__| |___._|________|__|__|_____|
--     ______
--    |   __ \.-----.--.--.-----.-----.-----.-----.
--    |      <|  -__|  |  |  -__|     |  _  |  -__|
--    |___|__||_____|\___/|_____|__|__|___  |_____|
--                                    |_____|
--*   @Author:              Kiwi
--*   @Date:                2017-12-18T14:01:25+01:00
--*   @Project:             Imperial Civil War
--*   @Filename:            PopulatePlanetUtilities.lua
--*   @Last modified by:    Kiwi
--*   @Last modified time:  2018-02-04T10:55:16-05:00
--*   @License:             This source code may only be used with explicit permission from the developers
--*   @Copyright:           © TR: Imperial Civil War Development Team
--******************************************************************************
require("eawx-plugins/revolt-manager/LoyaltyPolicyRepository")
require("eawx-util/ChangeOwnerUtilities")
require("UnitSpawnerTables")

-- Changes Planet owner to new Owner and randomly populates with fleet adjusted around inserted combat power value,
-- random_population decides if fleets must be made of ships that were aligned to old owner or not
function ChangePlanetOwnerAndPopulate(planet, newOwner, combat_power, random_population)

	local hostile = newOwner
	local CurrentPlanetOwner = planet.Get_Owner()

	-- Possible spawning units
    -- Arranged as Unit_Table = {{Find_Object_Type("Unit_Name"), weight}}

    Unit_Table = DefineUnitTable(newOwner)
    Groundbase_Table = DefineGroundBaseTable(newOwner)

	DebugMessage("%s -- Initializing spawning", tostring(Script))
	if hostile.Get_Difficulty() == "Easy" then
		Difficulty_Modifier = 0.75
	elseif hostile.Get_Difficulty() == "Hard" then
		Difficulty_Modifier = 1.5
	else
		Difficulty_Modifier = 1.0
	end


	-- Scaled combat power based on planet value, reduced by if connected to a player, then increased or decreased by difficulty level
	scaled_combat_power = combat_power * Difficulty_Modifier
	-- pick a random unit selection table, could probably use something other than free random

	DebugMessage("%s -- Attempting to spawn units at %s, from table for %s, combat power %s, difficulty modifier %s", tostring(Script), tostring(planet), tostring(newOwner), tostring(scaled_combat_power), tostring(Difficulty_Modifier))
	-- Spawns random units at the planet for the given faction and combat power per planet
	Spawn_Random_Units(Unit_Table, planet, hostile, CurrentPlanetOwner, scaled_combat_power, random_population)

end

-- Spawns random units at a given planet for a given player, up to a maximum combat power
-- In: unit table to spawn from, planet location, playerobject, combat power to spawn at planet, ignore the affiliation of units in the table
function Spawn_Random_Units(unit_spawn_table, planet, player, CurrentPlanetOwner, total_combat_power, ignore_affiliation)

	if not unit_spawn_table or not planet or not player or not total_combat_power then
		DebugMessage("%s -- Expected arguments: spawn table, planet, playerobject, combat power. Got %s, %s, %s, %s instead", tostring(Script), tostring(unit_spawn_table), tostring(planet), tostring(player), tostring(total_combat_power))
		return
	end

    ChangePlanetOwnerAndRetreat(planet, player)

	DebugMessage("%s -- Attempting to spawn units at %s", tostring(Script), tostring(planet))
	-- empty spawn table
	local spawn_table = {}
	-- Create distribution to sample from
	local distribution_space = DiscreteDistribution.Create()
	local distribution_land = DiscreteDistribution.Create()

	-- Add units to distributions
	for _, possible_spawn in pairs(unit_spawn_table) do

		if possible_spawn[3] == "Space" then
			--Insert unit into distribution
			distribution_space.Insert(possible_spawn[1], possible_spawn[2])
		end

		if possible_spawn[3] == "Land" then
			--Insert unit into distribution
			distribution_land.Insert(possible_spawn[1], possible_spawn[2])
		end
	end

	--Add units to the spawn table
	SpawnTableInsert(total_combat_power, 5, distribution_space, spawn_table, false)
	SpawnTableInsert(total_combat_power/10, 3, distribution_land, spawn_table, true)

	-- spawn the units!
	SpawnGroundBase(player, planet)
	SpawnListType(spawn_table, planet, player)
	return
end

-- Insert units into the spawn table
-- In: combat value for the units, number of units, distribution of units,
-- the spawn_table to fill, and boolean for land/space
function SpawnTableInsert(combat_value, count, distribution, spawn_table, land)

	local total_count = 0

	local combat_value_to_spawn = combat_value/count

	local combat_value_j = 0

	while combat_value_j <= combat_value do

		if land and total_count > 5 then
			break
		end

        unit = Select_Spawn_Unit(player, combat_value_to_spawn, distribution)
        -- Get a number of the units based on their combat rating
        local spawn_unit = unit
        if TestValid(spawn_unit) then
            unitcount = combat_value_to_spawn / spawn_unit.Get_Combat_Rating()

            if not unit then
                DebugMessage("%s -- Error! unit not found!", tostring(Script))
            end
            -- add up to count of the same unit from the Select_Spawn_Unit function
            local j = 0
            while j < unitcount do
                table.insert(spawn_table, unit)
                j = j + 1
            end
            combat_value_j = combat_value_j + combat_value_to_spawn
            total_count = total_count + unitcount
        else
            combat_value_j = combat_value + 1
        end

	end

	return
end

-- choose spawn units from a distribution based on combat power
-- In: playerobject, combat power desired, discrete distribution of units
function Select_Spawn_Unit(player, combat_value, distribution, space)
	local space = false
	local cap = GameRandom.Free_Random(1, 3)
	-- sample the distribution to pick a unit (hopefully random)
	local spawn_unit = distribution.Sample()

	-- none found?
	if not spawn_unit then
		return nil
	end

	-- Get a number of the units based on their combat rating
	local spawn_count = combat_value / spawn_unit.Get_Combat_Rating()

	-- cap the spawn count at either 1 or 5 if space, or 3 (default) if land
	if space then
		cap = 5
	end

	if spawn_count < 1 then
		spawn_count = 1
	elseif spawn_count > cap then
		spawn_count = cap
    end

    DebugMessage("%s -- Chosen %d of %s to spawn", tostring(Script), spawn_count, tostring(spawn_unit.Get_Name()))


    -- return the spawn unit and count
	return spawn_unit

end

-- Simple spawn function that can use the found object list instead of the name list
-- In: List of gameobjects, location, playerobject
function SpawnListType(type_list, entry_marker, player)

	for _, unit_type in pairs(type_list) do
		new_units = Spawn_Unit(unit_type, entry_marker, player)
		for _, unit in pairs(new_units) do
			unit.Prevent_AI_Usage(false)
		end
	end

	new_units = nil
	return
end

-- Spawns groundbase for player
-- In: playerobject, planet to spawn at, table index to get ground bases from
function SpawnGroundBase(player, planet, index)

	local base_table = Groundbase_Table

	local base_level = EvaluatePerception("MaxGroundbaseLevel", player, planet)

	if base_level == nil then
		return
	end

	if base_level < 1 then
		return
	else
		local m = 0
		while m < base_level/2 do
		    building = base_table[GameRandom.Free_Random(1,3)]
			Spawn_Unit(building, planet, player)
			m = m + 1
		end
	end

	return
end