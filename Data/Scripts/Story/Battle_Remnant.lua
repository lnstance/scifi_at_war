--******************************************************************************
--     _______ __
--    |_     _|  |--.----.---.-.--.--.--.-----.-----.
--      |   | |     |   _|  _  |  |  |  |     |__ --|
--      |___| |__|__|__| |___._|________|__|__|_____|
--     ______
--    |   __ \.-----.--.--.-----.-----.-----.-----.
--    |      <|  -__|  |  |  -__|     |  _  |  -__|
--    |___|__||_____|\___/|_____|__|__|___  |_____|
--                                    |_____|
--*   @Author:              [TR]Pox
--*   @Date:                2017-12-14T10:54:01+01:00
--*   @Project:             Imperial Civil War
--*   @Filename:            Survival_Remnant.lua
--*   @Last modified by:    [TR]Pox
--*   @Last modified time:  2017-12-21T13:18:54+01:00
--*   @License:             This source code may only be used with explicit permission from the developers
--*   @Copyright:           © TR: Imperial Civil War Development Team
--******************************************************************************



require("PGBase")
require("PGStateMachine")
require("PGStoryMode")
require("PGSpawnUnits")

------------------------------------------------------------------------------------------------


function Definitions()

	DebugMessage("%s -- In Definitions", tostring(Script))

	StoryModeEvents = { Setup_Start = Begin_GC,
						Battle_Start = Begin_Battle}

	pirates = Find_Player("Hutts")


end


function Begin_GC(message)
	if message == OnEnter then

		

		empire_spawn_marker = Find_First_Object("INSTANTACTION_MARKER_EMPIRE")
		rebel_spawn_marker = Find_First_Object("INSTANTACTION_MARKER_NEWREP")
		hapan_spawn_marker = Find_First_Object("INSTANTACTION_MARKER_HAPANS")
		eoth_spawn_marker = Find_First_Object("INSTANTACTION_MARKER_EOTH")
		csa_spawn_marker = Find_First_Object("INSTANTACTION_MARKER_CSA")
		pentastar_spawn_marker = Find_First_Object("INSTANTACTION_MARKER_PENTASTAR")
	
		rebel = Find_Player("Rebel")
		empire = Find_Player("Empire")
		hapans = Find_Player("Hapes_Consortium")
		eoth = Find_Player("EmpireoftheHand")
		pentastar = Find_Player("Pentastar")
		zsinj = Find_Player("Pirates")
		maldrood = Find_Player("Teradoc")
		eriadu = Find_Player("Hutts")
		csa = Find_Player("Corporate_Sector")
		
		 empire.Make_Ally(hapans)
		 empire.Make_Ally(rebel)
		 empire.Make_Ally(eoth)
		 empire.Make_Ally(pentastar)
		 empire.Make_Ally(zsinj)
		 empire.Make_Ally(maldrood)
		 empire.Make_Ally(eriadu)
		 empire.Make_Ally(csa)
		 
		 rebel.Make_Ally(empire)
		 rebel.Make_Ally(hapans)
		 rebel.Make_Ally(eoth)
		 rebel.Make_Ally(pentastar)
		 rebel.Make_Ally(zsinj)
		 rebel.Make_Ally(maldrood)
		 rebel.Make_Ally(eriadu)
		 rebel.Make_Ally(csa)

		 eoth.Make_Ally(empire)
		 eoth.Make_Ally(hapans)
		 eoth.Make_Ally(rebel)
		 eoth.Make_Ally(pentastar)
		 eoth.Make_Ally(zsinj)
		 eoth.Make_Ally(maldrood)
		 eoth.Make_Ally(eriadu)
		 eoth.Make_Ally(csa)
		 
		 hapans.Make_Ally(rebel)
		 hapans.Make_Ally(empire)
		 hapans.Make_Ally(eoth)
		 hapans.Make_Ally(pentastar)
		 hapans.Make_Ally(zsinj)
		 hapans.Make_Ally(maldrood)
		 hapans.Make_Ally(eriadu)
		 hapans.Make_Ally(csa)
		 
		 pentastar.Make_Ally(empire)
		 pentastar.Make_Ally(hapans)
		 pentastar.Make_Ally(rebel)
		 pentastar.Make_Ally(eoth)
		 pentastar.Make_Ally(zsinj)
		 pentastar.Make_Ally(maldrood)
		 pentastar.Make_Ally(eriadu)
		 pentastar.Make_Ally(csa)
		 
		 zsinj.Make_Ally(empire)
		 zsinj.Make_Ally(hapans)
		 zsinj.Make_Ally(rebel)
		 zsinj.Make_Ally(eoth)
		 zsinj.Make_Ally(pentastar)
		 zsinj.Make_Ally(maldrood)
		 zsinj.Make_Ally(eriadu)
		 zsinj.Make_Ally(csa)
		 
		 maldrood.Make_Ally(empire)
		 maldrood.Make_Ally(hapans)
		 maldrood.Make_Ally(rebel)
		 maldrood.Make_Ally(eoth)
		 maldrood.Make_Ally(pentastar)
		 maldrood.Make_Ally(zsinj)
		 maldrood.Make_Ally(eriadu)
		 maldrood.Make_Ally(csa)
		 
		 eriadu.Make_Ally(empire)
		 eriadu.Make_Ally(hapans)
		 eriadu.Make_Ally(rebel)
		 eriadu.Make_Ally(eoth)
		 eriadu.Make_Ally(pentastar)
		 eriadu.Make_Ally(zsinj)
		 eriadu.Make_Ally(maldrood)
		 eriadu.Make_Ally(csa)
		 
		 csa.Make_Ally(empire)
		 csa.Make_Ally(hapans)
		 csa.Make_Ally(rebel)
		 csa.Make_Ally(eoth)
		 csa.Make_Ally(pentastar)
		 csa.Make_Ally(zsinj)
		 csa.Make_Ally(maldrood)
		 csa.Make_Ally(eriadu)
		 
		 	
		empire_spawn_marker.Change_Owner(empire)
		rebel_spawn_marker.Change_Owner(empire)
		
		if TestValid(hapan_spawn_marker) then
			hapan_spawn_marker.Change_Owner(empire)
		end 
		if TestValid(eoth_spawn_marker) then
			eoth_spawn_marker.Change_Owner(empire)
		end
		if TestValid(csa_spawn_marker) then	
			csa_spawn_marker.Change_Owner(empire)
		end
		if TestValid(pentastar_spawn_marker) then		
			pentastar_spawn_marker.Change_Owner(empire)
		end
	end
end

function Begin_Battle(message)
	if message == OnEnter then

		empire_spawn_marker.Make_Invulnerable(true)
		rebel_spawn_marker.Make_Invulnerable(true)

		
		if TestValid(hapan_spawn_marker) then
			hapan_spawn_marker.Make_Invulnerable(true)
		end
		if TestValid(eoth_spawn_marker) then
			eoth_spawn_marker.Make_Invulnerable(true)
		end
		
		empire.Make_Enemy(hapans)
		empire.Make_Enemy(rebel)
		empire.Make_Enemy(eoth)
		empire.Make_Enemy(pentastar)
		empire.Make_Enemy(zsinj)
		empire.Make_Enemy(maldrood)
		empire.Make_Enemy(eriadu)
		empire.Make_Enemy(csa)
		
		rebel.Make_Enemy(empire)
		rebel.Make_Enemy(hapans)
		rebel.Make_Enemy(eoth)
		rebel.Make_Enemy(pentastar)
		rebel.Make_Enemy(zsinj)
		rebel.Make_Enemy(maldrood)
		rebel.Make_Enemy(eriadu)
		rebel.Make_Enemy(csa)

		eoth.Make_Enemy(empire)
		eoth.Make_Enemy(hapans)
		eoth.Make_Enemy(rebel)
		eoth.Make_Enemy(pentastar)
		eoth.Make_Enemy(zsinj)
		eoth.Make_Enemy(maldrood)
		eoth.Make_Enemy(eriadu)
		eoth.Make_Enemy(csa)
		
		hapans.Make_Enemy(rebel)
		hapans.Make_Enemy(empire)
		hapans.Make_Enemy(eoth)
		hapans.Make_Enemy(pentastar)
		hapans.Make_Enemy(zsinj)
		hapans.Make_Enemy(maldrood)
		hapans.Make_Enemy(eriadu)
		hapans.Make_Enemy(csa)
		
		pentastar.Make_Enemy(empire)
		pentastar.Make_Enemy(hapans)
		pentastar.Make_Enemy(rebel)
		pentastar.Make_Enemy(eoth)
		pentastar.Make_Enemy(zsinj)
		pentastar.Make_Enemy(maldrood)
		pentastar.Make_Enemy(eriadu)
		pentastar.Make_Enemy(csa)
		
		zsinj.Make_Enemy(empire)
		zsinj.Make_Enemy(hapans)
		zsinj.Make_Enemy(rebel)
		zsinj.Make_Enemy(eoth)
		zsinj.Make_Enemy(pentastar)
		zsinj.Make_Enemy(maldrood)
		zsinj.Make_Enemy(eriadu)
		zsinj.Make_Enemy(csa)
		
		maldrood.Make_Enemy(empire)
		maldrood.Make_Enemy(hapans)
		maldrood.Make_Enemy(rebel)
		maldrood.Make_Enemy(eoth)
		maldrood.Make_Enemy(pentastar)
		maldrood.Make_Enemy(zsinj)
		maldrood.Make_Enemy(eriadu)
		maldrood.Make_Enemy(csa)
		
		eriadu.Make_Enemy(empire)
		eriadu.Make_Enemy(hapans)
		eriadu.Make_Enemy(rebel)
		eriadu.Make_Enemy(eoth)
		eriadu.Make_Enemy(pentastar)
		eriadu.Make_Enemy(zsinj)
		eriadu.Make_Enemy(maldrood)
		eriadu.Make_Enemy(csa)
		
		csa.Make_Enemy(empire)
		csa.Make_Enemy(hapans)
		csa.Make_Enemy(rebel)
		csa.Make_Enemy(eoth)
		csa.Make_Enemy(pentastar)
		csa.Make_Enemy(zsinj)
		csa.Make_Enemy(maldrood)
		csa.Make_Enemy(eriadu)
	end
end









