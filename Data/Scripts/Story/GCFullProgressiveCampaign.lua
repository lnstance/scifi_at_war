--******************************************************************************
--     _______ __
--    |_     _|  |--.----.---.-.--.--.--.-----.-----.
--      |   | |     |   _|  _  |  |  |  |     |__ --|
--      |___| |__|__|__| |___._|________|__|__|_____|
--     ______
--    |   __ \.-----.--.--.-----.-----.-----.-----.
--    |      <|  -__|  |  |  -__|     |  _  |  -__|
--    |___|__||_____|\___/|_____|__|__|___  |_____|
--                                    |_____|
--*   @Author:              Corey
--*   @Date:                2017-12-18T14:01:09+01:00
--*   @Project:             Imperial Civil War
--*   @Filename:            GCWarlordsCampaign.lua
--*   @Last modified by:
--*   @Last modified time:  2018-03-13T22:28:32-04:00
--*   @License:             This source code may only be used with explicit permission from the developers
--*   @Copyright:           © TR: Imperial Civil War Development Team
--******************************************************************************

require("PGStoryMode")
require("PGSpawnUnits")
require("eawx-util/ChangeOwnerUtilities")
StoryUtil = require("eawx-util/StoryUtil")

function Definitions()
    DebugMessage("%s -- In Definitions", tostring(Script))

    StoryModeEvents = {
        Determine_Faction_LUA = Find_Faction,
        Level_Check = Determine_Start_Era,
        Maldrood_Antem = Antem_Maldrood,
        Maldrood_Kashyyyk = Kashyyyk_Maldrood,
        Maldrood_Commenor = Commenor_Maldrood,
        Eriadu_Elrood = Elrood_Eriadu,
        Zsinj_Centares = Centares_Zsinj,
        Set_Subera_Isard = SubEra_Change,
        Zero_Command_Split = Spawn_Harrsk,
        E_Level_Two = ThrawnSpawns,
        E_Level_Three = DarkEmpireSpawns,
        E_Level_Four = DaalaSpawns,
        Set_Subera_Jax = Empire_Fractures,
        E_Level_Five = PellaeonSpawns,
        Trigger_Ciutric = Ciutric_Breakaway,
        Vassal_Release_Hapes = HapesVassalEmerge,
        NewRep_Evac_Trigger = Check_Coruscant_Owner,
        Empire_wins_Coruscant = Spawn_Empire_Reward,
		Check_Executor_Build = Cronus_Lives,
		Loop_Clearer = Perpetuator,
		STORY_FLAG_KATANA_COUNT = Spawn_Katana,
		Katana_Spawn_Throttle = Perpetuator2, 
		Nzoth_Unlocked = Black_Fleet_Crisis,
		Nzoth_Conquer = EXF_Spawn,
    }
end

function Find_Faction(message)
    if message == OnEnter then
        DebugMessage("Find_Faction Entered")
        spawn_location_table = {
            ["ANTEM"] = false,
            ["BASTION"] = false,
            ["BYSS"] = false,
            ["CARIDA"] = false,
            ["CENTARES"] = false,
            ["CIUTRIC"] = false,
            ["COMMENOR"] = false,
            ["CORUSCANT"] = false,
            ["CORVIS"] = false,
            ["DATHOMIR"] = false,
            ["DA_SOOCHAV"] = false,
            ["ELROOD"] = false,
            ["ENTRALLA"] = false,
            ["EOLSHA"] = false,
            ["ERIADU"] = false,
			["ETTI_IV"] = false,
            ["GAROS_IV"] = false,
            ["HAKASSI"] = false,
            ["KALIST"] = false,
            ["KAMPE"] = false,
            ["KASHYYYK"] = false,
            ["KESSEL"] = false,
            ["MONCALIMARI"] = false,
            ["MYRKR"] = false,
            ["NIRAUAN"] = false,
            ["QUETHOLD"] = false,
            ["THE_MAW"] = false,
            ["VROS"] = false,
            ["YAVIN"] = false,
            ["ABREGADO_RAE"] = false
        }

        all_planets = FindPlanet.Get_All_Planets()

        Set_Existing_Planets(all_planets, spawn_location_table)

        p_newrep = Find_Player("Rebel")
        p_empire = Find_Player("Empire")
        p_eoth = Find_Player("EmpireoftheHand")
        p_eriadu = Find_Player("Hutts")
        p_pentastar = Find_Player("Pentastar")
        p_zsinj = Find_Player("Pirates")
        p_maldrood = Find_Player("Teradoc")
        p_corporate = Find_Player("Corporate_Sector")
        p_hapes = Find_Player("Hapes_Consortium")

        if p_newrep.Is_Human() then
            Story_Event("ENABLE_BRANCH_NEWREP_FLAG")
        elseif p_empire.Is_Human() then
            Story_Event("ENABLE_BRANCH_EMPIRE_FLAG")
        elseif p_eoth.Is_Human() then
            Story_Event("ENABLE_BRANCH_EOTH_FLAG")
        elseif p_eriadu.Is_Human() then
            Story_Event("ENABLE_BRANCH_ERIADU_FLAG")
        elseif p_pentastar.Is_Human() then
            Story_Event("ENABLE_BRANCH_PENTASTAR_FLAG")
        elseif p_zsinj.Is_Human() then
            Story_Event("ENABLE_BRANCH_ZSINJ_FLAG")
        elseif p_maldrood.Is_Human() then
            Story_Event("ENABLE_BRANCH_TERADOC_FLAG")
        elseif p_corporate.Is_Human() then
            Story_Event("ENABLE_BRANCH_CORPORATE_SECTOR_FLAG")
        elseif p_hapes.Is_Human() then
            Story_Event("ENABLE_BRANCH_HAPES_FLAG")
        end

        techLevel = p_empire.Get_Tech_Level()

        if techLevel == 2 then
            Story_Event("START_LEVEL_02")
        elseif techLevel == 3 then
            Story_Event("START_LEVEL_03")
        elseif techLevel == 4 then
            Story_Event("START_LEVEL_04")
        elseif techLevel == 5 then
            Story_Event("START_LEVEL_05")
        end

        DebugMessage("Find_Faction Done")
    end
end

function Set_Existing_Planets(planet_list, planet_table)
    DebugMessage("Set_Existing_Planets Started")
    for _, planet in pairs(planet_list) do
        name = planet.Get_Type().Get_Name()

        if planet_table[name] ~= nil then
            planet_table[name] = true
        end
        
    end
    DebugMessage("Set_Existing_Planets Done")
end

function Determine_Start_Era(message)
    if message == OnEnter then
        DebugMessage("Determine_start_Era Started")
        p_newrep = Find_Player("Rebel")
        p_empire = Find_Player("Empire")
        p_eoth = Find_Player("EmpireoftheHand")
        p_eriadu = Find_Player("Hutts")
        p_pentastar = Find_Player("Pentastar")
        p_zsinj = Find_Player("Pirates")
        p_maldrood = Find_Player("Teradoc")
        p_corporate = Find_Player("Corporate_Sector")

        techLevel = p_empire.Get_Tech_Level()

        if techLevel == 1 then
			Story_Event("NH_Enable")
            -- CSA : Re-enable after planet checks
            -- start_planet = FindPlanet("Ession")
            -- if start_planet.Get_Owner() ~= Find_Player("Corporate_Sector") then
            -- start_planet = TRUtil.FindFriendlyPlanet(p_corporate)
            -- end
            -- if start_planet then
            -- spawn_list_csa = { "Krin_Invincible" }
            -- SpawnList(spawn_list_csa, start_planet, p_corporate, true, false)
            -- end
            -- start_planet = FindPlanet("Etti_IV")
            -- if start_planet.Get_Owner() ~= Find_Player("Corporate_Sector") then
            -- start_planet = TRUtil.FindFriendlyPlanet(p_corporate)
            -- end
            -- if start_planet then
            -- spawn_list_csa = { "Grumby_Notropis", "Sloane_Enforce", "Fiolla_Team", "Odumin_Team", "Karrek_Flim_Team", "Fasser_Team", "Nieler_Team" }
            -- SpawnList(spawn_list_csa, start_planet, p_corporate, true, false)
            -- end
            -- Teradoc
            if spawn_location_table["CENTARES"] then
                start_planet = FindPlanet("Centares")
                if start_planet.Get_Owner() ~= Find_Player("Teradoc") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_maldrood)
                end
                if start_planet then
                    spawn_list_teradoc = {"Neomen_Ion_Storm", "13x_Teradoc", "Therbon_Allegiance"}
                    SpawnList(spawn_list_teradoc, start_planet, p_maldrood, true, false)
                end
            end

            if spawn_location_table["NIRAUAN"] then
                start_planet = FindPlanet("Nirauan")
                if start_planet.Get_Owner() ~= Find_Player("EmpireOfTheHand") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_eoth)
                end
                if start_planet then
                    spawn_list_eoth = {"Grey_Wolf", "Aralani_Frontier", "Niriz_Admonitor", "Parck_Strikefast", "Stent_Thrawns_Hand", "Hand_of_Judgement_Team"}
                    SpawnList(spawn_list_eoth, start_planet, p_eoth, true, false)
                end
            end

            if spawn_location_table["QUETHOLD"] then
                start_planet = FindPlanet("Quethold")
                if start_planet.Get_Owner() ~= Find_Player("EmpireOfTheHand") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_eoth)
                end
                if start_planet then
                    spawn_list_tasse = {"Tasse_Team"}
                    SpawnList(spawn_list_tasse, start_planet, p_eoth, true, false)
                end
            end

            if spawn_location_table["HAKASSI"] then
                start_planet = FindPlanet("Hakassi")
                if start_planet.Get_Owner() ~= Find_Player("Teradoc") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_maldrood)
                end
                if start_planet then
                    spawn_list_teradoc = {"Lancet_Kosh"}
                    SpawnList(spawn_list_teradoc, start_planet, p_maldrood, true, false)
                end
            end

            -- Zsinj
            if spawn_location_table["DATHOMIR"] then
                start_planet = FindPlanet("Dathomir")
                if start_planet.Get_Owner() ~= Find_Player("Pirates") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_zsinj)
                end
                if start_planet then
                    spawn_list_zsinj = {
                        "Night_Caller",
                        "Iron_Fist",
                        "Joshi_Provocateur",
                        "Nabyl_Hawkbat",						
                        "Banjeer",
                        "Netbers_Team",
                        "Lanu_Team",
                        "Gethzerion_Team",
                        "Implacable_Star_Destroyer",
						"181st_Cowall"
                    }
                    SpawnList(spawn_list_zsinj, start_planet, p_zsinj, true, false)
                end
            end

            if spawn_location_table["GAROS_IV"] then
                start_planet = FindPlanet("Garos_IV")
                if start_planet.Get_Owner() ~= Find_Player("Pirates") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_zsinj)
                end
                if start_planet then
                    spawn_list_zsinj = {"Demolisher"}
                    SpawnList(spawn_list_zsinj, start_planet, p_zsinj, true, false)
                end
            end

            -- Pentastar
            if spawn_location_table["ENTRALLA"] then
                start_planet = FindPlanet("Entralla")
                if start_planet.Get_Owner() ~= Find_Player("Pentastar") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_pentastar)
                end
                if start_planet then
                    spawn_list_zsinj = {"Grant_Oriflamme", "Sariss_Team", "Jerec_Team"}
                    SpawnList(spawn_list_zsinj, start_planet, p_pentastar, true, false)
                end
            end

            -- Eriadu
            if spawn_location_table["ERIADU"] then
                start_planet = FindPlanet("Eriadu")
                if start_planet.Get_Owner() ~= Find_Player("Hutts") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_eriadu)
                end
                if start_planet then
                    spawn_list_delvardus = {"General_Veers_Team", "Thalassa", "Kabalian_Star_Destroyer", "Johans_Team"}
                    SpawnList(spawn_list_delvardus, start_planet, p_eriadu, true, false)
                end
            end
        elseif techLevel == 2 then
            -- CSA : Re-enable after planet checks
            -- start_planet = FindPlanet("Ession")
            -- if start_planet.Get_Owner() ~= Find_Player("Corporate_Sector") then
            -- start_planet = TRUtil.FindFriendlyPlanet(p_corporate)
            -- end
            -- if start_planet then
            -- spawn_list_csa = { "Krin_Invincible" }
            -- SpawnList(spawn_list_csa, start_planet, p_corporate, true, false)
            -- end
            -- start_planet = FindPlanet("Etti_IV")
            -- if start_planet.Get_Owner() ~= Find_Player("Corporate_Sector") then
            -- start_planet = TRUtil.FindFriendlyPlanet(p_corporate)
            -- end
            -- if start_planet then
            -- spawn_list_csa = { "Grumby_Notropis", "Sloane_Enforce", "Fiolla_Team", "Odumin_Team", "Karrek_Flim_Team", "Fasser_Team", "Nieler_Team" }
            -- SpawnList(spawn_list_csa, start_planet, p_corporate, true, false)
            -- end
            -- Empire
            if spawn_location_table["CORUSCANT"] then
                start_planet = FindPlanet("Coruscant")
                if start_planet.Get_Owner() ~= Find_Player("Empire") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_empire)
                end
                if start_planet then
                    spawn_list_thrawn = {"Chimera", "Corellian_Gunboat_Ferrier", "Rukh_Team"}
                    SpawnList(spawn_list_thrawn, start_planet, p_empire, true, false)
                end
            end

            if spawn_location_table["CORUSCANT"] then
                start_planet = FindPlanet("Coruscant")
                if start_planet.Get_Owner() ~= Find_Player("Empire") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_empire)
                end
                if start_planet then
                    spawn_list_Thrawn = {
                        "181st_TIE_Interceptor_Squadron",
                        "General_Covell_Team",
                        "Judicator_Star_Destroyer",
                        "Relentless_Star_Destroyer",
						"Joruus_Cboath_Team",
						"Dezon_Constrainer",
						"Drost_Team"
                    }
                    SpawnList(spawn_list_Thrawn, start_planet, p_empire, true, false)
                end
            end

            -- New Republic
            if spawn_location_table["CORUSCANT"] then
                start_planet = FindPlanet("Coruscant")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
                if start_planet then
                    spawn_list_Thrawn = {"Iblis_Peregrine"}
                    SpawnList(spawn_list_Thrawn, start_planet, p_newrep, true, false)
                end
            end

            if spawn_location_table["MYRKR"] then
                start_planet = FindPlanet("Myrkr")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
                if start_planet then
                    spawn_list_Thrawn = {"Errant_Venture", "Mara_Saber_Team", "Wild_karrde"}
                    SpawnList(spawn_list_Thrawn, start_planet, p_newrep, true, false)
                end
            end

            -- Hand
            if spawn_location_table["NIRAUAN"] then
                start_planet = FindPlanet("Nirauan")
                if start_planet.Get_Owner() ~= Find_Player("EmpireoftheHand") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_eoth)
                end
                if start_planet then
                    spawn_list_Hand = {"Soontir_Fel_Squadron", "Aralani_Frontier", "Niriz_Admonitor", "Parck_Strikefast", "Stent_Thrawns_Hand", "Hand_of_Judgement_Team"}
                    SpawnList(spawn_list_Hand, start_planet, p_eoth, true, false)
                end
            end

            -- Teradoc
            if spawn_location_table["HAKASSI"] then
                start_planet = FindPlanet("Hakassi")
                if start_planet.Get_Owner() ~= Find_Player("Teradoc") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_maldrood)
                end
                if start_planet then
                    spawn_list_teradoc = {"Lancet_Kosh", "13x_Teradoc", "Tavira_Invidious", "Riizolo_Neutron"}
                    SpawnList(spawn_list_teradoc, start_planet, p_maldrood, true, false)
                end
            end

            -- Pentastar
            if spawn_location_table["ENTRALLA"] then
                start_planet = FindPlanet("Entralla")
                if start_planet.Get_Owner() ~= Find_Player("Pentastar") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_pentastar)
                end
                if start_planet then
                    spawn_list_zsinj = {
                        "Grant_Oriflamme",
                        "Reaper_Kaine",
                        "Gregor_Team",
                        "Dekeet_Praetor",
                        "Dynamic_Besk",
                        "Otro_Enforcer"
                    }
                    SpawnList(spawn_list_zsinj, start_planet, p_pentastar, true, false)
                end
            end

            -- Eriadu
            if spawn_location_table["KAMPE"] then
                start_planet = FindPlanet("Kampe")
                if start_planet.Get_Owner() ~= Find_Player("Hutts") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_eriadu)
                end
                if start_planet then
                    spawn_list_delvardus = {"Night_Hammer_Delvardus"}
                    SpawnList(spawn_list_delvardus, start_planet, p_eriadu, true, false)
                end
            end
        elseif techLevel == 3 then
            -- CSA : Re-enable after planet checks
            -- start_planet = FindPlanet("Ession")
            -- if start_planet.Get_Owner() ~= Find_Player("Corporate_Sector") then
            -- start_planet = TRUtil.FindFriendlyPlanet(p_corporate)
            -- end
            -- if start_planet then
            -- spawn_list_csa = { "Krin_Invincible" }
            -- SpawnList(spawn_list_csa, start_planet, p_corporate, true, false)
            -- end
            -- start_planet = FindPlanet("Etti_IV")
            -- if start_planet.Get_Owner() ~= Find_Player("Corporate_Sector") then
            -- start_planet = TRUtil.FindFriendlyPlanet(p_corporate)
            -- end
            -- if start_planet then
            -- spawn_list_csa = { "Grumby_Notropis", "Sloane_Enforce", "Fiolla_Team", "Odumin_Team", "Karrek_Flim_Team", "Fasser_Team", "Nieler_Team" }
            -- SpawnList(spawn_list_csa, start_planet, p_corporate, true, false)
            -- end
            -- Empire
            if spawn_location_table["BYSS"] then
                start_planet = FindPlanet("Byss")
                spawn_list_Palpatine = {
					"Umak_Team",
                    "Sedriss_Team",
                    "Emperor_Palpatine_Team",
                    "General_Veers_Team",
					"Praji_Secutor",
                    "Chimera_Pellaeon_Vice",
					"Cronal_Singularity"
                }
                SpawnList(spawn_list_Palpatine, start_planet, p_empire, true, false)
            end
            -- New Republic
            if spawn_location_table["MONCALIMARI"] then
                start_planet = FindPlanet("MonCalimari")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
            end

            checkLeia = Find_First_Object("Princess_Leia")
            if TestValid(checkLeia) then
                checkLeia.Despawn()
                spawn_list_Leia = {"Princess_Leia_Team_Noghri"}
                SpawnList(spawn_list_Leia, start_planet, p_newrep, true, false)
            end

            checkAckbar = Find_First_Object("Home_One")
            if TestValid(checkAckbar) then
                checkAckbar.Despawn()
                spawn_list_Ackbar = {"Galactic_Voyager"}
                SpawnList(spawn_list_Ackbar, start_planet, p_newrep, true, false)
            end

            if spawn_location_table["CORUSCANT"] then
                start_planet = FindPlanet("Coruscant")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
                if start_planet then
                    spawn_list_Thrawn = {"Iblis_Peregrine"}
                    SpawnList(spawn_list_Thrawn, start_planet, p_newrep, true, false)
                end
            end

            if spawn_location_table["MYRKR"] then
                start_planet = FindPlanet("Myrkr")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
                if start_planet then
                    spawn_list_Thrawn = {"Errant_Venture", "Mara_Saber_Team", "Wild_karrde"}
                    SpawnList(spawn_list_Thrawn, start_planet, p_newrep, true, false)
                end
            end

            -- Hand
            if spawn_location_table["NIRAUAN"] then
                start_planet = FindPlanet("Nirauan")
                if start_planet.Get_Owner() ~= Find_Player("EmpireoftheHand") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_eoth)
                end
                if start_planet then
                    spawn_list_Hand = {"Soontir_Fel_Squadron", "Siath_Battlehammer", "Niriz_Admonitor", "Parck_Strikefast", "Stent_Thrawns_Hand", "Hand_of_Judgement_Team"}
                    SpawnList(spawn_list_Hand, start_planet, p_eoth, true, false)
                end
            end

            -- Teradoc
            if spawn_location_table["HAKASSI"] then
                start_planet = FindPlanet("Hakassi")
                if start_planet.Get_Owner() ~= Find_Player("Teradoc") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_maldrood)
                end
                if start_planet then
                    spawn_list_teradoc = {"Lancet_Kosh", "13x_Teradoc"}
                    SpawnList(spawn_list_teradoc, start_planet, p_maldrood, true, false)
                end
                if not p_maldrood.Is_Human() then
                   ChangePlanetOwnerAndReplace(start_planet, p_empire)
                end
            end

            -- Pentastar
            if spawn_location_table["ENTRALLA"] then
                start_planet = FindPlanet("Entralla")
                if start_planet.Get_Owner() ~= Find_Player("Pentastar") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_pentastar)
                end
                if start_planet then
                    if p_pentastar.Is_Human() then
                        spawn_list_zsinj = {
                            "Reaper_Kaine",
                            "Gregor_Team",
                            "Dekeet_Praetor",
                            "Dynamic_Besk",
                            "Otro_Enforcer"
                        }
                    else
                        spawn_list_zsinj = {
                            "Reaper_Kaine"
                        }
                    end
                    SpawnList(spawn_list_zsinj, start_planet, p_pentastar, true, false)
                end
                if not p_pentastar.Is_Human() then
                    ChangePlanetOwnerAndReplace(start_planet, p_empire)
                 end
            end

            -- Eriadu
            if spawn_location_table["KAMPE"] then
                start_planet = FindPlanet("Kampe")
                if start_planet.Get_Owner() ~= Find_Player("Hutts") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_eriadu)
                end
                if start_planet then
                    if p_eriadu.Is_Human() then
                        spawn_list_delvardus = {
                            "Night_Hammer_Delvardus"
                        }
                    else
                        spawn_list_delvardus = {
                            "Delvardus_Brilliant"
                        }
                    end
                    SpawnList(spawn_list_delvardus, start_planet, p_eriadu, true, false)
                end
                if not p_eriadu.Is_Human() then
                    ChangePlanetOwnerAndReplace(start_planet, p_empire)
                 end
            end
        elseif techLevel == 4 then
            -- CSA : Re-enable after planet checks
            -- start_planet = FindPlanet("Ession")
            -- if start_planet.Get_Owner() ~= Find_Player("Corporate_Sector") then
            -- start_planet = TRUtil.FindFriendlyPlanet(p_corporate)
            -- end
            -- if start_planet then
            -- spawn_list_csa = { "Krin_Invincible" }
            -- SpawnList(spawn_list_csa, start_planet, p_corporate, true, false)
            -- end
            -- start_planet = FindPlanet("Etti_IV")
            -- if start_planet.Get_Owner() ~= Find_Player("Corporate_Sector") then
            -- start_planet = TRUtil.FindFriendlyPlanet(p_corporate)
            -- end
            -- if start_planet then
            -- spawn_list_csa = { "Grumby_Notropis", "Sloane_Enforce", "Fiolla_Team", "Odumin_Team", "Karrek_Flim_Team", "Fasser_Team", "Nieler_Team" }
            -- SpawnList(spawn_list_csa, start_planet, p_corporate, true, false)
            -- end
            if spawn_location_table["BYSS"] then
                Destroy_Planet("Byss")
            end
            if spawn_location_table["DA_SOOCHAV"] then
                Destroy_Planet("Da_SoochaV")
            end

            -- Empire
            if spawn_location_table["THE_MAW"] then
                start_planet = FindPlanet("The_Maw")
                spawn_list_Daala = {"Gorgon", "Odosk_Team", "Sivron_Team"}
                SpawnList(spawn_list_Daala, start_planet, p_empire, true, false)
            end

            if spawn_location_table["CARIDA"] then
                start_planet = FindPlanet("Carida")
                if start_planet.Get_Owner() ~= Find_Player("Empire") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_empire)
                end
                if start_planet then
                    spawn_list_Brakiss = {"Brakiss_Team", "Ardax_Vendetta"}
                    SpawnList(spawn_list_Brakiss, start_planet, p_empire, true, false)
                end
            end

            -- New Republic
            if spawn_location_table["YAVIN"] then
                start_planet = FindPlanet("Yavin")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
                if start_planet then
                    spawn_list_Daala = {"Cilghal_Team"}
                    SpawnList(spawn_list_Daala, start_planet, p_newrep, true, false)
                end
            end

            if spawn_location_table["CORUSCANT"] then
                start_planet = FindPlanet("Coruscant")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
            end

            checkAckbar = Find_First_Object("Home_One")
            if TestValid(checkAckbar) then
                checkAckbar.Despawn()
                spawn_list_Ackbar = {"Galactic_Voyager"}
                SpawnList(spawn_list_Ackbar, start_planet, p_newrep, true, false)
            end

            if spawn_location_table["CORUSCANT"] then
                start_planet = FindPlanet("Coruscant")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
                if start_planet then
                    spawn_list_Thrawn = {"Iblis_Peregrine"}
                    SpawnList(spawn_list_Thrawn, start_planet, p_newrep, true, false)
                end
            end

            if spawn_location_table["MYRKR"] then
                start_planet = FindPlanet("Myrkr")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
                if start_planet then
                    spawn_list_Thrawn = {"Errant_Venture", "Mara_Saber_Team", "Wild_karrde"}
                    SpawnList(spawn_list_Thrawn, start_planet, p_newrep, true, false)
                end
            end

            checkLeia = Find_First_Object("Princess_Leia")
            if TestValid(checkLeia) then
                checkLeia.Despawn()
                spawn_list_Leia = {"Princess_Leia_Team_Noghri"}
                SpawnList(spawn_list_Leia, start_planet, p_newrep, true, false)
            end
			
			
			-- CSA
			if spawn_location_table["ETTI_IV"] then
                start_planet = FindPlanet("Etti_IV")
                if start_planet.Get_Owner() ~= Find_Player("Corporate_Sector") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_corporate)
                end
                if start_planet then
                    spawn_list_Daala = {"Phineas_VSD"}
                    SpawnList(spawn_list_Daala, start_planet, p_corporate, true, false)
                end
			end

            -- Hand
            if spawn_location_table["NIRAUAN"] then
                start_planet = FindPlanet("Nirauan")
                if start_planet.Get_Owner() ~= Find_Player("EmpireoftheHand") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_eoth)
                end
                if start_planet then
                    spawn_list_Hand = {
                        "Soontir_Fel_Squadron",
                        "Siath_Battlehammer",
                        "Chak_Fel_Krsiss_Squadron_Association",
                        "Ashik_Team",
                        "Niriz_Admonitor",
                        "Parck_Strikefast",
                        "Stent_Thrawns_Hand",
                        "Hand_of_Judgement_Team"
                    }
                    SpawnList(spawn_list_Hand, start_planet, p_eoth, true, false)
                end
            end

            -- Teradoc
            if spawn_location_table["HAKASSI"] then
                start_planet = FindPlanet("Hakassi")
                if start_planet.Get_Owner() ~= Find_Player("Teradoc") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_maldrood)
                end
                if start_planet then
                    spawn_list_teradoc = {"Lancet_Kosh", "13x_Teradoc"}
                    SpawnList(spawn_list_teradoc, start_planet, p_maldrood, true, false)
                end
            end

            -- Eriadu
            if spawn_location_table["KAMPE"] then
                start_planet = FindPlanet("Kampe")
                if start_planet.Get_Owner() ~= Find_Player("Hutts") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_eriadu)
                end
                if start_planet then
                    spawn_list_delvardus = {"Night_Hammer_Delvardus"}
                    SpawnList(spawn_list_delvardus, start_planet, p_eriadu, true, false)
                end
            end

            -- Pentastar
            if spawn_location_table["ENTRALLA"] then
                start_planet = FindPlanet("Entralla")
                if start_planet.Get_Owner() ~= Find_Player("Pentastar") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_pentastar)
                end
                if start_planet then
                    spawn_list_zsinj = {"Gregor_Team", "Dekeet_Praetor", "Dynamic_Besk", "Otro_Enforcer"}
                    SpawnList(spawn_list_zsinj, start_planet, p_pentastar, true, false)
                end
            end
        elseif techLevel == 5 then
            if spawn_location_table["BYSS"] then
                Destroy_Planet("Byss")
            end
            if spawn_location_table["DA_SOOCHAV"] then
                Destroy_Planet("Da_SoochaV")
            end
            if spawn_location_table["CARIDA"] then
                Destroy_Planet("Carida")
            end
            if spawn_location_table["EOLSHA"] then
                Destroy_Planet("EolSha")
            end

            -- Empire

            if spawn_location_table["BASTION"] then
                start_planet = FindPlanet("Bastion")
                if start_planet.Get_Owner() ~= Find_Player("Empire") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_empire)
                end
                if start_planet then
                    spawn_list_Pellaeon = {
                        "Chimera_Pellaeon_Grand",
                        "Disra_Team",
                        "Tierce_Team",
                        "Ascian",
                        "Rogriss_Dominion",
                        "Navett_Team",
                        "181st_Stele",
                        "Hestiv_Team"
                    }
                    SpawnList(spawn_list_Pellaeon, start_planet, p_empire, true, false)
                end
            end
			
			if spawn_location_table["HAKASSI"] then
                start_planet = FindPlanet("Hakassi")
                if start_planet.Get_Owner() ~= Find_Player("Empire") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_empire)
                end
                if start_planet then
                    spawn_list_Scylla = {
                        "Scylla"
                    }
                    SpawnList(spawn_list_Scylla, start_planet, p_empire, true, false)
                end
            end

            -- New Republic
            if spawn_location_table["CORUSCANT"] then
                start_planet = FindPlanet("Coruscant")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
                if start_planet then
                    spawn_list_Palpatine = {"Gavrisom_Team"}
                    SpawnList(spawn_list_Palpatine, start_planet, p_newrep, true, false)
                end
            end

            if spawn_location_table["YAVIN"] then
                start_planet = FindPlanet("Yavin")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
                if start_planet then
                    spawn_list_Daala = {"Cilghal_Team"}
                    SpawnList(spawn_list_Daala, start_planet, p_newrep, true, false)
                end
            end

            if spawn_location_table["CORUSCANT"] then
                start_planet = FindPlanet("Coruscant")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
                if start_planet then
                    spawn_list_Daala = {"Bell_Endurance"}
                    SpawnList(spawn_list_Daala, start_planet, p_newrep, true, false)
                end
            end

            checkAckbar = Find_First_Object("Home_One")
            if TestValid(checkAckbar) then
                checkAckbar.Despawn()
                spawn_list_Ackbar = {"Galactic_Voyager"}
                SpawnList(spawn_list_Ackbar, start_planet, p_newrep, true, false)
            end

            if spawn_location_table["CORUSCANT"] then
                start_planet = FindPlanet("Coruscant")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
                if start_planet then
                    spawn_list_Thrawn = {"Iblis_Peregrine"}
                    SpawnList(spawn_list_Thrawn, start_planet, p_newrep, true, false)
                end
            end

            if spawn_location_table["MYRKR"] then
                start_planet = FindPlanet("Myrkr")
                if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
                end
                if start_planet then
                    spawn_list_Thrawn = {"Errant_Venture", "Mara_Saber_Team", "Wild_karrde"}
                    SpawnList(spawn_list_Thrawn, start_planet, p_newrep, true, false)
                end
            end

            checkLeia = Find_First_Object("Princess_Leia")
            if TestValid(checkLeia) then
                checkLeia.Despawn()
                spawn_list_Leia = {"Princess_Leia_Team_Noghri"}
                SpawnList(spawn_list_Leia, start_planet, p_newrep, true, false)
            end
			
			-- CSA
			if spawn_location_table["ETTI_IV"] then
                start_planet = FindPlanet("Etti_IV")
                if start_planet.Get_Owner() ~= Find_Player("Corporate_Sector") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_corporate)
                end
                if start_planet then
                    spawn_list_Daala = {"Phineas_VSD"}
                    SpawnList(spawn_list_Daala, start_planet, p_corporate, true, false)
                end
			end

            -- Hand
            if spawn_location_table["NIRAUAN"] then
                start_planet = FindPlanet("Nirauan")
                if start_planet.Get_Owner() ~= Find_Player("EmpireoftheHand") then
                    start_planet = StoryUtil.FindFriendlyPlanet(p_eoth)
                end
                if start_planet then
                    spawn_list_Hand = {
                        "Soontir_Fel_Squadron",
                        "Aurek_Seven_Team",
                        "Siath_Battlehammer",
                        "Chak_Fel_Krsiss_Squadron_Association",
                        "Ashik_Team",
                        "Niriz_Admonitor",
                        "Parck_Strikefast",
                        "Stent_Thrawns_Hand",
                        "Hand_of_Judgement_Team"
                    }
                    SpawnList(spawn_list_Hand, start_planet, p_eoth, true, false)
                end
            end

        -- CSA : Re-enable after planet checks
        -- start_planet = FindPlanet("Ession")
        -- if start_planet.Get_Owner() ~= Find_Player("Corporate_Sector") then
        -- start_planet = TRUtil.FindFriendlyPlanet(p_corporate)
        -- end
        -- if start_planet then
        -- spawn_list_csa = { "Krin_Invincible" }
        -- SpawnList(spawn_list_csa, start_planet, p_corporate, true, false)
        -- end

        -- start_planet = FindPlanet("Etti_IV")
        -- if start_planet.Get_Owner() ~= Find_Player("Corporate_Sector") then
        -- start_planet = TRUtil.FindFriendlyPlanet(p_corporate)
        -- end
        -- if start_planet then
        -- spawn_list_csa = { "Grumby_Notropis", "Sloane_Enforce", "Fiolla_Team", "Odumin_Team", "Karrek_Flim_Team", "Fasser_Team", "Nieler_Team" }
        -- SpawnList(spawn_list_csa, start_planet, p_corporate, true, false)
        -- end
        end

        placeholder_table = Find_All_Objects_Of_Type("Placement_Dummy")
        for i, unit in pairs(placeholder_table) do
            unit.Despawn()
        end

        DebugMessage("Determine_start_Era Done")
    end
end

function Commenor_Maldrood(message)
    if message == OnEnter then
        DebugMessage("Commenor_Maldrood Started")
        p_maldrood = Find_Player("Teradoc")
        
        if spawn_location_table["COMMENOR"] then
            start_planet = FindPlanet("Commenor")
            p_empire = Find_Player("Empire")
            techLevel = p_empire.Get_Tech_Level()
            if techLevel == 1 then
                if start_planet.Get_Owner() == Find_Player("Teradoc") then
                    if p_maldrood.Is_Human() then
                        Story_Event("GENDARR_JOINS_SPEECH")
                    end
                    spawn_list_commenor = {"Lott_Team", "Gendarr_Reliance"}
                    SpawnList(spawn_list_commenor, start_planet, p_maldrood, true, false)
                end
            end
        end
        DebugMessage("Commenor_Maldrood Finished")
    end
end

function Antem_Maldrood(message)
    if message == OnEnter then
        DebugMessage("Antem_Maldrood Started")
        p_maldrood = Find_Player("Teradoc")
        start_planet = FindPlanet("Antem")

        p_empire = Find_Player("Empire")
        techLevel = p_empire.Get_Tech_Level()
        if techLevel == 1 then
            if start_planet.Get_Owner() == Find_Player("Teradoc") then
                if p_maldrood.Is_Human() then
                    Story_Event("GETELLES_JOINS_SPEECH")
                end
                spawn_list_kosh = {"Getelles_Team", "Larm_Carrack"}
                SpawnList(spawn_list_kosh, start_planet, p_maldrood, true, false)
            --end
            end
        end
        DebugMessage("Antem_Maldrood Finished")
    end
end

function Kashyyyk_Maldrood(message)
    if message == OnEnter then
        DebugMessage("Kashyyyk_Maldrood Started")
        p_maldrood = Find_Player("Teradoc")
        start_planet = FindPlanet("Kashyyyk")

        p_empire = Find_Player("Empire")
        techLevel = p_empire.Get_Tech_Level()
        if techLevel == 1 then
            if start_planet.Get_Owner() == Find_Player("Teradoc") then
                if p_maldrood.Is_Human() then
                    Story_Event("SYN_JOINS_SPEECH")
                end
                spawn_list_syn = {"Syn_Silooth"}
                SpawnList(spawn_list_syn, start_planet, p_maldrood, true, false)
            --end
            end
        end
        DebugMessage("Kashyyyk_Maldrood Finished")
    end
end

function Elrood_Eriadu(message)
    if message == OnEnter then
        DebugMessage("Elrood_Eriadu Started")
        p_eriadu = Find_Player("Hutts")
        start_planet = FindPlanet("Elrood")

        p_empire = Find_Player("Empire")
        techLevel = p_empire.Get_Tech_Level()
        if techLevel == 1 then
            if start_planet.Get_Owner() == Find_Player("Hutts") then
                if p_eriadu.Is_Human() then
                    Story_Event("ELROOD_JOINS_SPEECH")
                end
                spawn_list_elrood = {"Andal_Team", "Zed_Stalker", "Pryl_Thunderflare"}
                SpawnList(spawn_list_elrood, start_planet, p_eriadu, true, false)
            end
        end
        DebugMessage("Elrood_Eriadu Finished")
    end
end

function Centares_Zsinj(message)
    if message == OnEnter then
        DebugMessage("Centares_Zsinj Started")
        p_zsinj = Find_Player("Pirates")
        start_planet = FindPlanet("Centares")

        p_empire = Find_Player("Empire")
        techLevel = p_empire.Get_Tech_Level()
        if techLevel == 1 then
            if start_planet.Get_Owner() == Find_Player("Pirates") then
                if p_zsinj.Is_Human() then
                    Story_Event("SELIT_JOINS_SPEECH")
                end
                spawn_list_selit = {"Selit_Team"}
                SpawnList(spawn_list_selit, start_planet, p_zsinj, true, false)
            --end
            end
        end
        DebugMessage("Centares_Zsinj Finished")
    end
end

function SubEra_Change(message)
    if message == OnEnter then
        DebugMessage("SubEra_Change Started")
        p_empire = Find_Player("Empire")

		start_planet = StoryUtil.SpawnAtSafePlanet("CORUSCANT", p_empire, spawn_location_table, {"Lusankya"})

        if start_planet then
            ProjectAmbition = Find_First_Object("Project_Ambition_Dummy")
            if TestValid(ProjectAmbition) then
                spawn_list_ambition = {
                    "Makati_Steadfast",
                    "Takel_MagicDragon",
                    "Corrupter_Star_Destroyer",
                    "Elite_Squadron",
					"Vorru_Team"
                }
                SpawnList(spawn_list_ambition, start_planet, p_empire, true, false)
                ProjectAmbition.Despawn()
            end
        end

        checkPestage = Find_First_Object("Sate_Pestage")
        if TestValid(checkPestage) then
            checkPestage.Despawn()
        end
        DebugMessage("SubEra_Change Finished")
    end
end

function Spawn_Harrsk(message)
    if message == OnEnter then
        DebugMessage("Spawn_Harrsk Started")
        p_empire = Find_Player("Empire")
        p_harrsk = Find_Player("Warlords")

        checkHarrsk = Find_First_Object("Whirlwind_Star_Destroyer")
        if TestValid(checkHarrsk) then
            checkHarrsk.Despawn()
        end

        --Harrsk spawns
        if spawn_location_table["KALIST"] then
            start_planet = FindPlanet("Kalist")
            if TestValid(start_planet) then
                if start_planet.Get_Owner() == p_empire then
                    ChangePlanetOwnerAndRetreat(start_planet, p_harrsk)

                    spawn_list = {"Whirlwind_Star_Destroyer"}
                    SpawnList(spawn_list, start_planet, p_harrsk, true, false)
                end
            end
        end

        if spawn_location_table["EOLSHA"] then
            start_planet = FindPlanet("ABREGADO_RAE")
            if TestValid(start_planet) then
                if start_planet.Get_Owner() == p_empire then
                    ChangePlanetOwnerAndRetreat(start_planet, p_harrsk)

                    spawn_list = {"Generic_Star_Destroyer"}
                    SpawnList(spawn_list, start_planet, p_harrsk, true, false)
                end
            end
        end
        --CCoGM spawns

        if spawn_location_table["KESSEL"] then
            start_planet = FindPlanet("Kessel")
            if TestValid(start_planet) then
                if start_planet.Get_Owner() == p_empire then
                    ChangePlanetOwnerAndRetreat(start_planet, p_harrsk)

                    spawn_list = {"Tigellinus_Avatar", "Hissa_Moffship"}
                    SpawnList(spawn_list, start_planet, p_harrsk, true, false)
                end
            end
        end
        DebugMessage("Spawn_Harrsk Finished")
    end
end

function EXF_Spawn(message)
    if message == OnEnter then
        DebugMessage("EXF_Spawn Started")
        start_planet = FindPlanet("Nzoth")
		p_owner =  start_planet.Get_Owner()

        if p_owner.Is_Human() then
			Story_Event("EXF_JOINS_SPEECH")
		end
		spawn_list_exf = {"EX_F"}
        SpawnList(spawn_list_exf, start_planet, p_owner, true, false)
        DebugMessage("EXF_Spawn Finished")
    end
end

function ThrawnSpawns(message)
    if message == OnEnter then
        DebugMessage("ThrawnSpawns Started")
        p_empire = Find_Player("Empire")
        p_newrep = Find_Player("Rebel")
        p_maldrood = Find_Player("Teradoc")

        -- Maldrood
		StoryUtil.SpawnAtSafePlanet("CENTARES", p_maldrood, spawn_location_table, {"Tavira_Invidious", "Riizolo_Neutron"})

        -- Hand
		StoryUtil.SpawnAtSafePlanet("NIRAUAN", p_eoth, spawn_location_table, {"Soontir_Fel_Squadron"})

        -- Empire
		spawn_list_Thrawn = {"Chimera", 
					"Corellian_Gunboat_Ferrier",
					"181st_TIE_Interceptor_Squadron",
                    "General_Covell_Team",
                    "Judicator_Star_Destroyer",
                    "Relentless_Star_Destroyer",
                    "Joruus_Cboath_Team",
					"Dezon_Constrainer",
					"Drost_Team",
					"Rukh_Team"
					}
		
		StoryUtil.SpawnAtSafePlanet("CORUSCANT", p_empire, spawn_location_table, spawn_list_Thrawn)
       
	   
        -- New Republic
		StoryUtil.SpawnAtSafePlanet("MYRKR", p_newrep, spawn_location_table, {"Errant_Venture", "Mara_Saber_Team", "Wild_karrde"})

        -- Despawn EotH Thrawn
        ThrawnDespawn = Find_First_Object("Grey_Wolf")
        if TestValid(ThrawnDespawn) then
            ThrawnDespawn.Despawn()
        end

        RegicideObject = Find_First_Object("Dummy_Regicide_Thrawn")
        if TestValid(RegicideObject) then
            RegicideObject.Despawn()
        end
        DebugMessage("ThrawnSpawns Finished")
    end
end

function DarkEmpireSpawns(message)
    if message == OnEnter then
        DebugMessage("DarkEmpireSpawns Started")
        p_newrep = Find_Player("Rebel")
        p_empire = Find_Player("Empire")
        p_eoth = Find_Player("EmpireoftheHand")

        -- Empire
        if spawn_location_table["BYSS"] then
            start_planet = FindPlanet("Byss")
			if start_planet.Get_Owner() ~= Find_Player("Neutral") then
				ChangePlanetOwnerAndRetreat(start_planet,p_empire)
			end
            spawn_list_Palpatine = {
				"Umak_Team",
                "Sedriss_Team",
                "Emperor_Palpatine_Team",
                "General_Veers_Team",
				"Praji_Secutor",
                "Chimera_Pellaeon_Vice",
				"Cronal_Singularity"
            }
            SpawnList(spawn_list_Palpatine, start_planet, p_empire, true, false)
        end
        -- New Republic
        if spawn_location_table["MONCALIMARI"] then
            start_planet = FindPlanet("MonCalimari")
            if start_planet.Get_Owner() ~= Find_Player("Rebel") then
                start_planet = StoryUtil.FindFriendlyPlanet(p_newrep)
            end
        end

        -- Despawn Admiral Aralani
        AralaniDespawn = Find_First_Object("Aralani_Frontier")
        if TestValid(AralaniDespawn) then
            AralaniDespawn.Despawn()
        end

        checkLeia = Find_First_Object("Princess_Leia")
        if TestValid(checkLeia) then
            checkLeia.Despawn()
            spawn_list_Leia = {"Princess_Leia_Team_Noghri"}
            SpawnList(spawn_list_Leia, start_planet, p_newrep, true, false)
        end

        checkAckbar = Find_First_Object("Home_One")
        if TestValid(checkAckbar) then
            checkAckbar.Despawn()
            spawn_list_Ackbar = {"Galactic_Voyager"}
            SpawnList(spawn_list_Ackbar, start_planet, p_newrep, true, false)
        end

        Sleep(1)
        RegicideObject = Find_First_Object("Dummy_Regicide_Palpatine")
        if TestValid(RegicideObject) then
            RegicideObject.Despawn()
        end

        -- Hand	
        StoryUtil.SpawnAtSafePlanet("NIRAUAN", p_eoth, spawn_location_table, {"Siath_Battlehammer"})
        DebugMessage("DarkEmpireSpawns Started")
    end
end

function Empire_Fractures(message)
    if message == OnEnter then
        DebugMessage("Empire_Fractures Started")
        p_empire = Find_Player("Empire")

		spawn_list_Jax = {
                    "Emperors_Revenge_Star_Destroyer",
                    "Jeratai_Allegiance",
                    "Xexus_Shev_Team",
                    "Kooloota_Team",
                    "Carnor_Jax_Team",
                    "Windcaller_Team",
                    "Manos_Team",
                    "Za_Team",
                    "Immodet_Fortress_Company"
                }

		StoryUtil.SpawnAtSafePlanet("BYSS", p_empire, spawn_location_table, spawn_list_Jax)

        RegicideObject = Find_First_Object("Dummy_Regicide_Jax")
        if RegicideObject then
            RegicideObject.Despawn()
        end

        PalpatineObject = Find_First_Object("Emperor_Palpatine")
        if PalpatineObject then
            PalpatineObject.Despawn()
        end

        SedrissObject = Find_First_Object("Sedriss")
        if SedrissObject then
            SedrissObject.Despawn()
        end

        VeersObject = Find_First_Object("Veers_AT_AT_Walker")
        if VeersObject then
            VeersObject.Despawn()
        end
		
		PrajiObject = Find_First_Object("Praji_Secutor")
        if PrajiObject then
            PrajiObject.Despawn()
        end
		
		UmakObject = Find_First_Object("Umak_Leth")
        if UmakObject then
            UmakObject.Despawn()
        end

        GiladObject = Find_First_Object("Chimera_Pellaeon_Vice")
        if GiladObject then
            GiladObject.Despawn()
        end
		
		CronalObject = Find_First_Object("Cronal_Singularity")
        if CronalObject then
            CronalObject.Despawn()
        end

        WarlordObject = Find_First_Object("Reaper_Kaine")
        if WarlordObject then
            if WarlordObject.Get_Owner() == Find_Player("Empire") then
                WarlordObject.Despawn()
            end
        end

        WarlordObject = Find_First_Object("Lancet_Kosh")
        if WarlordObject then
            if WarlordObject.Get_Owner() == Find_Player("Empire") then
                WarlordObject.Despawn()
            end
        end

        WarlordObject = Find_First_Object("13x_Teradoc")
        if WarlordObject then
            if WarlordObject.Get_Owner() == Find_Player("Empire") then
                WarlordObject.Despawn()
            end
        end
		
		WarlordObject = Find_First_Object("Delvardus_Brilliant")
        if WarlordObject then
            if WarlordObject.Get_Owner() == Find_Player("Empire") then
                WarlordObject.Despawn()
            end
        end
        DebugMessage("Empire_Fractures Finished")
    end
end

function DaalaSpawns(message)
    if message == OnEnter then
        DebugMessage("DaalaSpawns Started")
        p_newrep = Find_Player("Rebel")
        p_empire = Find_Player("Empire")
        p_eoth = Find_Player("EmpireoftheHand")
		p_corporate = Find_Player("Corporate_Sector")

        -- Empire	
        if spawn_location_table["THE_MAW"] then
            start_planet = FindPlanet("The_Maw")
			if start_planet.Get_Owner() ~= Find_Player("Neutral") then
				ChangePlanetOwnerAndRetreat(start_planet,p_empire)
			end

            spawn_list_Daala = {"Gorgon", "Odosk_Team", "Sivron_Team"}
            SpawnList(spawn_list_Daala, start_planet, p_empire, true, false)
        end
		
		StoryUtil.SpawnAtSafePlanet("CARIDA", p_empire, spawn_location_table, {"Brakiss_Team", "Ardax_Vendetta"})

        -- New Republic
		StoryUtil.SpawnAtSafePlanet("YAVIN", p_newrep, spawn_location_table, {"Cilghal_Team"})

        -- Hand
		StoryUtil.SpawnAtSafePlanet("NIRAUAN", p_eoth, spawn_location_table, {"Chak_Fel_Krsiss_Squadron_Association", "Ashik_Team"})
        
		StoryUtil.SpawnAtSafePlanet("ETTI_IV", p_corporate, spawn_location_table, {"Phineas_VSD"})
		
        RegicideObject = Find_First_Object("Dummy_Regicide_Daala")
        if TestValid(RegicideObject) then
            RegicideObject.Despawn()
        end
        DebugMessage("DaalaSpawns Finished")
    end
end

function PellaeonSpawns(message)
    if message == OnEnter then
        DebugMessage("DaalaSpawns Started")
        p_newrep = Find_Player("Rebel")
        p_empire = Find_Player("Empire")
        p_eoth = Find_Player("EmpireoftheHand")

        -- Empire
        local RegicideObject = Find_First_Object("Dummy_Regicide_Pellaeon")
        if RegicideObject then
            RegicideObject.Despawn()
			
			spawn_list_Scylla = {
                    "Scylla"
				}

			StoryUtil.SpawnAtSafePlanet("HAKASSI", p_empire, spawn_location_table, spawn_list_Scylla)
        end
		
		spawn_list_Pellaeon = {
                    "Chimera_Pellaeon_Grand",
                    "Disra_Team",
                    "Tierce_Team",
                    "Ascian",
                    "Rogriss_Dominion",
                    "Navett_Team",
                    "181st_Stele",
                    "Hestiv_Team"
                }

		StoryUtil.SpawnAtSafePlanet("BASTION", p_empire, spawn_location_table, spawn_list_Pellaeon)
		
        -- New Republic
		StoryUtil.SpawnAtSafePlanet("CORUSCANT", p_newrep, spawn_location_table, {"Gavrisom_Team"})

        -- Hand
        if spawn_location_table["NIRAUAN"] then
            start_planet = FindPlanet("Nirauan")
            if start_planet.Get_Owner() ~= Find_Player("EmpireoftheHand") then
                start_planet = StoryUtil.FindFriendlyPlanet(p_eoth)
            end
            if start_planet then
                spawn_list_Hand = {"Aurek_Seven_Team"}
                SpawnList(spawn_list_Hand, start_planet, p_eoth, true, false)
            end
        end
        DebugMessage("DaalaSpawns Finished")
    end
end

function Ciutric_Breakaway(message)
    if message == OnEnter then
        DebugMessage("Ciutric_Breakaway Started")
        p_empire = Find_Player("Empire")
        p_ciutric = Find_Player("Warlords")

           if spawn_location_table["CIUTRIC"] then
                start_planet = FindPlanet("Ciutric")
                if TestValid(start_planet) then
                    if start_planet.Get_Owner() == p_empire then
                        ChangePlanetOwnerAndRetreat(start_planet, p_ciutric)

                        spawn_list = {"Generic_Star_Destroyer"}
                        SpawnList(spawn_list, start_planet, p_ciutric, true, false)
                    end
                end
            end

            if spawn_location_table["VROS"] then
                start_planet = FindPlanet("Vros")
                if TestValid(start_planet) then
                    if start_planet.Get_Owner() == p_empire then
                        ChangePlanetOwnerAndRetreat(start_planet, p_ciutric)

                        spawn_list = {"Generic_Star_Destroyer"}
                        SpawnList(spawn_list, start_planet, p_ciutric, true, false)
                    end
                end
            end

            if spawn_location_table["CORVIS"] then
                start_planet = FindPlanet("Corvis")
                if TestValid(start_planet) then
                    if start_planet.Get_Owner() == p_empire then
                        ChangePlanetOwnerAndRetreat(start_planet, p_ciutric)

                        spawn_list = {"Generic_Star_Destroyer"}
                        SpawnList(spawn_list, start_planet, p_ciutric, true, false)
                    end
                end
            end
        DebugMessage("Ciutric_Breakaway Finished")
    end
end

function HapesVassalEmerge(message)
    if message == OnEnter then
        DebugMessage("HapesVassalEmerge Started")
        p_hapes = Find_Player("Hapes_Consortium")

        --Returning occupied Hapan planets

        hapanCorePlanets = {"Hapes", "Charubah", "Terephon", "MistOne", "MistTwo", "MistThree"}
        hapanOwnedPlanets = {}

        for _, planet in pairs(hapanCorePlanets) do
            planetObject = FindPlanet(planet)
            if planetObject.Get_Owner() == Find_Player("Rebel") then
                table.insert(hapanOwnedPlanets, planetObject)
            end
        end

        ChangePlanetOwnerAndRetreat(hapanOwnedPlanets, Find_Player("Hapes_Consortium"))

        for _, planet in pairs(hapanOwnedPlanets) do
            spawn_list_Hapans = {
                "BattleDragon",
                "BattleDragon",
                "BattleDragon",
                "BattleDragon",
                "BattleDragon",
                "BattleDragon",
                "BattleDragon",
                "BattleDragon",
                "BattleDragon",
                "BattleDragon",
                "BattleDragon",
                "BattleDragon",
                "BattleDragon",
                "Nova_Cruiser",
                "Nova_Cruiser",
                "Nova_Cruiser",
                "Nova_Cruiser",
                "Nova_Cruiser",
                "Nova_Cruiser",
                "Nova_Cruiser",
                "Nova_Cruiser",
                "Nova_Cruiser",
                "Nova_Cruiser"
            }
            SpawnList(spawn_list_Hapans, planet, p_hapes, true, false)
        end
        DebugMessage("HapesVassalEmerge Finished")
    end
end

function Check_Coruscant_Owner(message)
    if message == OnEnter then
        DebugMessage("Check_Coruscant_Owner Started")
        p_newrep = Find_Player("Rebel")

        if FindPlanet("Coruscant").Get_Owner() == p_newrep then
            Story_Event("ENABLE_BATTLE_OF_CORUSCANT")
        end
        DebugMessage("Check_Coruscant_Owner Finished")
    end
end

function Spawn_Empire_Reward(message)
    if message == OnEnter then
        DebugMessage("Spawn_Empire_Reward Started")
        local p_empire = Find_Player("Empire")
        local start_planet = FindPlanet("Coruscant")

        spawn_list_coruscant = {
            "Generic_Star_Destroyer_Two",
            "Generic_Secutor",
            "Generic_Allegiance",
            "MTC_Sensor",
            "MTC_Sensor",
            "MTC_Sensor",
            "Generic_Interdictor_Cruiser",
            "Generic_Victory_Destroyer",
            "Vindicator_Cruiser",
            "Carrack_Cruiser",
            "Carrack_Cruiser",
            "Lancer_Frigate",
            "Lancer_Frigate",
            "Lancer_Frigate",
            "Raider_Pentastar",
            "Generic_Procursator",
            "Generic_Victory_Destroyer_Two",
            "Generic_Star_Destroyer",
            "Generic_Star_Destroyer",
            "Strike_Cruiser",
            "Strike_Cruiser",
            "Generic_Victory_Destroyer"
        }
        CoruscantSpawn = SpawnList(spawn_list_coruscant, start_planet, p_empire, false, false)
    end
    DebugMessage("Spawn_Empire_Reward Finished")
end

function Cronus_Lives(message)
    if message == OnEnter then
        local dummy_table = Find_All_Objects_Of_Type("Generic_Executor")
		for i, unit in pairs(dummy_table) do
			if unit.Get_Owner() == Find_Player("Hutts") then
				spawn_list_NH = {"Night_Hammer"}
				SpawnList(spawn_list_NH, unit.Get_Planet_Location(), Find_Player("Hutts"), true, false)
				unit.Despawn()
					
				if Find_Player("Hutts").Is_Human() then
					Story_Event("BUILD_NIGHT_HAMMER")
				end
				Story_Event("NH_Built")
				break
			end
		end
		Story_Event("LOOP_CLEAR")
    end
end

function Perpetuator(message)
    if message == OnEnter then
    end
end

function Perpetuator2(message)
    if message == OnEnter then
    end
end

function Spawn_Katana(message)
    if message == OnEnter then
		local deepspace = FindPlanet("Katana_Space")
		local katana_winner = deepspace.Get_Owner()
		
		local spawn_list_katana
		
		if katana_winner == Find_Player("Rebel") or katana_winner == Find_Player("Hapes_Consortium") then
			spawn_list_katana = {"Katana_Dreadnaught_Rebel"}
		else
			spawn_list_katana = {"Katana_Dreadnaught_Empire"}
		end
		
		SpawnList(spawn_list_katana,deepspace, katana_winner, true, false)
		Story_Event("KATANA_THROTTLE")
    end
end

function Black_Fleet_Crisis(message)
if message == OnEnter then
		local dewit = FindPlanet("Nzoth")
		if TestValid(dewit) then
			Story_Event("BF_Enable")
		end
    end
end