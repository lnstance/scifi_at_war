--******************************************************************************
--     _______ __
--    |_     _|  |--.----.---.-.--.--.--.-----.-----.
--      |   | |     |   _|  _  |  |  |  |     |__ --|
--      |___| |__|__|__| |___._|________|__|__|_____|
--     ______
--    |   __ \.-----.--.--.-----.-----.-----.-----.
--    |      <|  -__|  |  |  -__|     |  _  |  -__|
--    |___|__||_____|\___/|_____|__|__|___  |_____|
--                                    |_____|
--*   @Author:              Corey
--*   @Date:                2017-10-01T19:08:32+02:00
--*   @Project:             Imperial Civil War
--*   @Filename:            GCThrawnClone.lua
--*   @Last modified by:    [TR]Jorritkarwehr
--*   @Last modified time:  2019-2-12T12:39:24+01:00
--*   @License:             This source code may only be used with explicit permission from the developers
--*   @Copyright:           © TR: Imperial Civil War Development Team
--******************************************************************************



require("PGBase")
require("PGStateMachine")
require("PGStoryMode")
require("PGSpawnUnits")

function Definitions()

  DebugMessage("%s -- In Definitions", tostring(Script))

  StoryModeEvents =
  {
	Determine_Faction_LUA = Find_Faction,
	Unlock_Clone = Spawn_Dummy_Perpetual,
	Keep_Unlocked = Spawn_Dummy_Perpetual,
	Loop_Clearer = Perpetuator,
	Find_Clone = Thrawn_Return
  }

end

function Find_Faction(message)
  if message == OnEnter then

	local p_eoth = Find_Player("EmpireoftheHand")
    if p_eoth.Is_Human() then
		Story_Event("ENABLE_BRANCH_EOTH_FLAG")
    end
	
  end
end


function Spawn_Dummy_Perpetual(message)
  if message == OnEnter then
	local checkobject = Find_First_Object("Thrawn_Clone_Dummy")
	if not TestValid(checkobject) then
		local p_eoth = Find_Player("EmpireoftheHand")
		local start_planet = FindPlanet("Nirauan")
		local dummytype = Find_Object_Type("Thrawn_Clone_Dummy")
		if start_planet.Get_Owner() == p_eoth then
			local probe_list = Spawn_Unit(dummytype, start_planet, p_eoth)
		end
	end
	Story_Event("LOOP_CLEAR")
  end
end

function Perpetuator(message)
    if message == OnEnter then
    end
end

function Thrawn_Return(message)
  if message == OnEnter then
	local p_eoth = Find_Player("EmpireoftheHand")
	local start_planet = FindPlanet("Nirauan")
	local cloned = Find_Object_Type("Eviscerator")
	local probe_list = Spawn_Unit(cloned, start_planet, p_eoth) --since an object was just built by the EotH at Nirauan, there's not much need to make sure they exist and own it
	ScriptExit()
  end
end