--******************************************************************************
--     _______ __
--    |_     _|  |--.----.---.-.--.--.--.-----.-----.
--      |   | |     |   _|  _  |  |  |  |     |__ --|
--      |___| |__|__|__| |___._|________|__|__|_____|
--     ______
--    |   __ \.-----.--.--.-----.-----.-----.-----.
--    |      <|  -__|  |  |  -__|     |  _  |  -__|
--    |___|__||_____|\___/|_____|__|__|___  |_____|
--                                    |_____|
--*   @Author:              Corey
--*   @Date:                2017-12-18T14:01:09+01:00
--*   @Project:             Imperial Civil War
--*   @Filename:            GCWarlordsCampaign.lua
--*   @Last modified by:
--*   @Last modified time:  2018-03-13T22:28:32-04:00
--*   @License:             This source code may only be used with explicit permission from the developers
--*   @Copyright:           © TR: Imperial Civil War Development Team
--******************************************************************************

require("PGStoryMode")
require("PGSpawnUnits")
require("eawx-util/ChangeOwnerUtilities")
StoryUtil = require("eawx-util/StoryUtil")

function Definitions()
    DebugMessage("%s -- In Definitions", tostring(Script))

    StoryModeEvents = {
        Support_Candidate_Leia = Leia_Support,
        Support_Candidate_Borsk = Borsk_Support,
        Support_Candidate_Mothma = Mothma_Support,
        Support_Candidate_Gavrisom = Gavrisom_Support,
		Support_Candidate_Tevv = Tevv_Support,
		Support_Candidate_SoBilles = SoBilles_Support,
		Support_Candidate_Navik = Navik_Support,
		
		Admiral_Initializer = init_admirals,
		Loop_Clearer = Perpetuator,
		Build_New_Slot = Handle_New_Slot,
		Research_NMCP2 = NMCP2_Research,
		Decrement_1_Slot = Decrement_1,
		Decrement_2_Slots = Decrement_2,
		Decrement_3_Slots = Decrement_3,
		Decrement_4_Slots = Decrement_4,
		Remove_Slots = Decrement_All,
		Admiral_Death = Admiral_Die,
		Assign_Ackbar = Ackbar_Spawn,
		Retire_Ackbar = Ackbar_Despawn,
		No_Ackbar = Exit_Ackbar,
		Assign_Nantz = Nantz_Spawn,
		Retire_Nantz = Nantz_Despawn,
		Assign_Sovv = Sovv_Spawn,
		Retire_Sovv = Sovv_Despawn,
		Assign_Solo = Solo_Spawn,
		Retire_Solo = Solo_Despawn,
		Falcon_Respawn = Respawn_Falcon,
		Assign_Han = Han_Spawn,
		Retire_Han = Han_Despawn,
		Falcon_Respawn2 = Respawn_Falcon2,
		Assign_Iblis = Iblis_Spawn,
		Retire_Iblis = Iblis_Despawn,
		Upgrade_Iblis = Iblis_Upgrade,
		Upgrade_Iblis2 = Iblis_Upgrade2,
		No_Iblis = Exit_Iblis,
		Assign_Drayson = Drayson_Spawn,
		Retire_Drayson = Drayson_Despawn,
		Assign_Ragab = Ragab_Spawn,
		Retire_Ragab = Ragab_Despawn,
		Assign_Kalback = Kalback_Spawn,
		Retire_Kalback = Kalback_Despawn,
		Kalback_Exit = Exit_Kalback,
		Assign_Tallon = Tallon_Spawn,
		Retire_Tallon = Tallon_Despawn,
		Assign_Vantai = Vantai_Spawn,
		Retire_Vantai = Vantai_Despawn,
		Assign_Brand = Brand_Spawn,
		Retire_Brand = Brand_Despawn,
		Assign_Bell = Bell_Spawn,
		Retire_Bell = Bell_Despawn,
		Assign_Abaht = Abaht_Spawn,
		Retire_Abaht = Abaht_Despawn,
		Assign_Krefey = Krefey_Spawn,
		Retire_Krefey = Krefey_Despawn,
		Assign_Ackdool = Ackdool_Spawn,
		Retire_Ackdool = Ackdool_Despawn,
		Assign_Burke = Burke_Spawn,
		Retire_Burke = Burke_Despawn,
		Assign_Massa = Massa_Spawn,
		Retire_Massa = Massa_Despawn,
		Upgrade_Massa = Massa_Upgrade,
		Assign_Dorat = Dorat_Spawn,
		Retire_Dorat = Dorat_Despawn,
		Assign_Grant = Grant_Spawn,
		Retire_Grant = Grant_Despawn,
		Assign_Snunb = Snunb_Spawn,
		Retire_Snunb = Snunb_Despawn,
		Upgrade_Snunb = Snunb_Upgrade,
		Assign_Nammo = Nammo_Spawn,
		Retire_Nammo = Nammo_Despawn,
		Iblis_Add = Add_Iblis,
		NMCP2_Add = Add_NMCP2,
		Nammo_Add = Add_Nammo,
		Dorat_Add = Add_Dorat,
		Massa_Add = Add_Massa,
		Massa_to_Dorat = Massa_Dorat,
		Dorat_to_Massa = Dorat_Massa,
		Krefey_Add = Add_Krefey,
		Ackdool_Add = Add_Ackdool,
    }
	
	full_list = { --All options for reference operations. Note that the tag in the first field should be the prefix of the enable event: e.g. Drayson and DRAYSON_UNLOCK
		{"Ackbar","Ackbar_Assign",{"Ackbar_Retire","Ackbar_Retire2"},{"Home_One","Galactic_Voyager"},"TEXT_UNIT_GALACTIC_VOYAGER"},
		{"Nantz","Nantz_Assign",{"Nantz_Retire","Nantz_Retire2"},{"Nantz_Independence","Nantz_Faithful_Watchman"},"TEXT_UNIT_NANTZ"},
		{"Sovv","Sovv_Assign",{"Sovv_Retire","Sovv_Retire2"},{"Sovv_Dauntless","Sovv_Voice_of_the_People"},"TEXT_UNIT_SOVV"},
		{"Solo","Solo_Assign",{"Solo_Retire"},{"Solo_Remonda"},"TEXT_HERO_HAN_SOLO"},
		{"Han","Han_Assign",{"Han_Retire"},{"Han_Intrepid"},"TEXT_HERO_HAN_SOLO"}, --Han's forms are so separate that it's easier to handle them separately
		{"Iblis","Iblis_Assign",{"Iblis_Retire","Iblis_Retire2","Iblis_Retire3"},{"Iblis_Peregrine","Iblis_Selonian_Fire","Iblis_Harbinger"},"TEXT_HERO_GARM"},
		{"Drayson","Drayson_Assign",{"Drayson_Retire","Drayson_Retire2"},{"Drayson_True_Fidelity","Drayson_New_Hope"},"TEXT_HERO_DRAYSON"},
		{"Ragab","Ragab_Assign",{"Ragab_Retire"},{"Ragab_Emancipator"},"TEXT_HERO_RAGAB"},
		{"Kalback","Kalback_Assign",{"Kalback_Retire"},{"Kalback_Justice"},"TEXT_HERO_KALBACK"},
		{"Tallon","Tallon_Assign",{"Tallon_Retire"},{"Tallon_Silent_Water"},"TEXT_HERO_TALLON"},
		{"Vantai","Vantai_Assign",{"Vantai_Retire"},{"Vantai_Moonshadow"},"TEXT_HERO_VANTAI"},
		{"Brand","Brand_Assign",{"Brand_Retire","Brand_Retire2"},{"Brand_Indomitable","Brand_Yald"},"TEXT_HERO_BRAND"},
		{"Bell","Bell_Assign",{"Bell_Retire"},{"Bell_Endurance"},"TEXT_HERO_BELL"},
		{"Abaht","Abaht_Assign",{"Abaht_Retire"},{"Abaht_Intrepid"},"TEXT_HERO_ABAHT"},
		{"Nammo","Nammo_Assign",{"Nammo_Retire"},{"Nammo_Defiance"},"TEXT_HERO_NAMMO"},
		{"Krefey","Krefey_Assign",{"Krefey_Retire"},{"Krefey_Ralroost"},"TEXT_HERO_KREFEY"},
		{"Snunb","Snunb_Assign",{"Snunb_Retire","Snunb_Retire2"},{"Snunb_Antares_Six","Snunb_Resolve"},"TEXT_HERO_SNUNB"},
		{"Ackdool","Ackdool_Assign",{"Ackdool_Retire"},{"Ackdool_Mediator"},"TEXT_HERO_ACKDOOL"},
		{"Burke","Burke_Assign",{"Burke_Retire"},{"Burke_Remember_Alderaan"},"TEXT_HERO_BURKE"},
		{"Massa","Massa_Assign",{"Massa_Retire","Massa_Retire2"},{"Massa_Lucrehulk_Auxiliary","Massa_Lucrehulk_Carrier"},"TEXT_HERO_MASSA"},
		{"Dorat","Dorat_Assign",{"Dorat_Retire"},{"Dorat_Arrow_of_Sullust"},"TEXT_HERO_DORAT"},
		{"Grant","Grant_Assign",{"Grant_Retire"},{"Grant_Oriflamme"},"TEXT_HERO_GRANT"},
	}
	
	--Heroes currently available for purchase. Seeded with those who have no special prereqs
	admiral_list = {
		"Ackbar",
		"Nantz",
		"Sovv",
		"Solo",
		"Drayson",
		"Ragab",
		"Kalback",
		"Tallon",
		"Vantai",
		"Snunb",
		"Burke",
		"Massa",
	}
	
	total_slots = 4			 --Max slot number. Set at the start of the GC and never change
	free_admiral_slots = 4   --Slots open to buy
	vacant_admiral_slots = 0 --Slots that need another action to move to free
	
	--tracking indices of permanently upgrading heroes
	Iblis_index = 1
	Snunb_index = 1
	Massa_index = 1
	
	Voon_Chel_death_flag = false --marked true when one has actually died to prevent an infinite loop of their deaths unlocking each other
end

function Leia_Support(message)
    DebugMessage("Support Change Started")
    if message == OnEnter then
        GlobalValue.Set("ChiefOfStatePreference", "DUMMY_CHIEFOFSTATE_LEIA")
    end
    DebugMessage("Support Change Finished")
end

function Borsk_Support(message)
    DebugMessage("Support Change Started")
    if message == OnEnter then
        GlobalValue.Set("ChiefOfStatePreference", "DUMMY_CHIEFOFSTATE_FEYLYA")
    end
    DebugMessage("Support Change Finished")
end

function Mothma_Support(message)
    DebugMessage("Support Change Started")
    if message == OnEnter then
        GlobalValue.Set("ChiefOfStatePreference", "DUMMY_CHIEFOFSTATE_MOTHMA")
    end
    DebugMessage("Support Change Finished")
end

function Gavrisom_Support(message)
    DebugMessage("Support Change Started")
    if message == OnEnter then
        GlobalValue.Set("ChiefOfStatePreference", "DUMMY_CHIEFOFSTATE_GAVRISOM")
    end
    DebugMessage("Support Change Finished")
end

function Tevv_Support(message)
    DebugMessage("Support Change Started")
    if message == OnEnter then
        GlobalValue.Set("ChiefOfStatePreference", "DUMMY_CHIEFOFSTATE_TEVV")
    end
    DebugMessage("Support Change Finished")
end

function SoBilles_Support(message)
    DebugMessage("Support Change Started")
    if message == OnEnter then
        GlobalValue.Set("ChiefOfStatePreference", "DUMMY_CHIEFOFSTATE_SOBILLES")
    end
    DebugMessage("Support Change Finished")
end

function Navik_Support(message)
    DebugMessage("Support Change Started")
    if message == OnEnter then
        GlobalValue.Set("ChiefOfStatePreference", "DUMMY_CHIEFOFSTATE_NAVIK")
    end
    DebugMessage("Support Change Finished")
end


function Lock_Options()
	Story_Event("ACKBAR_LOCK")
	Story_Event("NANTZ_LOCK")
	Story_Event("SOVV_LOCK")
	Story_Event("SOLO_LOCK")
	--No Han lock intentionally, as that replaces a hero on the field
	Story_Event("KALBACK_LOCK")
	Story_Event("IBLIS_LOCK")
	Story_Event("DRAYSON_LOCK")
	Story_Event("RAGAB_LOCK")
	Story_Event("VANTAI_LOCK")
	Story_Event("TALLON_LOCK")
	Story_Event("BRAND_LOCK")
	Story_Event("BELL_LOCK")
	Story_Event("ABAHT_LOCK")
	Story_Event("KREFEY_LOCK")
	Story_Event("ACKDOOL_LOCK")
	Story_Event("SNUNB_LOCK")
	Story_Event("NAMMO_LOCK")
	Story_Event("BURKE_LOCK")
	Story_Event("MASSA_LOCK")
	Story_Event("DORAT_LOCK")
	Story_Event("GRANT_LOCK")
end

function Get_Active_Admirals(init)
	local admiral_count = 0
	local admiral_list = {}
	for index, entry in pairs(full_list) do
		for index2, ship in pairs(entry[4]) do
			local find_it = Find_First_Object(ship)
			if TestValid(find_it) then
				if find_it.Get_Owner() == Find_Player("Rebel") then
					admiral_count = admiral_count + 1
					if init == true then
						remove_admiral_entry(entry[1])
					end
					table.insert(admiral_list, entry[5])
				end
				break
			end
		end
	end
	DebugMessage("Admiral check count: %s vacant: %s total: %s", tostring(admiral_count), tostring(vacant_admiral_slots), tostring(total_slots))
	for id=admiral_count+1,admiral_count+vacant_admiral_slots do
		table.insert(admiral_list, "TEXT_GOVERNMENT_ADMIRAL_VACANT")
	end
	for id=admiral_count+vacant_admiral_slots+1,total_slots do
		table.insert(admiral_list, "TEXT_GOVERNMENT_ADMIRAL_OPEN")
	end
	GlobalValue.Set("NR_ADMIRAL_LIST", admiral_list)
	return admiral_count
end

function init_admirals(message)
    if message == OnEnter then
		free_admiral_slots = free_admiral_slots - Get_Active_Admirals(true)
		vacant_admiral_slots = 0
		Lock_Options()
		Unlock_Valid_Options()
		
		if TestValid(Find_First_Object("Iblis_Selonian_Fire")) then
			Iblis_index = 2
		end
		if TestValid(Find_First_Object("Iblis_Harbinger")) then
			Iblis_index = 3
		end
		if TestValid(Find_First_Object("Snunb_Resolve")) then
			Snunb_index = 2
		end
		
		local tech_level = Find_Player("Rebel").Get_Tech_Level()
		
		if tech_level >= 1 then
			Handle_Admiral_Exit("Kalback")
			Handle_Admiral_Exit("Massa")
			Handle_Admiral_Add("Dorat")
			Voon_Chel_death_flag = true
			Story_Event("DORAT2MASSA_LOCK")
		end
		
		if tech_level >= 2 then
			Handle_Admiral_Exit("Solo")
			Handle_Admiral_Add("Grant")
			Story_Event("GRANT_RETIRE_UNLOCK")
		end
		
		if tech_level >= 3 then
			Handle_Admiral_Exit("Ragab")
		end
		
		if tech_level >= 4 then
			Handle_Admiral_Add("Brand")
			Handle_Admiral_Add("Abaht")
			
			total_slots = total_slots + 1
			free_admiral_slots = free_admiral_slots + 1
			Unlock_Valid_Options()
		end
    end
end

function remove_admiral_entry(entry)
	for index, obj in pairs(admiral_list) do
		if obj == entry then
			table.remove(admiral_list, index)
			return true
		end
	end
	return false
end

function Get_Admiral_Entry(key)
	for index, obj in pairs(full_list) do
		if obj[1] == key then
			return obj
		end
	end
	return nil
end

function check_admiral_entry(entry)
	for index, obj in pairs(admiral_list) do
		if obj == entry then
			return true
		end
	end
	return false
end

function Handle_Admiral_Despawn(admiral_tag)
	local admiral_data = Get_Admiral_Entry(admiral_tag)
	for flaship_id=1,table.getn(admiral_data[4]) do
		local admiral_retire = admiral_data[3][flaship_id]
		local admiral_unit = admiral_data[4][flaship_id]
		
		local check_admiral = Find_First_Object(admiral_retire)
		if TestValid(check_admiral) then
			check_admiral.Despawn()
		end
		check_admiral = Find_First_Object(admiral_unit)
		if TestValid(check_admiral) then
			check_admiral.Despawn()
			free_admiral_slots = free_admiral_slots + 1
			table.insert(admiral_list, admiral_tag)
			Unlock_Valid_Options()
			Story_Event("LOOP_CLEAR")
			Get_Active_Admirals(false)
			return true
			 --You could do the insert here so the retiree isn't immediately unlocked, but that makes it awkward to get them back if they're the only one in the pool
		end
	end
	
	Story_Event("LOOP_CLEAR")
	return false
end

function Handle_Admiral_Spawn(admiral_tag)
	Handle_Admiral_Spawn_Indexed(admiral_tag,1)  --Always take primary flagship for initial spawn
end

function Handle_Admiral_Spawn_Indexed(admiral_tag, index)
	local admiral_data = Get_Admiral_Entry(admiral_tag)
	
	local admiral_assign = admiral_data[2]
	local admiral_unit = admiral_data[4][index]
	
	local check_admiral = Find_First_Object(admiral_assign)
	local planet
    if TestValid(check_admiral) then
		planet = check_admiral.Get_Planet_Location()
        check_admiral.Despawn()
	else
		planet = StoryUtil.FindFriendlyPlanet(Find_Player("Rebel"))
    end
	if free_admiral_slots > 0 then
		free_admiral_slots = free_admiral_slots - 1
		remove_admiral_entry(admiral_tag)
		SpawnList({admiral_unit}, planet, Find_Player("Rebel"), true, false)
	end
	
	Lock_Options()
	Unlock_Valid_Options()
	Get_Active_Admirals(false)
	
	Story_Event("LOOP_CLEAR")
end

function Admiral_Die(message)
	if message == OnEnter then
		--You'd think you could increment vacant_admiral_slots, but two+ deaths in the same battle won't register
		--On the plus side, calculating this from scratch means the distinction between, death, retirement, and flagship swap is found here instead of through a bunch of disable events
		local active_admirals = Get_Active_Admirals(false)
		vacant_admiral_slots = total_slots - free_admiral_slots - active_admirals
		if vacant_admiral_slots > 0 then
			Story_Event("EXTRA_UNLOCK")
		end
		Story_Event("LOOP_CLEAR")
	end
end

function Handle_New_Slot(message)
	if message == OnEnter then
		local new_slot = Find_First_Object("Extra_Admiral_Slot")
		if TestValid(new_slot) then
			new_slot.Despawn()
		end
		vacant_admiral_slots = vacant_admiral_slots - 1
		free_admiral_slots = free_admiral_slots + 1
		Unlock_Valid_Options()
		if vacant_admiral_slots == 0 then
			Story_Event("EXTRA_LOCK")
		end
		Get_Active_Admirals(false)
		Story_Event("LOOP_CLEAR")
	end
end

--Handle the permanent removal of an option for story purposes
function Handle_Admiral_Exit(admiral_tag)
	local entry = Get_Admiral_Entry(admiral_tag)
	for index, ship in pairs(entry[4]) do
		local find_it = Find_First_Object(ship)
		if TestValid(find_it) then
			find_it.Despawn()
			free_admiral_slots = free_admiral_slots + 1
			Lock_Options()
			Unlock_Valid_Options()
			Get_Active_Admirals(false)
			return true
		end
	end
			
	remove_admiral_entry(admiral_tag)
	Lock_Options()
	Unlock_Valid_Options()
	Get_Active_Admirals(false)
	return false
end

--Handle the addition of an admiral to the pool
function Handle_Admiral_Add(admiral_tag)
	--Don't add if the ship already exists
	if check_admiral_entry(admiral_tag) then
		return
	end
	local entry = Get_Admiral_Entry(admiral_tag)
	for index, ship in pairs(entry[4]) do
		local find_it = Find_First_Object(ship)
		if TestValid(find_it) then
			return
		end
	end
	table.insert(admiral_list, admiral_tag)
	Unlock_Valid_Options()
end

function Unlock_Valid_Options()
	--local CoS = GlobalValue.Get("ChiefOfState")
	if free_admiral_slots > 0 then
		for index, adm in pairs(admiral_list) do
			Story_Event(adm .. "_UNLOCK")
		end
	end
	if vacant_admiral_slots > 0 then
		Story_Event("EXTRA_UNLOCK")
	end
end

function Perpetuator(message)
    if message == OnEnter then
    end
end

function Decrement_Amount(amount)
	total_slots = total_slots - amount
	free_admiral_slots = free_admiral_slots - amount
	if free_admiral_slots <= 0 then
		Lock_Options()
	end
end

function NMCP2_Research(message)
	if message == OnEnter then
		total_slots = total_slots + 1
		--Free does not go up beause Bell is occupying the new slot
		local planet = FindPlanet("Coruscant")
		local NR_player = Find_Player("Rebel")
		if TestValid(planet) then
			if planet.Get_Owner() ~= NR_player then
				planet = StoryUtil.FindFriendlyPlanet(NR_player)
			end
		else
			planet = StoryUtil.FindFriendlyPlanet(NR_player)
		end
		SpawnList({"Bell_Endurance"}, planet, NR_player, true, false)
		Get_Active_Admirals(false)
	end
end

function Decrement_1(message)
	if message == OnEnter then
		Decrement_Amount(1)
	end
end

function Decrement_2(message)
	if message == OnEnter then
		Decrement_Amount(2)
	end
end

function Decrement_3(message)
	if message == OnEnter then
		Decrement_Amount(3)
	end
end

function Decrement_4(message)
	if message == OnEnter then
		Decrement_Amount(4)
	end
end

function Decrement_All(message)
	if message == OnEnter then --Negative so that even lost admirals won't reenable slots
		total_slots = -5
		free_admiral_slots = -5
		Lock_Options()
	end
end

function Ackbar_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Ackbar")
	end
end

function Ackbar_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Ackbar")
	end
end

function Exit_Ackbar(message)
	if message == OnEnter then
		Handle_Admiral_Exit("Ackbar")
	end
end

function Nantz_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Nantz")
	end
end

function Nantz_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Nantz")
	end
end

function Sovv_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Sovv")
	end
end

function Sovv_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Sovv")
	end
end

function Solo_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Solo")
	end
end

function Solo_Despawn(message)
	if message == OnEnter then
		local planet
		local dummy = Find_First_Object("Solo_Retire")
		if TestValid(dummy) then
			planet = dummy.Get_Planet_Location()
		else
			planet = StoryUtil.FindFriendlyPlanet(Find_Player("Rebel"))
		end
		if Handle_Admiral_Despawn("Solo") then
			SpawnList({"Han_Solo_Team"}, planet, Find_Player("Rebel"), true, false)
		end
	end
end

function Respawn_Falcon(message)
	if message == OnEnter then
		if not (check_admiral_entry("Solo") or TestValid(Find_First_Object("Millennium_Falcon"))) then
			local NR_player = Find_Player("Rebel")
			local planet = StoryUtil.FindFriendlyPlanet(NR_player)
			SpawnList({"Han_Solo_Team"}, planet, NR_player, true, false)
			if NR_player.Is_Human() then
				Story_Event("HAN_RESPAWN_SPEECH")
			end
		end
	end
end

function Han_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Exit("Abaht")
		Handle_Admiral_Spawn("Han")
		if Find_Player("Rebel").Is_Human() then
			Story_Event("HAN_INTREPID_SPEECH")
		end
	end
end

function Han_Despawn(message)
	if message == OnEnter then
		local planet
		local dummy = Find_First_Object("Han_Retire")
		if TestValid(dummy) then
			planet = dummy.Get_Planet_Location()
		else
			planet = StoryUtil.FindFriendlyPlanet(Find_Player("Rebel"))
		end
		if Handle_Admiral_Despawn("Han") then
			SpawnList({"Han_Solo_Team"}, planet, Find_Player("Rebel"), true, false)
		end
		Handle_Admiral_Add("Abaht")
	end
end

function Respawn_Falcon2(message)
	if message == OnEnter then
		if not (check_admiral_entry("Han") or TestValid(Find_First_Object("Millennium_Falcon"))) then
			local NR_player = Find_Player("Rebel")
			local planet = StoryUtil.FindFriendlyPlanet(NR_player)
			SpawnList({"Han_Solo_Team"}, planet, NR_player, true, false)
			if NR_player.Is_Human() then
				Story_Event("HAN_RESPAWN_SPEECH")
			end
		end
	end
end

function Iblis_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn_Indexed("Iblis", Iblis_index)
	end
end

function Iblis_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Iblis")
	end
end

function Iblis_Despawn3(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Iblis")
	end
end

function Iblis_Upgrade(message)
	if message == OnEnter then
		Iblis_index = 2
	end
end

function Iblis_Upgrade2(message)
	if message == OnEnter then
		Iblis_index = 3
	end
end

function Exit_Iblis(message)
	if message == OnEnter then
		Handle_Admiral_Exit("Iblis")
	end
end

function Drayson_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Drayson")
	end
end

function Drayson_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Drayson")
	end
end

function Ragab_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Ragab")
	end
end

function Ragab_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Ragab")
	end
end

function Kalback_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Kalback")
	end
end

function Kalback_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Kalback")
	end
end

function Exit_Kalback(message)
	if message == OnEnter then
		if Handle_Admiral_Exit("Kalback") then
			if Find_Player("Rebel").Is_Human() then
				Story_Event("KALBACK_SPEECH")
			end
		end
	end
end

function Tallon_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Tallon")
	end
end

function Tallon_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Tallon")
	end
end

function Vantai_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Vantai")
	end
end

function Vantai_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Vantai")
	end
end

function Brand_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Brand")
	end
end

function Brand_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Brand")
	end
end

function Bell_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Bell")
	end
end

function Bell_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Bell")
	end
end

function Abaht_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Abaht")
	end
end

function Abaht_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Abaht")
	end
end

function Krefey_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Krefey")
	end
end

function Krefey_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Krefey")
	end
end

function Ackdool_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Ackdool")
	end
end

function Ackdool_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Ackdool")
	end
end

function Burke_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Burke")
	end
end

function Burke_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Burke")
	end
end

function Massa_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn_Indexed("Massa", Massa_index)
	end
end

function Massa_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Massa")
	end
end

function Massa_Upgrade(message)
	if message == OnEnter then
		Massa_index = 2
	end
end

function Dorat_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Dorat")
	end
end

function Dorat_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Dorat")
	end
end

function Grant_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Grant")
	end
end

function Grant_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Grant")
	end
end

function Snunb_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn_Indexed("Snunb", Snunb_index)
	end
end

function Snunb_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Snunb")
	end
end

function Snunb_Upgrade(message)
	if message == OnEnter then
		Snunb_index = 2
	end
end

function Nammo_Spawn(message)
	if message == OnEnter then
		Handle_Admiral_Spawn("Nammo")
	end
end

function Nammo_Despawn(message)
	if message == OnEnter then
		Handle_Admiral_Despawn("Nammo")
	end
end

function Add_Iblis(message)
	if message == OnEnter then
		Handle_Admiral_Add("Iblis")
	end
end

function Add_NMCP2(message)
	if message == OnEnter then
		Handle_Admiral_Add("Brand")
		Handle_Admiral_Add("Abaht")
		local dummy = Find_First_Object("Solo_Remonda")
		if TestValid(dummy) then
			local planet = dummy.Get_Planet_Location()
			if Handle_Admiral_Exit("Solo") then
				SpawnList({"Han_Solo_Team"}, planet, Find_Player("Rebel"), true, false)
				if Find_Player("Rebel").Is_Human() then
					Story_Event("HAN_RETIRE_SPEECH")
				end
			end
		else
			Handle_Admiral_Exit("Solo")
		end
		
	end
end

function Add_Nammo(message)
	if message == OnEnter then
		if not (check_admiral_entry("Ackbar") or TestValid(Find_First_Object("Home_One")) or TestValid(Find_First_Object("Galactic_Voyager")) ) then
			Handle_Admiral_Add("Nammo")
		end
	end
end

function Massa_Dorat(message)
	if message == OnEnter then
		Handle_Admiral_Add("Dorat")
		Handle_Admiral_Exit("Massa")
	end
end

function Dorat_Massa(message)
	if message == OnEnter then
		Handle_Admiral_Add("Massa")
		Handle_Admiral_Exit("Dorat")
	end
end

function Add_Dorat(message)
	if message == OnEnter then
		if not Voon_Chel_death_flag and (not (check_admiral_entry("Massa") or TestValid(Find_First_Object("Massa_Lucrehulk_Auxiliary")) or TestValid(Find_First_Object("Massa_Lucrehulk_Carrier")) )) then
			Handle_Admiral_Add("Dorat")
			Story_Event("DORAT2MASSA_LOCK")
			Voon_Chel_death_flag = true
		end
	end
end

function Add_Massa(message)
	if message == OnEnter then
		if not Voon_Chel_death_flag and (not (check_admiral_entry("Dorat") or TestValid(Find_First_Object("Dorat_Arrow_of_Sullust")) )) then
			Handle_Admiral_Add("Massa")
			Story_Event("MASSA2DORAT_LOCK")
			Voon_Chel_death_flag = true
		end
	end
end

function Add_Krefey(message)
	if message == OnEnter then
		Handle_Admiral_Add("Krefey")
	end
end

function Add_Ackdool(message)
	if message == OnEnter then
		Handle_Admiral_Add("Ackdool")
	end
end