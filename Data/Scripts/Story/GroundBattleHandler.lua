--******************************************************************************
--     _______ __
--    |_     _|  |--.----.---.-.--.--.--.-----.-----.
--      |   | |     |   _|  _  |  |  |  |     |__ --|
--      |___| |__|__|__| |___._|________|__|__|_____|
--     ______
--    |   __ \.-----.--.--.-----.-----.-----.-----.
--    |      <|  -__|  |  |  -__|     |  _  |  -__|
--    |___|__||_____|\___/|_____|__|__|___  |_____|
--                                    |_____|
--*   @Author:              [TR]Pox
--*   @Date:                2017-11-24T12:43:51+01:00
--*   @Project:             Imperial Civil War
--*   @Filename:            InvadingFleet.lua
--*   @Last modified by:    svenmarcus
--*   @Last modified time:  2018-03-30T03:07:16+02:00
--*   @License:             This source code may only be used with explicit permission from the developers
--*   @Copyright:           © TR: Imperial Civil War Development Team
--******************************************************************************

require("PGEvents")
require("PGStoryMode")
require("TRCommands")

function Definitions()

    DebugMessage("%s -- In Definitions", tostring(Script))
    
    AnakinObiwan_Timer = 0
	
	StoryModeEvents =
	{
		Battle_Start = DetermineEvents
	}

	defender = nil

end


function DetermineEvents(message)
    DebugMessage("DetermineEvents Ground Handler Started")
    if message == OnEnter then
	
		if not ModContentLoader then
            ModContentLoader = require("eawx-std/ModContentLoader")
		end
		
		local reinforcementPointTypes = {
            "Reinforcement_Point",
            "Reinforcement_Point_Plus1_Cap", 
            "Reinforcement_Point_Plus2_Cap", 
            "Reinforcement_Point_Plus3_Cap", 
            "Reinforcement_Point_Plus4_Cap", 
            "Reinforcement_Point_Plus5_Cap",
            "Reinforcement_Point_Plus10_Cap"}
			
		local p_neutral = Find_Player("Neutral")

        for i, obj in pairs(reinforcementPointTypes) do
            local reinforcementPoints = Find_All_Objects_Of_Type(obj)

            for _,point in pairs(reinforcementPoints) do

               unit = Find_Object_Type("Field_Base_Pad")

               entry = Find_First_Object("Attacker Entry Position")
               distance = point.Get_Distance(entry)
            
               if distance >= 200 then
                    Spawn_Unit(unit, point.Get_Position(), p_neutral)
               end
            end
		end
		
		CONSTANTS = ModContentLoader.get("GameConstants")
		
		players = {}
		
		for _, player_name in pairs(CONSTANTS.PLAYABLE_FACTIONS) do
			player_object = Find_Player(player_name)
			if player_object.Is_Human() then
				p_human = player
			end
			
			if table.getn(Find_All_Objects_Of_Type(player_object)) > 0 then
				table.insert(players, player_object)
			end
		end
		
		--for _, player in pairs(players) do
		--	DebugMessage("Player %s is present in battle", tostring(player.Get_Name()))
		--end
		
		for _, player in pairs(players) do
			if EvaluatePerception("Is_Defender", player) > 0 then
				defender = player
				break
			end
		end
		
		victory_point_present = false
		
        victoryPoint = Find_First_Object("Victory_Point")
		
		if TestValid(victoryPoint) then
			victory_point_present = true
		end

        if p_human ~= defender then
            if victory_point_present then
                victoryPoint.Highlight(true)
                Story_Event("CAPTURE_POINT_PRESENT")
            else
                Story_Event("CAPTURE_POINT_NOT_PRESENT")
            end
        else
            if victory_point_present then
                victoryPoint.Highlight(true)
                Story_Event("CAPTURE_POINT_PRESENT_DEFENDER")
            else
                Story_Event("CAPTURE_POINT_NOT_PRESENT_DEFENDER")
            end
        end
		
		for _, player in pairs(players) do
			if player ~= defender then
				if EvaluatePerception("Disable_Bombardment", player) > 0 then
					player.Disable_Orbital_Bombardment(true)
					bombardment_disabled = true
					DebugMessage("Initial bombardment disabled for %s", tostring(player.Get_Faction_Name()))
				else
					bombardment_disabled = false
					DebugMessage("Initial bombardment enabled for %s", tostring(player.Get_Faction_Name()))
				end
			end
		end
		
        DebugMessage("DetermineEvents Ground Handler Finished")

    elseif message == OnUpdate then
        DebugMessage("DetermineEvents Ground Handler Update Started")
        if victory_point_present and victoryPoint.Get_Owner() ~= Find_Player("Neutral") then
			for _, player in pairs(players) do
				if victoryPoint.Get_Owner() == player and player ~= defender then
					--DebugMessage("Triggered story event %s", "SET_VICTOR_"..string.gsub(tostring(player.Get_Name()), " ", "_").upper.."_FLAG")
					Story_Event("SET_VICTOR_"..string.upper(tostring(player.Get_Faction_Name())).."_FLAG")
					ScriptExit()
				end
			end
        end
		
		for _, player in pairs(players) do
			if player ~= defender and bombardment_disabled then
				if EvaluatePerception("Disable_Bombardment", player) == 0 then
					player.Disable_Orbital_Bombardment(false)
					DebugMessage("Bombardment enabled for %s", tostring(player.Get_Faction_Name()))
				end
			end
		end
		
        DebugMessage("DetermineEvents Ground Handler Update Finished")

        if Find_First_Object("Obi_Wan") and Find_First_Object("Anakin") then
            if AnakinObiwan_Timer <= 0 then
                Anakin_ObiWan_Greeting()
            else
                AnakinObiwan_Timer = AnakinObiwan_Timer - 1
            end
        end
    end
end

function Anakin_ObiWan_Greeting()
      
    if Find_First_Object("Obi_Wan").Get_Distance(Find_First_Object("Anakin")) <= 200 then
        Find_First_Object("Obi_Wan").Play_SFX_Event("Unit_Banter_Obiwan_Anakin")

        AnakinObiwan_Timer = 200
    end

end
