--******************************************************************************
--     _______ __
--    |_     _|  |--.----.---.-.--.--.--.-----.-----.
--      |   | |     |   _|  _  |  |  |  |     |__ --|
--      |___| |__|__|__| |___._|________|__|__|_____|
--     ______
--    |   __ \.-----.--.--.-----.-----.-----.-----.
--    |      <|  -__|  |  |  -__|     |  _  |  -__|
--    |___|__||_____|\___/|_____|__|__|___  |_____|
--                                    |_____|
--*   @Author:              [TR]Pox
--*   @Date:                2017-08-20T21:31:11+02:00
--*   @Project:             Imperial Civil War
--*   @Filename:            Katana_Story.lua
--*   @Last modified by:    [TR]Pox
--*   @Last modified time:  2017-12-21T12:40:57+01:00
--*   @License:             This source code may only be used with explicit permission from the developers
--*   @Copyright:           © TR: Imperial Civil War Development Team
--******************************************************************************



require("PGStoryMode")
require("PGSpawnUnits")
require("PGMoveUnits")



function Definitions()

	DebugMessage("%s -- In Definitions", tostring(Script))

	StoryModeEvents =
	{
		Battle_Start = Begin_Battle,
		STORY_VICTORY_Player = Return_Katana_Count
	}



	marker_list = {}
	mission_started = false
	start_speech_trigger = "START_SPEECH"
	end_speech_trigger = "END_SPEECH"

end


function Begin_Battle(message)
	if message == OnEnter then
		DebugMessage("KatanaFleet Begin_Battle Started")
		empire = Find_Player("Empire")
		rebels = Find_Player("Rebel")
		eoth = Find_Player("EmpireoftheHand")
		pentastar = Find_Player("Pentastar")
		zsinj = Find_Player("Pirates")
		maldrood = Find_Player("Teradoc")
		eriadu = Find_Player("Hutts")
		csa = Find_Player("Corporate_Sector")
		hapans = Find_Player("Hapes_Consortium")
		hostile = Find_Player("Hostile")
		pirates = Find_Player("Warlords")
			
		rebels.Make_Ally(pirates)
		hostile.Make_Ally(pirates)
		empire.Make_Ally(pirates)
		eoth.Make_Ally(pirates)
		pentastar.Make_Ally(pirates)
		zsinj.Make_Ally(pirates)
		maldrood.Make_Ally(pirates)
		eriadu.Make_Ally(pirates)
		csa.Make_Ally(pirates)
		hapans.Make_Ally(pirates)
		
		pirates.Make_Ally(empire)
		pirates.Make_Ally(rebels)
		pirates.Make_Ally(hostile)
		pirates.Make_Ally(eoth)
		pirates.Make_Ally(pentastar)
		pirates.Make_Ally(zsinj)
		pirates.Make_Ally(maldrood)
		pirates.Make_Ally(eriadu)
		pirates.Make_Ally(csa)
		pirates.Make_Ally(hapans)
		
		entry_marker = Find_First_Object("Attacker Entry Position")
		defender_marker = Find_First_Object("Defending Forces Position")
		reb_boarders = {"Gallofree_Transport", "Gallofree_Transport", "Gallofree_Transport"}
		imp_boarders = {"Imperial_Landing_Craft", "Imperial_Landing_Craft", "Imperial_Landing_Craft"}
		
		reb_enemy = {"Alliance_Assault_Frigate", "Alliance_Assault_Frigate", "Quaser", "Katana_Dreadnaught_Rebel", "Alliance_Assault_Frigate", "Alliance_Assault_Frigate", "Quaser", "Katana_Dreadnaught_Rebel", "MC80B", "Calamari_Cruiser"}
		imp_enemy = {"Generic_Star_Destroyer_Two", "Generic_Star_Destroyer_Two", "Carrack_Cruiser", "Carrack_Cruiser", "Generic_Star_Destroyer_Two", "Generic_Star_Destroyer_Two", "Carrack_Cruiser", "Carrack_Cruiser"}


		player_list = nil
		enemy_list = nil
		player = nil

		if empire.Is_Human() then
			player_list = imp_boarders
			enemy_list = reb_enemy
			player = empire
			start_speech_trigger = "START_SPEECH_IR"
			end_speech_trigger = "END_SPEECH_IR"
		elseif rebels.Is_Human() then
			player = rebels
			player_list = reb_boarders
			enemy_list = imp_enemy
			start_speech_trigger = "START_SPEECH_NR"
			end_speech_trigger = "END_SPEECH_NR"
		elseif eoth.Is_Human() then
			player_list = imp_boarders
			enemy_list = reb_enemy
			player = eoth
			start_speech_trigger = "START_SPEECH_EOTH"
			end_speech_trigger = "END_SPEECH_EOTH"
		elseif pentastar.Is_Human() then
			player_list = imp_boarders
			enemy_list = reb_enemy
			player = pentastar
			start_speech_trigger = "START_SPEECH_PA"
			end_speech_trigger = "END_SPEECH_PA"
		elseif zsinj.Is_Human() then
			player_list = imp_boarders
			enemy_list = reb_enemy
			player = zsinj
			start_speech_trigger = "START_SPEECH_ZE"
			end_speech_trigger = "END_SPEECH_ZE"
		elseif maldrood.Is_Human() then
			player_list = imp_boarders
			enemy_list = reb_enemy
			player = maldrood
			start_speech_trigger = "START_SPEECH_GM"
			end_speech_trigger = "END_SPEECH_GM"
		elseif eriadu.Is_Human() then
			player_list = imp_boarders
			enemy_list = reb_enemy
			player = eriadu
			start_speech_trigger = "START_SPEECH_EA"
			end_speech_trigger = "END_SPEECH_EA"
		elseif csa.Is_Human() then
			player_list = reb_boarders
			enemy_list = imp_enemy
			player = csa
			start_speech_trigger = "START_SPEECH_CSA"
			end_speech_trigger = "END_SPEECH_CSA"
		elseif hapans.Is_Human() then
			player_list = reb_boarders
			enemy_list = imp_enemy
			player = hapans
			start_speech_trigger = "START_SPEECH_HAPES"
			end_speech_trigger = "END_SPEECH_HAPES"
		end
		
		plot = Get_Story_Plot("Tactical_Katana.XML")
		
		event = plot.Get_Event("INCREMENT_FLAG_Katana_Count")
		
		if table.getn(Find_All_Objects_Of_Type(player))== 0 then --The AI took Katana Space, exit the battle
			event.Set_Reward_Parameter(1, 10)
			Story_Event("AI_AUTOWIN")
			ScriptExit()
		end

		boarder_list = SpawnList(player_list, entry_marker, player, false, true)
		
		defender_list = SpawnList(enemy_list, defender_marker, hostile, true, true)






		for j, unit in pairs(boarder_list) do
			Register_Prox(unit, Take_Over_Katana, 200, Find_Player("Neutral"))

		end


		--spawn = Find_First_Object("STORY_TRIGGER_ZONE_00")

		--Spawn_Unit(Find_Object_Type("Katana_Dreadnaught"), spawn, Find_Player("Neutral"))
		list = Find_All_Objects_Of_Type("Katana_Dreadnaught")

		current_cinematic_thread = Create_Thread("Intro_Cinematic")
		mission_started = true
		DebugMessage("KatanaFleet Begin_Battle Finished")
	end
end




function Return_Katana_Count(message)
	if message == OnEnter then
		DebugMessage("KatanaFleet Return_Katana_Count Started")
		reb_katana = {}
		katana_list = Find_All_Objects_Of_Type("Katana_Dreadnaught")
		for i, obj in pairs(katana_list) do
			if obj.Get_Owner() == player then
				table.insert(reb_katana, obj)

			end
		end
		event.Set_Reward_Parameter(1, table.getn(reb_katana))
		Story_Event("INCREMENT_KATANA_COUNT")
		DebugMessage("KatanaFleet Return_Katana_Count Finished")

	end
end



function Take_Over_Katana(self_obj, trigger_obj)
	DebugMessage("KatanaFleet Take_Over_Katana Started")
	for j, obj in pairs(list) do
		if trigger_obj == obj then

			table.remove(list, j)
			trigger_obj.Change_Owner(pirates)

			self_obj.Stop()
			self_obj.Suspend_Locomotor(true)
			if self_obj.Get_Owner().Is_Human() then
				Story_Event("BOARDING_KATANA_TEXT")
			end
			unit_list = {self_obj, trigger_obj}
			Create_Thread("ChangeKatanaOwner",unit_list)



		end
	end
	DebugMessage("KatanaFleet Take_Over_Katana Finished")
end





function ChangeKatanaOwner(unit_list)
	DebugMessage("KatanaFleet ChangeKatanaOwner Started")
	Sleep(15)

	if TestValid(unit_list[1]) then

		if unit_list[1].Get_Owner().Is_Human() then
			Story_Event("BOARDING_SUCCESSFUL_TEXT")
		end
		unit_list[1].Suspend_Locomotor(false)
		unit_list[2].Change_Owner(unit_list[1].Get_Owner())

	end
	DebugMessage("KatanaFleet ChangeKatanaOwner Finished")
end




function Intro_Cinematic()
	DebugMessage("KatanaFleet ChangeKatanaOwner Started")

	Fade_Screen_Out(.5)
	Suspend_AI(1)
	Lock_Controls(1)
	Letter_Box_In(0)

	Start_Cinematic_Camera()

	-- camera location	object	   x     y     z   + not so important stuff
	Set_Cinematic_Camera_Key(list[1], 1500, 120, 30, 1, 0, 0, 0)

	-- object the camera points at     x  y  z  blabla
	Set_Cinematic_Target_Key(list[1], 0, 0, 0, 0, 0, 0, 0)

	Fade_Screen_In(3)
	Story_Event(start_speech_trigger)

	Sleep(9)

	Transition_Cinematic_Target_Key(boarder_list[1], 5, 0, 0, 0, 0, 0, 1, 0)
	Transition_Cinematic_Camera_Key(boarder_list[1], 5, 600, 10, 30, 1, 0, 1, 0)

	Sleep(9)


	Transition_To_Tactical_Camera(1)

	Story_Event(end_speech_trigger)
    	Letter_Box_Out(1)
	Sleep(1)
    	End_Cinematic_Camera()
    	Fade_Screen_In(.5)
	Lock_Controls(0)
	Suspend_AI(0)
	DebugMessage("KatanaFleet ChangeKatanaOwner Finished")
end


function Story_Handle_Esc()
	DebugMessage("KatanaFleet Story_Handle_Esc Started")
  	if current_cinematic_thread ~= nil then
		Thread.Kill(current_cinematic_thread)
		current_cinematic_thread = nil

    		Letter_Box_Out(0)
		End_Cinematic_Camera()
		Lock_Controls(0)
		Suspend_AI(0)


	end
	DebugMessage("KatanaFleet Story_Handle_Esc Finished")
end
