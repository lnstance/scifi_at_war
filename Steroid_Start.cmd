@echo off
:: ===================================================================================
:: ************************ V0.3 Written by Imperial / Regret ************************
:: ===================================================================================
:: CAUTION this path must not contain emptyspaces, as they are not supported for Custom_Mod_Path. 
:: Set to false if not used
set Custom_Mod_Path=false

:: Replace StarWarsG with StarWarsI to switch to Debug mode
set Exe_Path=steamapps\common\"Star Wars Empire at War"\corruption\StarWarsG


:: Leave Host_Mod_ID empty or comment it out, if this mod IS no submod!
:: EAWX - Thrawn's Revenge
:: set Host_Mod_ID=1125571106

:: EAWX - Fall of the Republic
:: set Host_Mod_ID=1976399102

:: ===================================================================================
:: **************************** Make sure Steam is started ***************************
:: ===================================================================================
set Use_Workshop=true 

:: Get name of current directory
for %%I in (.) do set CurrentDirName=%%~nxI
:: echo %CurrentDirName%
:: echo %~p0 :: = full path


cd..
for %%I in (".") do set Parent_Folder=%%~nxI
if %Parent_Folder%==Mods cd.. && set Use_Workshop=false 
if %Parent_Folder%==mods cd.. && set Use_Workshop=false
if %Parent_Folder%==MODS cd.. && set Use_Workshop=false


:: Grabbing steam directory form registry and make sure steam has launched.
set KEY_NAME=HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Valve\Steam
set VALUE_NAME=InstallPath
set Exe=\steam.exe
FOR /F "tokens=2* skip=2" %%a in ('reg query "%KEY_NAME%" /v "%VALUE_NAME%"') do set Steam_Path=%%b


:: echo "%Steam_Path%%Exe%"
cd "%Steam_Path%"
:: start steam.exe 

:: ===================================================================================
:: ********************************** Get Core Count *********************************
:: ===================================================================================
set Core_Count=%NUMBER_OF_PROCESSORS%

:: Setting Thread_Count as Hex Value
if %Core_Count%==3 set Thread_Count=6 
if %Core_Count%==4 set Thread_Count=C 
if %Core_Count%==6 set Thread_Count=30 
if %Core_Count%==8 set Thread_Count=C0
if %Core_Count%==10 set Thread_Count=300 
if %Core_Count%==12 set Thread_Count=C00 
if %Core_Count%==14 set Thread_Count=3000 
if %Core_Count%==16 set Thread_Count=C000
if %Core_Count%==32 set Thread_Count=C0000000
if %Core_Count%==64 set Thread_Count=C000000000000000
:: cls
:: echo Thread Count: %Thread_Count%


 

:: All paths below are meant for the Steam version of EAW
:: ===================================================================================
:: ************************** From inside of Custom_Mod_Path *************************
:: ===================================================================================
if %Custom_Mod_Path% == false goto :Mod_Directory 

start /affinity %Thread_Count% /High %Exe_Path% modpath=%Custom_Mod_Path% Ignoreasserts 


goto End_Of_File
:: ===================================================================================
:: ************************* From inside of .\corruption\Mods ************************
:: ===================================================================================
:Mod_Directory
if %Use_Workshop%==true goto Workshop

:: start from any mod inside of .\mods directory!
start /affinity %Thread_Count% /High %Exe_Path% modpath=mods\%CurrentDirName% Ignoreasserts 


goto End_Of_File
:: ===================================================================================
:: ******************** From inside of .\workshop\content\32470\ *********************
:: ===================================================================================
:Workshop

start /affinity %Thread_Count% /High %Exe_Path% STEAMMOD=%CurrentDirName% STEAMMOD=%Host_Mod_ID% Ignoreasserts
:: start /affinity %Thread_Count% /High %Exe_Path% STEAMMOD=%CurrentDirName% STEAMMOD=%Host_Mod_ID% Ignoreasserts Language=English


:End_Of_File
:: pause
:: ===================================================================================
:: ********************************** End of File ************************************
:: ===================================================================================
